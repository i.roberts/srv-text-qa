
;January;Gonville Hall;Gonville;Caius College;Cambridge;England
;January 25;6.9-magnitude;1348;Friuli;earthquake;Northern;Italy;Europe
;February 2;Teutonic;Grand Duchy;Lithuania
;April 7;Charles University;Prague;the previous year;Charles;King;Bohemia
;April 23;Edward III;England;first;English
;June 24;The Black Death;England;English;Channel;sailor;Gascony;Melcombe;Weymouth;Dorset;November;London
;July 6;Pope Clement VI;Jews;Black Death pandemic
;November 1;Union of Valencia;Jews;King;Valencia
;November 18;Emperor;Kōmyō;Japan;Emperor;Sukō;second;and third;Northern Court;Ashikaga Pretenders;his
;Europe;Cairo
;Stefan;Emperor;Serbia;Thessaly;Epirus
;Pskov;Republic;Novgorod Republic;Bolotovo
;Hundred Years;War;1337;1360;England;France;1355
;Hangzhou;Mongolian;China;Cairo;Mamluk;Egypt
;April 11;Emperor;1385

;John Fitzalan;1st;Arundel;1379
;Alice Perrers;English;1400
;February 2;Christian;Lithuanian;prince;Pinsk
;June 9;Lorenzetti;Sienese;painter;1290
;June 13;Don Juan Manuel;prince;Villena;Spanish;writer;1282
;July 1;Joan;England;princess;1333/34
;August 20;Laurence Hastings;1st;Earl;Pembroke;English;1319
;August 23;John de Stratford;Archbishop;Canterbury;1275
;October 2;Alice de Lacy;4th;Countess;Lincoln;English;1281

;Laura de Noves;French;countess;Petrarch;1310
;Pietro Lorenzetti;Sienese;painter;1280
;Emir;killed in action;1309
;Giovanni Villani;Florence;1276
