;Rodrigo Díaz de Vivar;1043;10 July 1099;Castilian;medieval;Spain;Christians;El Campeador;Master of the Battlefield;Old;Spanish;him;Vivar del Cid;Burgos;He;Díaz de Vivar;Christian;Muslim;Valencia;independent;his;his;his;El Cid;Spain;medieval;Spanish;El Cantar;his
;this day;El Cid;Spanish;his
;El Cid;Ferdinand the Great;Ferdinand;Sancho II;León;Castile;commander;Castile;Sancho;1065;He;El Cid;Castilian;Sancho;Alfonso VI;León;García II;Galicia;Muslim;Castile;Muslims;Sancho;He;his;Sancho;1072;El Cid;Sancho;Alfonso;El Cid;his;El Cid;he;his;him;him;1081;he
;El Cid;Muslim;Zaragoza;Aragon;he;strategist;leader;he;his;Muslim;Lérida;Christian;Christian;King;Sancho Ramírez;Aragon;He;1086;North;African;Almoravids;Castile;Alfonso;El Cid;he;El Cid;Christian;El Cid;his;the next several years;El Cid;Valencia;Alfonso;Banu Hud;Muslim;his;Valencia;Islamic;Yahya al-Qadir;1092;He;his;his;El Cid;Valencia;1094;El Cid;independent;Mediterranean;Spain;Christians;Muslims;He
;El Cid;years;first;1094;Caurte;Valencia;He;his;El Cid;Valencia;Diego Rodríguez;Almoravids;Alfonso;1097;his;El Cid;1099;Jimena Díaz;Valencia;1102;his;him;she
;Spanish;Old Castilian;Arabic;Mozarabs;Arabs;Christians;his;him;Rodrigo;Arab;Ludriq al-Kanbiyatur;Rodrigo;doctor;King;Sancho II;Castile;King;Alfonso VI;León;King;García II;Galicia;He;his;Christian;Arab;Christian;his;him;his;him;he;first;1195;Navarro-Aragonese Linage de;Rodric Díaz
;El Cid;Rodrigo Díaz;1043;Vivar;Castillona de Bivar;ten;six;Burgos;Castile;Diego Laínez;bureaucrat;His;El Cid;later years;one;him;El Cid;Laín;five;Ferdinand;Rodrigo Álvarez;two;Sancho II;El Cid;one;his;his
;1057;Rodrigo;Moorish;Zaragoza;emir;Sancho;the spring of 1063;Rodrigo;Graus;Ferdinand;Ramiro I of Aragon;Moorish;Graus;river;Cinca;Al-Muqtadir;Castilian;El Cid;Aragonese;Ramiro;Aragonese;One;Aragonese;Campeador
;Ferdinand;Sancho;Christian;Moorish;Zamora;Badajoz;his;Sancho;Alfonso;Sancho;Alfonso;Sancho;him;his;him
;Sancho;1072;Alfonso;Urraca;his;his;December 2013;Sancho;Alfonso;Toledo;king;Castile;León;his;his;his;Sancho;murder;He;Castilian;El Cid;dozen;Alfonso;Santa;Gadea;Saint Agatha) Church;Burgos;he;his;February 2010;Rodrigo Diaz;Alfonso VI;Castile;León;Rodrigo;Rodrigo;García Ordóñez;December 2012
;1079;Rodrigo;Alfonso VI;Seville;Castile;Granada;Castilian;Seville;Rodrigo;Christian;Cabra;king;he;his;he;García Ordóñez;Castilian;three days
;1079;El Cid;Emir;Abdullah;Granada;García Ordóñez;his;his;Granada;Alfonso;May 8, 1080;El Cid;King;Alfonso;El Cid;Alfonso;El Cid;Alfonso;El Cid;Seville;El Cid
;first;Barcelona;Ramon Berenguer II;he;his
;El Cid;Ramon Berenguer II;El Cid;Zaragoza;he;1081;El Cid;Moorish;king;al-Andalus;Zaragoza;Yusuf al-Mu;Hud;his;him;his;Moorish;Muwallads;Berbers;Arabs;Malians;He;June 2017
;Moorish
;El Cid;Alfonso;Yusuf;Hud;Zaragoza;he
;Medieval;Spain;Cornell University Press;1975;Joseph F. O'Callaghan;his
;1081;1085;Zaragoza;al-Mundhir;Lérida;Tortosa;his;El Cid;al-Mutamin;Zaragoza;al-Mundhir;Sancho I of Aragón;Ramon Berenguer II;1082;he
;1084;Zaragoza;El Cid;Aragonese;Morella;Tortosa;autumn;Castilians;Toledo;later the next year;Christians;Salamanca;Toledo
;1086;Almoravid;Iberian Peninsula;Gibraltar;Almoravids;Berber;North Africa;Yusuf ibn Tashfin;Alfonso;Almoravid;Badajoz;Málaga;Granada;Tortosa;Seville;León;Aragón;Castile
;1087;Raymond of Burgundy;Christian;Zaragoza;Siege of Tudela;Alfonso;Aledo;Murcia;Taifas;Iberian Peninsula;his
;Alfonso;El Cid;his;El Cid;July 1087
;El Cid;Alfonso;now;he;his;Zaragoza;He;El Cid;Almoravid;Alfonso;Almoravids;Alfonso;Alfonso;his;El Cid;he;February 2012;Valencia;him;his
;El Cid;Christian;Moorish;Moorish Mediterranean;Valencia;his;his;First;Berenguer Ramon II;Barcelona;May 1090;El Cid;Berenguer;Pinar de Tévar;Monroyo;Teruel;Berenguer;Ramon Berenguer III;El Cid;Maria;future;his
;Valencia;El Cid;Valencia;El Puig
;El Cid;Valencia;Yahya al-Qadir;October 1092;Valencia;judge;Ibn Jahhaf;Almoravids;Valencia;December 1093;May 1094;El Cid;Mediterranean;his;El Cid;Alfonso;El Cid;independent;Christian;Muslim;Christians;Jerome;Périgord;bishop
;El Cid;Jimena Díaz;Valencia;five years;Almoravids;his;El Cid;July 10, 1099;His;Valencia;Masdali;May 5, 1102;Christian;125 years;Jimena;Burgos;Castile;1101;El Cid;She;her;Castile;now;Burgos Cathedral;his
;Valencia;Jimena;El Cid;Babieca;his;his;his;his;Rodrigo;Valencia;Christian;Spaniards;his;Jimena;Burgos;Jimena;Burgos;her
;El Cid;Roman;Greek;his;him;his;El Cid;Castejón;his;Minaya;Spanish;Anaia;Álvar Fáñez;Castile;Alfonso VI;him;his;his
;Babieca;Bavieca;El Cid;El Cid;Babieca;One;El Cid;he;Rodrigo;Pedro El Grande;monk;Carthusian;Pedro;El Cid;Andalusian;his;El Cid;monk;his;El Cid;King;Sancho;Campeador;King;El Cid;Babieca;Bavieca;his;Babieca;Seville;war;Babia;León;Spain;Carmen Campidoctoris;Babieca;El Cid
;Babieca;Christians;El Cid;El Cid;Babieca;San Pedro;de Cardeña;him;El Cid;His
;El Cid;Tizona;Army Museum (Museo del Ejército;Toledo;1999;Moorish Córdoba;the eleventh century;Damascus
;2007;Castile;León;1.6 million Euros;currently;Museum of Burgos
;El Cid
;El Cid;Jimena Díaz;Asturias;the mid-1070s;Historia Roderici;Count;Diego Fernández de Oviedo;her;El Cid;first;her;he;her;El Cid;Jimena;two;Cristina;María;Diego Rodríguez;Muslim Almoravids;North Africa;Consuegra;1097;El Cid;Cristina Rodríguez;María;Cristina;Ramiro;Monzón;García Sánchez III;Navarre;El Cid;Navarre;King;García Ramírez;Her;María;first;prince;Aragon;Peter;later wed;Ramon Berenguer III;count;Barcelona;she;Carrión
;El Cid;the 12th century;one;his;Rodrigo;El Cid;Reconquista;Spain;him;the early 17th century;Spanish;writer;Guillén de Castro;French;playwright;Pierre Corneille;one;Le Cid;his;Spanish;Romantic;Juan Eugenio Hartzenbusch;La;Jura de;Santa;Gadea;José Zorrilla;La;He;2019;Arturo Pérez-Reverte
;Georges Bizet;Don Rodrigue;1873;Jules Massenet;Le Cid;1885;Corneille;Claude Debussy;1890;Rodrigue;Edison Denisov;1993;he;his
;American;actor;Charlton Heston;1961;Anthony Mann;Doña Ximena;Italian;actress;Sophia Loren;2020;Amazon Prime Video;Spanish;Jaime Lorente;El Cid
;1980;Ruy;Nippon Animation
;El Cid;Spanish;16;1;his;2004
;second
;first;and second;Medieval;War;El Cid;independent;general;Valencia
;2003;Spanish;Legend
;Ministry of Time;Spanish;2;1;February 15, 2016




;McNair;Alexander J.;https://www.scribd.com/doc/294324954;1254;Poem of the Cid;Medieval;26;2010;45;68
;Kurtz;Barbara E.;University of Illinois
;I. Michael;Manchester;1975
;Burton Raffel;2009
;http://www.planetalibro.com.ar/ebooks/eam/ebook_view.php?ebooks_books_id=85;Spanish
;European;third
;R. Selden Rose;Leonard Bacon;Semicentennial Publications of the University of California;1868;1918;Berkeley;CA;University of California Press;1997
;https://books.google.com/books?id=TcgJAAAAQAAJ;Ruy Díaz de Vibar;1828
;https://books.google.com/books?id=0y1RAAAAcAAJ;1533
;Simon Barton;Richard Fletcher;Spanish;Manchester;University Press;2000;0;-7190-5225-4;0;-7190-5226-2
;Gonzalo Martínez Díez;Verdadero Rodrigo Díaz de Vivar;http://www.editorial.planeta.es;Spain;June 1999;84
;C. Melville;A. Ubaydli;Christians;Spain;III;Arabic;711;1501;Warminster;1992
;Joseph F. O'Callaghan;Medieval;Spain;Ithaca;Cornell University Press;1975
;Peter Pierson;Spain;Ed;John E. Findling;Frank W. Thacheray;Wesport;Connecticut;Greenwood Press;1999;34;36
;http://libro.uca.edu/alfonso6/;Bernard F. Reilly;León-Castilla;King;Alfonso VI;1065;1109;Princeton;New Jersey;University Press;1988
;http://balagan.info/industrial-warfare/reconquista;Steven Thomas;711;1492
;M. J. Trow;Sutton Publishing Limited;2007
;Henry Edwards Watts;1026;1099;Christian;Spain;Spain;Moorish Conquest;Fall;Granada;711;1492 AD;New York;Putnam;1894;71;91
;T.Y. Henderson;Valencia
;J. I. Garcia Alonso;J. A. Martinez;A. J. Criado;El Cid;Europe;11/4;1999
;http://www.caminodelcid.org/Camino_TheRouteofElCid.aspx;English
