;John Horton Conway;100%;26 December 1937;11 April 2020;English;mathematician;He
;Liverpool;Conway;first;University of Cambridge;United States;John von Neumann;Princeton University;his;he;his;11 April 2020;82;complications;he

;Conway;26 December 1937;Liverpool;Cyril Horton Conway;Agnes Boyce;He;11;mathematician;he;his;sixth;Gonville;Caius College;Cambridge;he;Cambridge;charismatic;mathematician;he;his;him
;Conway;1959;Harold Davenport;Davenport;fifth;Conway;years;Cambridge Mathematical Tripos;hours;his;his;he;1964;Lecturer;Sidney Sussex College;Cambridge;He;his;Cambridge;1986;John von Neumann;Princeton University;he
;Conway;one;His
;Martin Gardner;Scientific;American;1970;days;Conway;he

;Conway;Scientific;American;columnist;Martin Gardner;Gardner;Conway;Mathematical Games;October 1970;Conway;his;his;Gardner;Conway;first;late;1950s;the years;Gardner;Conway;Conway;Jul 1967;Hackenbush;Jan 1972;Feb 1974;he;his;September 1976;Conway;Games;Conway;he
;Conway;Martin Gardner;Gardner;He;him;his;1976;Gardner;a week;him;him;Conway;Gardner;January 1977;he;his;Scientific;American;Conway
;4;Gardner;every two years;Martin Gardner;Conway
;Conway;CGT;his;Elwyn Berlekamp;Richard Guy;he;Games;ONAG;CGT;He
;one;philosopher;He;Conway;He;2006;He
;Donald Knuth;He;Conway;He;0th
;the mid-1960s;Michael Guy;Conway;sixty-four;two;non-Wythoffian;Conway;Conway
;he
;first;He
;Conway;Alexander;now;more than a decade;the 1980s;Conway;Conway;the 19th-century;four;11;3;½;11;1970;one;two;Fox;D. Lombardero;1968;one;Alexander;his;he;Conway;him
;author;He;Robert Curtis;Simon P. Norton;first;his;he;three;Leech lattice;he;him
;1978;mathematician;John McKay;Conway;Norton;Conway;two;now
;Conway;Mathieu;12;13
;student;one;Edward Waring;37;fifth;Chen Jingrun;Conway;he
;Conway;Stephen Kleene;Neil Sloane;he
;13;He
;the day of the week;he;Conway;two seconds;his;he;his;his;him;he;One;his
;2004;Conway;Simon B. Kochen;Princeton;mathematician;Conway
;Conway;1971;Royal Society;1981;American Academy of Arts and Sciences;1992;first;LMS;1987;1998;Leroy P. Steele;2000;American Mathematical Society;2001;University of Liverpool;2014;one;Alexandru Ioan Cuza University;he
;1981;mathematician;His;He
;2017;Conway;British Mathematical Association
;8 April 2020;Conway;11 April;New Brunswick;New Jersey;82;he
;1971;Chapman;London;1971;Chapman;0412106205
;1976;Academic Press;New York;1976;6;0121863506
;1979;Coplanar Points;Paul Erdős;Michael Guy;H. T. Croft;London Mathematical Society;II;19;137;143
;1979;Simon P. Norton;London Mathematical Society;11;2;308;339
;1982;Richard K. Guy;Elwyn Berlekamp;0120911507
;1985;Robert Turner Curtis;Simon Phillips Norton;Richard A. Parker;Robert Arnott Wilson;Clarendon Press;New York;Oxford University Press;1985;0198531990
;1988;Neil Sloane;Springer-Verlag;New York;290;9780387966175
;1995;Neil Sloane;R. H. Hardin;Tom Duff;14;3;237;259
;1996;Book of Numbers;Richard K. Guy;Copernicus;New York;1996;0614971667
;1997;Francis Yein Chei Fung;Mathematical Association of America;Washington, DC;1997;Carus;26;1614440255
;2002;Derek A. Smith;A. K. Peters;Natick;MA;2002;1568811349
;2008;Symmetries of Things;Heidi Burgiel;Chaim Goodman-Strauss;A. K. Peters;Wellesley;MA;2008;1568812205
;John Horton Conway
;Alpert;Mark;1999;Games] Scientific;American;April 1999
;Conway;John;Smith;Derek A.;2003;http://math.ucr.edu/home/baez/octonions/conway_smith/;Amer;2005;42;2;229;243;1568811349
;Boden;Margaret;2006;Mind As Machine;Oxford University Press;2006;1271
;James;2014;https://sinews.siam.org/Details-Page/martin-gardners-mathematical-grapevine;Martin Gardner;Mathematical Grapevine] Book Reviews of Undiluted;Martin Gardner;Martin Gardner;Twenty-First;Century;James Case;SIAM News;1 April 2014
;Marcus;2008;308
;Richard K;1983;https://www.jstor.org/pss/2690263;Conway;56;1;Jan. 1983;26;33
;Harris;Michael;2015;http://www.nature.com/nature/journal/v523/n7561/full/523406a.html;John Horton Conway;23 July 2015
;Mulcahy;Colm;2014;https://blogs.scientificamerican.com/guest-blog/the-top-10-martin-gardner-scientific-american-articles/?redirect=1;10;Martin Gardner Scientific American Articles;American;21 October 2014
;Princeton University;2009;http://www.math.princeton.edu/WebCV/ConwayBIB.pdf;John H. Conway
;Rendell;Paul;2015;https://www.springer.com/gp/book/9783319198415;Turing Machine Universality;Springer;July 2015;3319198416
;Charles;1994;http://www.users.cloud9.net/~cgseife/conway.html;Conway
;Schleicher;Dierk;2011;https://www.ams.org/notices/201305/rnoti-p567.pdf;John Conway
;John Conway;2014
;2014
;https://www.quantamagazine.org/john-conway-solved-mathematical-problems-with-his-bare-hands-20200420/;Keith Hartnett;Quanta Magazine;April 20, 2020
