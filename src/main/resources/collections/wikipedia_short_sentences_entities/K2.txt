
;8611;m;second;Mount Everest;8848;m;China;Pakistan;Baltistan;Gilgit-Baltistan;Pakistan;Dafdar Township;Taxkorgan Tajik Autonomous County;Xinjiang;China;Pakistan;Xinjiang
;Savage;Mountain;George Bell;climber;1953;American;Expedition;five;one;four;Chhogori;Mount Godwin-Austen;King;Mountain of Mountains;climber;Reinhold Messner;his
;first;Italian;Lino Lacedelli;Achille Compagnoni;1954;Italian;Karakoram;Ardito Desio;January 2021;eight-thousander;winter;Nepalese
;eight-thousand;July;August;the year;now;Everest;2018;6;367;86;eight-thousanders
;British;India;Thomas Montgomerie;first;Karakoram;Mount Haramukh;130;two
;Mount Everest;Tibetan;Chomolungma;Qomolongma;George Everest;29;30;Masherbrum;Askole;Chogori;two;Western;Chinese;Lamba Pahar;Urdu;Dapsang
;Mount Godwin-Austen;Henry Godwin-Austen;explorer;Royal Geographical Society
;surveyor;now;Balti;Kechu;Ketu;Italian;climber;Fosco Maraini;his;He
;André Weil
;Karakoram Range;Baltistan;Gilgit;Baltistan;Pakistan;Taxkorgan Tajik Autonomous County;Xinjiang;China;India;Tarim;Lesser Himalayas;January 2021
;22nd;independent;Karakoram;Tibetan Plateau;Himalaya;Mount Everest;Everest;4594;m;Kora;La;Nepal;China;Mustang Lo;independent;Karakoram
;3000;3200;m;3000;m;2800;m;4000;m
;1986;George Wallerstein;Mount Everest;1987
;Broad Peak;Sarpo Laggo;K2 Gneiss;Karakoram Metamorphic Complex;K2 Gneiss;Base Camp;Gilkey-Puchoz;Broad Peak;K2 Gneiss;Karakoram Metamorphic Complex;Skyang Kangri
;40;39;115;120;million years ago;Gneiss;K2 Gneiss;Asia;Asia;Indian;20;K2 Gneiss;post-Miocene;K2 Gneiss
;first;British;1856;Thomas Montgomerie;second;Karakoram;Gasherbrum II;Gasherbrum;1892;Martin Conway;British;Concordia
;first;1902;Oscar Eckenstein;Aleister Crowley;Jules Jacot-Guillarmod;Heinrich Pfannl;Victor Wessely;Guy Knowles;Northeast;Ridge;1900s;fourteen days
;five;6525;m;Crowley;Crowley;malaria;68 days;time;eight
;1938;First;American;Charles Houston;Abruzzi Spur;8000;m
;The following year;1939;American;Karakoram;Fritz Wiessner;200;m;disaster;Dudley Wolfe;Pasang Kikuli;Pasang Kitar;Pintso
;Charles Houston;1953;American;storm;10 days;7800;climber;Art Gilkey;Pete Schoening;fall;Gilkey;his
;1954;Italian;Karakoram;31 July 1954;Ardito Desio;two;Lino Lacedelli;Achille Compagnoni;Pakistani;Colonel;Muhammad Ata-ullah;1953;American;Walter Bonatti;Pakistani;porter;Amir Mehdi;8100;m;Lacedelli;Compagnoni;Lacedelli;Compagnoni;Mehdi;Bonatti;Mehdi;Bonatti;overnight;8,000;Bonatti;Mehdi;Mehdi;months;his;the 1950s;Lacedelli;Compagnoni;Italian;Compagnoni;Bonatti;Bonatti;Mehdi
;9 August 1977;23 years;Italian;Ichiro Yoshizawa;second;Ashraf Aman;first;Pakistani;climber;Japanese;Abruzzi Spur;1,500
;third;1978;East Face;Abruzzi;American;James Whittaker;Louis Reichardt;Jim Wickwire;John Roskelley;Rick Ridgeway;Wickwire;overnight;150;m;one;American;1938;team;forty years earlier
;first;climber;Czech;climber;Josef Rakoncaj;Rakoncaj;1983;Italian;Francesco Santon;second;31 July 1983;Three years later;5 July 1986;Agostino da Polenza;he
;first;Polish;climber;Wanda Rutkiewicz;23 June 1986;Liliane;Maurice Barrard;day;Liliane Barrard;19 July 1986
;1986;two;Polish;two;Polish;Line;Jerzy Kukuczka;Tadeusz Piotrowski;Piotrowski;two;his;January 2021
;Thirteen;1986;Disaster;six;13 August 1995;eleven;2008;disaster;January 2021
;2004
;2004;Spanish;climber;Carlos Soria Fontán;65
;2008
;1 August 2008;four;11;Ger McDonnell;first;Irish
;2009
;January 2021
;2010
;6 August 2010;Fredrik Ericsson;Gerlinde Kaltenbrunner;Ericsson;1000;m;Kaltenbrunner;her
;January 2021
;2011
;23 August 2011;four;Gerlinde Kaltenbrunner;first;14;eight-thousanders;Maxut Zhumayev;Vassiliy Pivtsov;eight-thousanders;fourth;Dariusz Załuski;Poland
;2012
;The year;Russian;first;winter;Vitaly Gorelik;pneumonia;Russian;summer;28;day;the year;30
;2013
;28 July 2013;two;New Zealanders;Marty Schmidt;Denali;his;guide;British;climber;Adrian Hayes;Facebook;his
;2014
;26 July 2014;first;Pakistani;six;Pakistani;three;Italian;60 Years Later;BBC;Previously;Pasang Lhamu Sherpa Akita;Maya Sherpa;Dawa Yangzum Sherpa;first;Nepali
;27 July 2014;Garrett Madison;three;American;six
;31 July 2014;Boyan Petrov;first;Bulgarian;8 days;Boyan;diabetes;7000
;2017
;28 July 2017;Vanessa O'Brien;12;Mingma Gyalje Sherpa;first;British;American;52 years old;Julie Tullis;Alison Hargreaves;two;British;1986;1995;She;John Snorri Sigurjónsson;Dawa Gyalje Sherpa;Dawa Yangzum Sherpa;second;his;Mingma Gyalje Sherpa;Fazal Ali;second;January 2021
;2018
;22 July 2018;Garrett Madison;first;American;climber;once;eight;nine;Nepali;four;Pakistani;two;he
;22 July 2018;Polish;mountaineer;runner;Andrzej Bargiel;first
;2019
;25 July 2019;Anja Blacha;first;German;She
;1987/1988;Andrzej Zawada;Pakistani;13;7;Canadians;4;2 March;Krzysztof Wielicki;Leszek Cichy;III;7,300;Roger Mear;Jean-Francois Gagnon;few days later;Hurricane
;2002/2003;Netia;Polish;Winter;fourteen;Krzysztof Wielicki;four;Kazakhstan;Uzbekistan;Georgia;North;Ridge;Marcin Kaczkan;Piotr Morawski;Denis Urubko;7,650;Kaczkan;Urubko;Kaczkan
;2011/2012;Russian;Nine;Russian;7,200;Vitaly Gorelik;Valery Shamalo;Nicholas Totmyanin;Gorelik;Gorelik;climber;pneumonia;cardiac arrest
;2017/2018;Polish;Winter;Krzysztof Wielicki;13;the end of December 2017;Cesen/Basque;6300;Abruzzi Spur;7400;m.;Denis Urubko;7600;m.;his;he
;2021;Ten;Chhang Dawa Sherpa;first;winter;January 16, 2021;Mingma Gyalje Sherpa;Nirmal Purja;Gelje Sherpa;Mingma David Sherpa;Mingma Tenzi Sherpa;Dawa Temba Sherpa;Pem Chhiri Sherpa;Kilu Pemba Sherpa;Dawa Tenjing Sherpa;Sona Sherpa;Nepal;Nirmal Purja;one;-40;Celsius;the same day;Spanish;Sergi Mingote;Camp III;he
;first;one-third;climber
;second;storms;several days;third;storm;first;winter;2021;Pakistani;January 2021
;75%;Abruzzi Spur;Pakistani;first;Prince;Luigi Amedeo;Duke;Abruzzi;1909;5400;m;two;House;Chimney;Bottleneck;one;2001;2002;2003
;1 August 2008;11;accidents;January 2021
;Abruzzi Spur;North;Ridge;Chinese;Shaksgam;River;Abruzzi;two;North;Ridge;Abruzzi;December 2020;—Camp IV;Eagle;Nest;7900;m;January 2021
;Japanese;North;Ridge;one;1990;Greg Child;Greg Mortimer;Steve Swenson;Camp 2;Japanese
;75%;one;East Face;North Face;2007;Denis Urubko;Serguey Samoilov;North;Ridge;2 October 2007;summer
;Abruzzi;first;Polish;Janusz Kurczab;1976;First;Louis Reichardt;James Wickwire;6 september 1978
;First;1981;Japanese;Negrotto Glacier;January 2021
;second;First;1986;Polish-Slovak;Jordi Corominas;Spain;climber;2004;he;January 2021
;Polish;Line;Central Rib;July 1986;Jerzy Kukuczka;Tadeusz Piotrowski;Piotrowski;first;Southwest Pillar;Hockey Stick;1000;Reinhold Messner;one;Kukuczka;Piotrowski;one
;Northwest Face;First;1990;Japanese;Chinese
;First;1991;French;Pierre Beghin;Christophe Profit;North;Ridge;Second;1995;American;8100;2;August before
;South-southeast;Basque;Abruzzi Spur;Polish;Black Pyramid;1986;Tomo Česen;8000;first;Basque;1994
;first;Russian;2007
;2004;28;47;year

;pulmonary edema;8000;m;climber;January 2021
;1991;Patrick Meyers;Roddam;Jim Wickwire;Louis Reichardt;first;Americans
;2000;American;Martin Campbell
;2012;American;Dave Ohlson;2009;100-year;Duke;Abruzzi;1909
;2012;2008;disaster;Nick Ryan
;1986;disaster
;1995;disaster
;2008;disaster
;6111;m;35.87318;76.57692;Broad Peak;Skyang Kangri;January 2021

;Concordia
;Gilgit;Baltistan
;Kangchenjunga;3rd;Everest
;eight-thousanders

;Pakistan

;Solar System


;http://www.himalaya-info.org/Map%20karakorum_baltoro.htm Himalaya-Info.org;German
;http://www.leica-geosystems.com/en/The-Himalayas-K2_2704.htm;1996;8614.27;0.6
;http://hermetic.com/crowley/confessions/chapter38.html;Aleister Crowley;1902
;first;1902;Italian;1954
;Pakistan
;http://www.omnimap.com/cgi/graphic.pl?images/for-topo/64-40851.jpg
;http://www.8000ers.com/cms/content/view/53/192/;December 2007
;http://www.mensjournal.com/k2;November 2008
;https://www.telegraph.co.uk/news/obituaries/sport-obituaries/5320510/Achille-Compagnoni.html;Achille Compagnoni;Daily
;https://www.telegraph.co.uk/news/obituaries/sport-obituaries/6255625/Dr-Charles-Houston.html;Charles Houston;Daily
;http://www.k2climb.net k2climb.net
