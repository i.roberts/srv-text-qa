{{Politics of Equatorial Guinea}}

The government's official policy is one of [[Non-Aligned Movement|nonalignment]]. In its search for assistance to meet the goal of national reconstruction, the government of Equatorial Guinea has established diplomatic relations with numerous European and [[Third World]] countries. Having [[History of Equatorial Guinea#Independence|achieved independence]] under UN sponsorship, Equatorial Guinea feels a special kinship with that organization. It became the 126th [[Member states of the United Nations|UN member]] on November 12, 1968. Equatorial Guinea currently serves as a non-permanent member on the [[United Nations Security Council]] with a two-year term ending in 2019.

==Bilateral relations==
===Africa===
{| class="wikitable sortable" style="width:100%; margin:auto;"
|-
! style="width:15%;"| Country
! style="width:12%;"| Formal relations established
! style="width:50%;"| Notes
|- valign="top"
|{{flag|Angola}}||<!--Start Date-->||

* Angola has an embassy in [[Malabo]].
* Equatorial Guinea has an embassy in [[Luanda]].
|- valign="top"
|{{flag|Cameroon}}||<!--Start Date-->||
Equatorial Guinea has cordial relations with neighbouring Cameroon, although there was criticism in Cameroon in 2000 about perceived mistreatment of Cameroonians working in Equatorial Guinea. Cameroon and Equatorial Guinea have an unresolved maritime border dispute. The majority [[Beti-Pahuin|Fang]] ethnic group of mainland Equatorial Guinea extends both north and south into the forests of Cameroon and Gabon. Cameroon exports some food products to Equatorial Guinea and imports oil from Equatorial Guinea for its refinery at nearby [[Limbe, Cameroon|Limbe]].

In December 2008, Equatorial Guinea security forces killed a Cameroonian fisherman and abducted two immigrants, Cameroon closed its border in response.<ref>{{cite web|url=https://2009-2017.state.gov/j/drl/rls/hrrpt/2008/af/118999.htm |title=2008 Human Rights Report |publisher=State.gov |date=2009-02-25 |access-date=2012-02-02}}</ref>

* Cameroon has an embassy in Malabo and a consulate in [[Bata, Equatorial Guinea|Bata]].
* Equatorial Guinea has an embassy in [[Yaoundé]] and a consulate-general in [[Ebolowa]] and a consulate in [[Douala]].
|- valign="top"
|{{flag|Chad}}||<!--Start Date-->||

* Chad has an embassy in Malabo.
* Equatorial Guinea has an embassy in [[N'Djamena]].
|- valign="top"
|{{flag|Congo}}||<!--Start Date-->||

* Congo has an embassy in Malabo.
* Equatorial Guinea has an embassy in [[Brazzaville]].
|- valign="top"
|{{flag|Egypt}}||<!--Start Date-->||

* Egypt has an embassy in Malabo.
* Equatorial Guinea has an embassy in [[Cairo]].
|- valign="top"
|{{flag|Gabon}}||<!--Start Date-->||

* Gabon has an embassy in Malabo.
* Equatorial Guinea has an embassy in [[Libreville]].
|- valign="top"
|{{flag|Ghana}}||<!--Start Date-->||

* Equatorial Guinea has an embassy in [[Accra]].
* Ghana has an embassy in Malabo.
|- valign="top"
|{{flag|Ivory Coast}}||<!--Start Date-->||

* Equatorial Guinea has an embassy in [[Abidjan]].
* Ivory Coast has an embassy in Malabo.
|- valign="top"
|{{flag|Morocco}}||<!--Start Date-->||

* Equatorial Guinea has an embassy in [[Rabat]].
* Morocco has an embassy in Malabo.
|- valign="top"
|{{flag|Nigeria}}||<!--Start Date-->||

Equatorial Guinea has warmer relations with Nigeria, and the Nigerian President made an official visit to Malabo in 2001. The two countries have delineated their offshore borders, which will facilitate development of nearby gas fields. In addition, many Nigerians work in Equatorial Guinea, as do immigrants from Cameroon and some West African states.
* Equatorial Guinea has an embassy in [[Abuja]] and consulates in [[Calabar]] and [[Lagos]].
* Nigeria has an embassy in Malabo and a consulate in Bata.
|- valign="top"
|{{flag|São Tomé and Príncipe}}||<!--Start Date-->||

* Equatorial Guinea has an embassy in [[São Tomé]].
* São Tomé and Príncipe has an embassy in Malabo.
|- valign="top"
|{{flag|South Africa}}||<!--Start Date-->||

* Equatorial Guinea has an embassy in [[Pretoria]].
* South Africa has an embassy in Malabo.
|}

===Americas===
{| class="wikitable sortable" style="width:100%; margin:auto;"
|-
! style="width:15%;"| Country
! style="width:12%;"| Formal relations established
! style="width:50%;"| Notes
|- valign="top"
|{{flag|Argentina}}||<!--Start Date-->||

* Argentina is accredited to Equatorial Guinea from its embassy in Abuja, Nigeria.
* Equatorial Guinea is accredited to Argentina from its embassy in Brasília, Brazil.
|- valign="top"
|{{flag|Brazil}}||<!--Start Date-->1974||

* Brazil has an embassy in Malabo.
* Equatorial Guinea has an embassy in [[Brasília]].
|- valign="top"
|{{flag|Canada}}||<!--Start Date-->1968||

* Canada is accredited to Equatorial Guinea from its high commission in Abuja, Nigeria.<ref>[https://www.canadainternational.gc.ca/nigeria/bilateral_relations_bilaterales/canada_equatorial-guinea.aspx?lang=eng Canada - Equatorial Guinea Relations]</ref>
* Equatorial Guinea is accredited to Canada from its Permanent Mission to the [[United Nations]] in [[New York City]].
|- valign="top"
|{{flag|Cuba}}||<!--Start Date-->||

* Cuba has an embassy in Malabo.
* Equatorial Guinea has an embassy in [[Havana]].
|- valign="top"
|{{flag|Mexico}}||<!--Start Date-->26 September 1975||See [[Equatorial Guinea–Mexico relations]]

* Equatorial Guinea is accredited to Mexico from its embassy in Washington, D.C., United States.<ref>[https://www.egembassydc.com/copy-of-la-embajada-1 Jurisdiction of Equatorial Guinea's embassy in the United States]{{Dead link|date=December 2019 |bot=InternetArchiveBot |fix-attempted=yes }}</ref>
* Mexico is accredited to Equatorial Guinea from its embassy in [[Abuja]], [[Nigeria]].<ref>[https://embamex.sre.gob.mx/nigeria/ Embassy of Mexico in Nigeria]</ref>
|- valign="top"
|{{flag|United States}}||<!--Start Date-->||See [[Equatorial Guinea–United States relations]]
[[File:Embassy of Equatorial Guinea - Washington, D.C.jpg|thumb|right|Embassy of Equatorial Guinea in Washington, D.C.]]
In 1995, the United States closed its embassy, ostensibly for budget reasons, though the ambassador of the time had been accused of [[witchcraft]], and had criticised the human rights situation. In 1996, offshore oil began flowing, and, with several US oil companies present in the country, the US reopened the embassy in October 2003. The US has sought to encourage the progress of human rights to the country by addressing its concerns directly to the government, as well as holding seminars for better police conduct and judicial conferences with US judges to improve the rule of law.<ref>[http://malabo.usembassy.gov/us_record_democracy_hr_support.html ] {{webarchive |url=https://web.archive.org/web/20101004195725/http://malabo.usembassy.gov/us_record_democracy_hr_support.html |date=October 4, 2010 }}</ref>

* Equatorial Guinea has an [[Embassy of Equatorial Guinea in Washington, D.C.|embassy in Washington, D.C.]] and a consulate-general in [[Houston]].<ref>[http://www.egembassydc.com/ Embassy of Equatorial Guinea in Washington, DC]</ref>
* United States has an embassy in Malabo.<ref>{{Cite web |url=http://malabo.usembassy.gov/ |title=Embassy of the United States in Malabo (in English and Spanish) |access-date=2006-03-06 |archive-url=https://web.archive.org/web/20110711091319/http://malabo.usembassy.gov/ |archive-date=2011-07-11 |url-status=dead }}</ref>
|- valign="top"
|{{flag|Venezuela}}||<!--Start Date-->7 May 1981||See [[Equatorial Guinea–Venezuela relations]]

* Equatorial Guinea has an embassy in [[Caracas]].
* Venezuela has an embassy in Malabo.
|}

===Asia===
{| class="wikitable sortable" style="width:100%; margin:auto;"
|-
! style="width:15%;"| Country
! style="width:12%;"| Formal relations established
! style="width:50%;"| Notes
|- valign="top"
|{{flag|Armenia}}||<!--Start Date-->19 May 1992||
Both countries established diplomatic relations on 19 May 1992.
|-
|{{Flag|Azerbaijan}}
|11 November 2004<ref name=":0">{{Cite web|title=Equatorial Guinea|url=https://mfa.gov.az/en/content/249/equatorial-guinea|access-date=2021-01-23|website=mfa.gov.az}}</ref>
|
* On November 11, 2004, Azerbaijan and Equatorial Guinea established diplomatic relations.<ref name=":0" />
|- valign="top"
|{{flag|China}}||<!--Start Date-->||See [[China–Equatorial Guinea relations]]
The People's Republic of China and the Republic of Equatorial Guinea established diplomatic relations on October 15, 1970.<ref>http://www.china.org.cn/english/features/focac/183538.htm</ref>
* China has an embassy in Malabo and a consulate-general in [[Bata, Equatorial Guinea|Bata]].
* Equatorial Guinea has an embassy in [[Beijing]].
|- valign="top"
|{{flag|India}}||<!--Start Date-->||See [[Equatorial Guinea–India relations]]

* Equatorial Guinea has an embassy in [[New Delhi]].
* India has an embassy in Malabo.
|- valign="top"
|{{flag|Israel}}||<!--Start Date-->||

* Equatorial Guinea has an embassy in [[Tel Aviv]].
* Israel is accredited to Equatorial Guinea from its embassy in Yaoundé, Cameroon.
|- valign="top"
|{{flag|Japan}}||<!--Start Date-->||

* Equatorial Guinea is accredited to Japan from its embassy in Beijing, China.
* Japan is accredited to Equatorial Guinea from its embassy in Libreville, Gabon.
|- valign="top"
|{{flag|North Korea}}||<!--Start Date-->30 January 1969||See [[Equatorial Guinea–North Korea relations]]

* Equatorial Guinea is accredited to North Korea from its embassy in Beijing, China.
* North Korea has an embassy in Malabo.
|- valign="top"
|{{flag|South Korea}}||<!--Start Date-->14 September 1979||

* Equatorial Guinea is accredited to South Korea from its embassy in Beijing, China.
* South Korea is accredited to Equatorial Guinea from its embassy in Libreville, Gabon.
|- valign="top"
|{{flag|Turkey}}||<!--Start Date-->1980<ref name="auto33">{{Cite web | url=http://www.mfa.gov.tr/relations-between-turkey-and-equatorial-guinea.en.mfa| title= Relations between Turkey and Equatorial Guinea}}</ref>||
* Equatorial Guinea has an embassy in [[Ankara]].<ref name="auto33"/>
* Turkey has an embassy in Malabo.<ref name="auto33"/>
* Trade volume between the two countries was 23.8 million USD in 2019 (Guinean exports/imports: 3.6/20.2 million USD).<ref name="auto33"/>
*There are direct flights from [[Istanbul]] to [[Malabo]] since 7 February 2020.<ref name="auto33"/>
|}

===Europe===
{| class="wikitable sortable" style="width:100%; margin:auto;"
|-
! style="width:15%;"| Country
! style="width:12%;"| Formal relations established
! style="width:50%;"| Notes
|- valign="top"
|{{flag|Belgium}}||<!--Start Date-->||

* Belgium is accredited to Equatorial Guinea from its embassy in [[Kinshasa]], DR Congo.
* Equatorial Guinea has an embassy in [[Brussels]].
|- valign="top"
|{{flag|France}}||<!--Start Date-->||

Equatorial Guinea is member of the Central African Economic and Monetary Union (CEMAC), which includes Cameroon, Central African Republic, Chad, Congo, and Gabon. It also is a member of the Franc zone. Parallel to the Equatoguinean rapprochement with its Francophone neighbors, France's role has significantly increased following Equatorial Guinea's entry into the [[CFA Franc Zone]] and the BEAC. French technical advisers work in the finance and planning ministries, and agreements have been signed for infrastructure development projects.
* Equatorial Guinea has an embassy in [[Paris]].
* France has an embassy in Malabo.
|- valign="top"
|{{flag|Germany}}||<!--Start Date-->||

* Equatorial Guinea has an embassy in [[Berlin]].
* Germany has an embassy in Malabo.
|- valign="top"
|{{flag|Holy See}}||<!--Start Date-->||

* Equatorial Guinea has an embassy to the Holy See based in Rome.
* Holy See has an apostolic nunciature accredited to Equatorial Guinea from Yaoundé, Cameroon.
|- valign="top"
|{{flag|Italy}}||<!--Start Date-->||

* Equatorial Guinea has an embassy in [[Rome]].
* Italy is accredited to Equatorial Guinea from its embassy in Yaoundé, Cameroon.
|- valign="top"
|{{flag|Poland}}||<!--Start Date-->||

* Equatorial Guinea is accredited to Poland from its embassy in Berlin, Germany.
* Poland is accredited to Equatorial Guinea from its embassy in Abuja, Nigeria.
|- valign="top"
|{{flag|Portugal}}||<!--Start Date-->||

* Equatorial Guinea has an embassy in [[Lisbon]].
* Portugal is accredited to Equatorial Guinea from its embassy in São Tomé and Príncipe.
|- valign="top"
|{{flag|Russia}}||<!--Start Date-->||See [[Equatorial Guinea–Russia relations]]

* Equatorial Guinea has an embassy in [[Moscow]].
* Russia is accredited to Equatorial Guinea from its embassy in Yaoundé, Cameroon.
|- valign="top"
|{{flag|Spain}}||<!--Start Date-->12 October 1968||See [[Equatorial Guinea–Spain relations]]

A transitional agreement, signed in October 1968, implemented a Spanish preindependence decision to assist [[Equatorial Guinea]] and provided for the temporary maintenance of Spanish military forces there. A dispute with [[President of Equatorial Guinea|President]] [[Francisco Macías Nguema]] in 1969 led to a request that all Spanish troops immediately depart, and many civilians left at the same time. Diplomatic relations between the two countries were never broken but were suspended by Spain in March 1977 in the wake of renewed disputes. After Macías' fall in 1979, President [[Teodoro Obiang Nguema Mbasogo]] asked for Spanish assistance, and since then, Spain has regained influence in Equatorial Guinea's diplomatic relations. The two countries signed permanent agreements for economic and technical cooperation, private concessions, and trade relations. President Obiang made an official visit to [[Madrid]] in March 2001, and senior Spanish Foreign Ministry officials visited [[Malabo]] during 2001 as well. Spain maintained a bilateral assistance program in Equatorial Guinea. Some Equato-Guinean opposition elements are based in Spain to the annoyance of the government in Malabo.

* Equatorial Guinea has an embassy in [[Madrid]] and a consulate in [[Las Palmas]].<ref>{{Cite web |url=https://www.guineamadrid.es/espagnol/ |title=Embassy of Equatorial Guinea in Spain (in French and Spanish) |access-date=2017-05-01 |archive-url=https://web.archive.org/web/20161229180853/http://www.guineamadrid.es/espagnol/ |archive-date=2016-12-29 |url-status=dead }}</ref>
* Spain has an embassy in Malabo and a consulate-general in [[Bata, Equatorial Guinea|Bata]].<ref>[http://www.exteriores.gob.es/Embajadas/Malabo/es/Paginas/inicio.aspx Embassy of Spain in Equatorial Guinea (in Spanish)]</ref>
|- valign="top"
|{{flag|United Kingdom}}||<!--Start Date-->||

* Equatorial Guinea has an embassy in [[London]].
* United Kingdom is accredited to Equatorial Guinea from its high commission in Yaoundé, Cameroon.
|}

==See also==
*[[List of diplomatic missions in Equatorial Guinea]]
*[[List of diplomatic missions of Equatorial Guinea]]

==References==
{{reflist}}

==External links==
*[https://web.archive.org/web/20180118184857/http://www.mae-ge.org/ Ministry of Foreign Affairs of Equatorial Guinea]
*[https://web.archive.org/web/20060213191021/http://www.embarege-londres.org/  Embassy of Equatorial Guinea in London, United Kingdom]
*[https://web.archive.org/web/20110711091319/http://malabo.usembassy.gov/ United States Embassy in Malabo]
*[http://www.ecaligiuri.com Honorary Consul of Equatorial Guinea and Investment Opportunities in Bucharest, Romania] (Spanish)
*[https://web.archive.org/web/20070228141309/http://www.ecaligiuri.com/ministro.pdf  Curriculum Vitae of Equatorial Guinea Foreign Minister H.E. Don Pastor Micha Ondo Bile (Spanish)]

{{Foreign relations of Equatorial Guinea}}
{{Africa in topic|Foreign relations of}}

{{DEFAULTSORT:Foreign Relations Of Equatorial Guinea}}
[[Category:Foreign relations of Equatorial Guinea| ]]