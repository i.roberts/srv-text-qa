{{Decadebox BC|3}}
This article concerns the period '''39 BC – 30 BC'''.
{{Events by year for decade BC|3}}

==Significant people==
* [[Mark Antony]], Roman politician and general ([[83 BC|83]]–[[30 BC]])
* [[Pharaoh]] [[Cleopatra VII of Egypt]] (lived [[70 BC|70]]/[[69 BC|69]]–[[30 BC]], reigned [[51 BC|51]]–[[30 BC]])
* Gaius Julius Caesar Octavianus, known in English as [[Caesar Augustus|Octavian]], Roman politician and general ([[62 BC]]–[[AD 14]])
* [[Pharaoh]] [[Ptolemy XV]] [[Caesarion]] (lived [[47 BC|47]]–[[30 BC]], reigned [[44 BC|44]]–[[30 BC]])

{{Births and deaths by year for decade|-3}}

==References==
{{Reflist}}

{{DEFAULTSORT:30s Bc}}
[[Category:30s BC| ]]