{{short description|Color}}
{{About|the color||violet (disambiguation){{!}}Violet}}
{{use dmy dates|date=August 2019}}
{{Use American English|date=September 2020}}
{{Infobox colour
| title=Violet
| textcolor=white
| hex=8000FF
| size=243
| color_border=#AAAAAA
| color=#F8F8F8 
| image={{photomontage
| photo1a=Amatista Laye 2.jpg
| photo2a=Chasublepurple.jpg
| photo2b=Manganese violet.jpg
| photo3a=Devon Violets. Viola odorata (33624079715).jpg
| size=243
| color_border=#AAAAAA
| color=#567890
}}
| wavelength=380–450	
| frequency=790–666
| r=127|g=  0|b=255
| c= 50|m=100|y=  0|k=  0
| h=270|s=100|v=100
| source=99Colors<ref>{{cite web|url=http://www.99colors.net/name/violet|title=Color Violet|access-date=18 May 2018}}</ref>
}}

'''Violet''' is the [[color]] of [[light]] at the short [[Spectral color|wavelength]] end of the [[visible spectrum]], between [[blue]] and invisible [[ultraviolet]]. It is one of the seven colors that [[Isaac Newton]] labeled when dividing the spectrum of visible light in 1672. Violet light has a wavelength between approximately 380 and 450 nanometers.<ref name=":2">{{cite web|url=http://hyperphysics.phy-astr.gsu.edu/hbase/vision/specol.html|title=Spectral Colors|website=HyperPhysics site|author=Georgia State University Department of Physics and Astronomy|access-date=20 October 2017}}</ref> The color's name is derived from the [[Viola (plant)|violet]] flower.<ref name=":0">{{Cite web|url=https://www.oed.com/view/Entry/223648|title=violet, n.1|website=OED Online|publisher=Oxford University Press|access-date=2020-04-06}}</ref><ref name=":1">{{Cite web|url=https://unabridged.merriam-webster.com/unabridged/violet|title=Violet|website=Webster's Third New International Dictionary, Unabridged|access-date=2020-04-06}}</ref> 

In the [[RGB color model]] used in computer and television screens, violet is produced by [[Additive color|mixing]] red and blue light, with more blue than red. In the [[RYB color model]] historically used by painters, violet is created with a combination of red and blue pigments and is located between blue and [[purple]] on the [[color wheel]]. In the [[CMYK color model]] used in printing, violet is created with a combination of magenta and cyan pigments, with more magenta than cyan.

Violet is closely associated with [[purple]]. In [[optics]], violet is a [[spectral color]] (referring to the color of different single wavelengths of light) and purple is the color of various combinations of red and blue (or violet) light,<ref name="gilbert2" /><ref name=":3" /> some of which humans perceive as similar to violet. In common usage, both refer to colors that are between blue and red in [[hue]], with [[Shades of violet|violets]] closer to blue and [[Shades of purple|purples]] closer to red.<ref name=":0" /><ref name=":1" /> Similarly, in the traditional painters' color wheel, violet and purple are both placed between blue and red, with violet closer to blue.

Violet and purple have a long history of association with royalty, originally because [[Tyrian purple]] dye was extremely expensive in antiquity.<ref>{{Cite news|last=Dunn|first=Casey|url=https://www.nytimes.com/2013/10/09/science/the-color-of-royalty-bestowed-by-science-and-snails.html|title=The Color of Royalty, Bestowed by Science and Snails|date=2013-10-09|work=The New York Times|access-date=2020-04-04|language=en-US|issn=0362-4331}}</ref> The emperors of Rome wore purple togas, as did the Byzantine emperors. During the [[Middle Ages]] violet was worn by [[bishop]]s and university professors and was often used in art as the color of the robes of the [[Mary, mother of Jesus|Virgin Mary]].{{Citation needed|date=April 2020}} In [[Chinese painting]], the color violet represents the "unity transcending the duality of [[Yin and yang]]" and "the ultimate harmony of the [[Cosmos|universe]]".<ref name="ReferenceA">Varichon, Anne ''Colors:What They Mean and How to Make Them'' New York:2006 Abrams Page 138</ref> In [[Hinduism]] and [[Buddhism]] violet is associated with the [[crown chakra|Crown Chakra]].<ref name="color-wheel-artist.com">https://web.archive.org/web/20170508232015/http://www.color-wheel-artist.com/meanings-of-violet.html</ref> According to surveys in Europe and the United States, violet is the color people most often associate with extravagance and individualism, the unconventional, the artificial, and ambiguity.<ref name="Eva Heller p. 4">Eva Heller, ''Psychologie de la couleur: effets et symboliques''. p.&nbsp;4. "La plus individualist et extravagant des coulours." (associated by 26 percent of respondents to survey with "extravagance", by 22 percent with "individualism" , 24 percent with "vanity", 21 percent with "ambiguity". </ref>

==Etymology and definitions==
[[File:Line of purples.png|thumb|The [[line of purples]] circled on the [[CIE chromaticity diagram]]. The bottom left of curved edge is violet. Points near and along the circled edge are purple.]]
The word ''violet'' as a color name derives from the [[Middle English]] and [[Old French]] ''violete'', in turn from the Latin ''viola'', names of the [[violet (flower)|violet]] flower.<ref name=":0" /><ref name=":1" /> The first recorded use as a color name in English was in 1370.<ref>Maerz and Paul ''A Dictionary of Color'' New York: 1930 McGraw-Hill Page 207</ref>

===Violet and purple===
Violet is closely associated with [[purple]]. In [[optics]], violet is a [[spectral color]]: It refers to the color of any different single wavelength of light on the short wavelength end of the visible spectrum (between approximately 380 and 450 nanometers),<ref name=":2" /> whereas purple is the color of various combinations of red, blue, and violet light,<ref name="gilbert2">{{cite book|author=P. U.P. A Gilbert and Willy Haeberli|url=https://books.google.com/books?id=qSRqXvZ67lQC&pg=PA112|title=Physics in the Arts|publisher=Academic Press|year=2008|isbn=978-0-12-374150-9|page=112}}</ref><ref name=":3">{{cite book|author=Louis Bevier Spinney|url=https://archive.org/details/atextbookphysic00spingoog|title=A Text-book of Physics|publisher=Macmillan Co.|year=1911|page=[https://archive.org/details/atextbookphysic00spingoog/page/n636 573]}}</ref> some of which humans perceive as similar to violet. In common usage, both refer to colors that are between blue and red in [[hue]], with [[Shades of violet|violets]] closer to blue and [[Shades of purple|purples]] closer to red.<ref name=":0" /><ref name=":1" /> In the traditional [[color wheel]] used by painters, violet and purple are both placed between red and blue, with purple being closer to red.

In humans, the L (red) [[Cone cell|cone]] in the eye is primarily sensitive to long wavelength light in the yellow-red region of the spectrum, but is also somewhat sensitive to the shorter wavelength violet light that primarily stimulates the S (blue) cone. As a result, when violet light strikes the eye, the S-cone is stimulated strongly and the L-cone is stimulated weakly. Accordingly, strong blue light mixed with weaker red light can mimic this pattern of stimulation, causing humans to perceive colors that have the same hue as violet, but with lower [[Colorfulness|saturation]].{{Citation need|date=April 2020}} Computer and television screens rely on this phenomenon. Because they use the [[RGB color model]], they cannot produce violet light and instead substitute purple, combining blue light at high intensity with red light of approximately half the intensity.

==In science==
===Optics===
[[File:Linear visible spectrum.svg|center|400px]]
Violet is at one end of the [[Visible spectrum|spectrum of visible light]], between [[blue]] light, which has a longer wavelength, and [[ultraviolet]] light, which has a shorter wavelength and is not visible to humans. Violet encompasses light with a wavelength of approximately 380 to 450 nanometers. Violet objects often appear dark, because human vision is relatively insensitive to those wavelengths.{{Citation needed|date=April 2020}}

===Chemistry – pigments and dyes===
The earliest violet pigments used by humans, found in prehistoric cave paintings, were made from the minerals [[manganese]] and [[hematite]]. Manganese is still used today by the [[Aranda people]], a group of [[indigenous Australians]], as a traditional pigment for coloring the skin during rituals. It is also used by the [[Hopi]] Indians of [[Arizona]] to color ritual objects.

The most famous violet-purple dye in the ancient world was [[Tyrian purple]], made from a type of sea snail called the [[murex]], found around the Mediterranean.

In western [[Polynesia]], residents of the islands made a violet dye similar to Tyrian purple from the [[sea urchin]]. In Central America, the inhabitants made a dye from a different sea snail, the [[purpura (gastropod)|purpura]], found on the coasts of [[Costa Rica]] and [[Nicaragua]]. The Mayans used this color to dye fabric for religious ceremonies, and the Aztecs used it for paintings of ideograms, where it symbolized royalty.<ref name="Anne Carichon 2000 p. 133">Anne Carichon (2000), ''Couleurs: pigments et teintures dans les mains des peuples''. p.&nbsp;133.</ref>

During the Middle Ages, most artists made purple or violet on their paintings by combining red and blue pigments; usually blue azurite or lapis-lazuli with [[red ochre]], [[cinnabar]] or [[minium]]. They also combined lake colors made by mixing dye with powder; using [[woad]] or [[indigo]] dye for the blue, and dye made from [[cochineal]] for the red.<ref name="Anne Carichon 2000 p.&nbsp;133"/>

[[Orcein]], or ''purple moss'', was another common violet dye. It was known to the ancient Greeks and Hebrews, was made from a Mediterranean [[lichen]] called archil or dyer's moss ([[Roccella tinctoria]]), combined with an [[ammoniac]], usually urine. Orcein began to achieve popularity again in the 19th century, when violet and purple became the color of demi-mourning, worn after a widow or widower had worn black for a certain time, before he or she returned to wearing ordinary colors.<ref>Anne Carichon (2000), ''Couleurs: pigments et teintures dans les mains des peuples''. p.&nbsp;144.</ref>

In the 18th century, chemists in England, France and Germany began to create the first synthetic dyes. Two synthetic purple dyes were invented at about the same time. '''Cudbear''' is a [[dye]] extracted from [[orchil]] [[lichen]]s that can be used to dye [[wool]] and [[silk]], without the use of [[mordant]]. Cudbear was developed by Dr. Cuthbert Gordon of [[Scotland]]: production began in 1758, The lichen is first boiled in a solution of [[ammonium carbonate]]. The mixture is then cooled and [[ammonia]] is added and the mixture is kept damp for 3–4 weeks. Then the lichen is dried and ground to powder. The manufacture details were carefully protected, with a ten-feet high wall being built around the manufacturing facility, and staff consisting of Highlanders sworn to secrecy.

'''French purple''' was developed in France at about the same time. The lichen is extracted by urine or ammonia. Then the extract is acidified, the dissolved dye precipitates and is washed. Then it is dissolved in ammonia again, the solution is heated in air until it becomes purple, then it is precipitated with [[calcium chloride]]; the resulting dye was more solid and stable than other purples.

'''[[Cobalt violet]]''' is a synthetic pigment that was invented in the second half of the 19th century, and is made by a similar process as [[cobalt blue]], [[cerulean blue]] and [[cobalt green]]. It is the violet pigment most commonly used today by artists, along with [[manganese violet]].

'''[[Mauveine]]''', also known as '''[[aniline]] [[purple]]''' and '''Perkin's [[mauve]]''', was the first synthetic [[organic chemistry|organic chemical]] dye,<ref>{{Cite journal|author=Hubner K|year=2006|title=History – 150 Years of mauveine|journal=Chemie in Unserer Zeit|volume=40|issue=4|pages=274–275|doi=10.1002/ciuz.200690054}}</ref><ref>{{Cite journal|author=Anthony S. Travis|year=1990|title=Perkin's Mauve: Ancestor of the Organic Chemical Industry|journal=Technology and Culture|volume=31|issue=1|pages=51–82|doi=10.2307/3105760|jstor=3105760}}</ref> discovered [[serendipity|serendipitously]] in 1856. Its chemical name is 3-amino-2,±9-dimethyl-5-phenyl-7-(p-tolylamino) phenazinium acetate.

In the 1950s, a new family of violet synthetic organic pigments called [[quinacridone]] came onto the market. It had originally been discovered in 1896, but were not synthetized until 1936, and not manufactured until the 1950s. The colors in the group range from deep red to violet in color, and have the molecular formula C<sub>20</sub>H<sub>12</sub>N<sub>2</sub>O<sub>2</sub>. They have strong resistance to sunlight and washing, and are used in oil paints, water colors, and acrylics, as well as in automobile coatings and other industrial coatings.<gallery mode="packed" heights="220">
File:Amatista Laye 2.jpg|In [[amethyst]], the violet color arises from an impurity of iron in the quartz.
File:Pigment Violet 29.svg|Chemical structure of [[Pigment Violet 29|pigment violet 29]]. Violet pigments typically have several rings.
File:Manganese violet.jpg|Manganese violet, a popular inorganic pigment.
</gallery>

===Zoology===
<gallery heights="220" mode="packed">
File:Messina Straits Argyropelecus hemigymnus.jpg|The [[marine hatchetfish]] (here eating a small crustacean) lives in extreme depths.
File:PurpleUrchinPuertoVG.JPG|The [[purple sea urchin]].
File:Xylocopa violacea-Abeille charpentière-201606102.jpg|The violet carpenter bee ([[Xylocopa violacea]]) is one of the largest bees in [[Europe]].
File:Cinnyricinclus leucogaster - 20080321.jpg|The [[violet-backed starling]] is found in [[Sub-Saharan Africa]].
File:Violet Sabrewing JCB.jpg|The [[violet sabrewing]] is found in [[Central America]].
File:Amazona imperialis -Roseau -Dominica -aviary-6a-3c.jpg|The [[imperial amazon]] parrot is featured on the national flag of [[Dominica]], making it the only national flag in the world with a violet color.
</gallery>

===Botany===
<gallery heights="200" mode="packed">
File:Lobelia (aka).jpg|[[Lobelia]]
File:- Crocus -.jpg|[[Crocus]] flowers.
File:Royal Botanical Gardens Lilac Celebration.JPG|[[Lilac]] flowers
File:Pensées violettes et noires.JPG|[[Pansy]] flowers.
File:Viole.jpg|[[Sweet Violet|Sweet violet]] flowers.
File:Iris sanguinea 2007-05-13 361.jpg|The [[iris (plant)|iris]] flower takes its name from the [[Greek language|Greek]] word for [[rainbow]].
File:Vaucluse lavanda.jpg|[[Lavender]] fields in the [[Vaucluse]], in [[Provence]], France
File:Glycines (Wisteria) (2).jpg|alt=Wisteria blooms are a light violet color.|[[Wisteria]] blooms are a light violet color.
File:Aubergine.jpg|An [[eggplant]].
</gallery>

==In history and art==
===Prehistory and antiquity===
Violet is one of the oldest colors used by humans. Traces of very dark violet, made by grinding the mineral [[manganese]], mixed with water or animal fat and then brushed on the cave wall or applied with the fingers, are found in the [[prehistoric cave art]] in [[Pech Merle]], in France, dating back about twenty-five thousand years. It has also been found in the [[cave of Altamira]] and [[Lascaux]].<ref>Phillip Ball (2001), ''Bright earth- Art and the Invention of Colour'', p.&nbsp;84</ref> It was sometimes used as an alternative to black charcoal. Sticks of manganese, used for drawing, have been found at sites occupied by [[Neanderthal]]s in France and Israel. From the grinding tools at various sites, it appears it may also have been used to color the body and to decorate animal skins.

More recently, the earliest dates on cave paintings have been pushed back farther than 35,000 years. Hand paintings on rock walls in Australia may be even older, dating back as far as 50,000 years.

Berries of the genus [[rubus]], such as [[Blackberry|blackberries]], were a common source of dyes in antiquity. The ancient Egyptians made a kind of violet dye by combining the juice of the [[mulberry]] with crushed green grapes. The Roman historian [[Pliny the Elder]] reported that the [[Gauls]] used a violet dye made from [[bilberry]] to color the clothing of slaves. These dyes made a satisfactory purple, but it faded quickly in sunlight and when washed.<ref>Anne Varichon (2000), ''Couleurs: pigments et teintures dans les mains des peuples'', p.&nbsp;146–148</ref>

===The Middle Ages and the Renaissance===
Violet and purple retained their status as the color of emperors and princes of the church throughout the long rule of the [[Byzantine Empire]].

While violet was worn less frequently by Medieval and Renaissance kings and princes, it was worn by the professors of many of Europe's new universities. Their robes were modeled after those of the clergy, and they often wore square violet caps and violet robes, or black robes with violet trim.

Violet also played an important part in the religious paintings of the Renaissance. Angels and the [[Virgin Mary]] were often portrayed wearing violet robes. The 15th-century Florentine painter [[Cennino Cennini]] advised artists: "If you want to make a lovely violet colour, take fine lacca, ultramarine blue (the same amount of the one as of the other)..." For fresco painters, he advised a less-expensive version, made of a mixture of blue indigo and red [[hematite]].<ref>Lara Broecke, ''Cennino cennini's ''Il Libro dell'Arte'': a New English Translation and Commentary with Italian Transcription'', Archetype 2015, p. 115</ref>

<gallery widths="220px" heights="220px">
File:The Wilton Diptych (Right).jpg|The [[Wilton Diptych]] (1395), painted for King [[Richard II]].
File:Rafael - Ressurreição de Cristo (detalhe - anjo).jpg|A violet-clad angel from the ''[[Resurrection of Christ (Raphael)|Resurrection of Christ]]'' by Raphael (1483–1520).
</gallery>

===18th and 19th centuries===
In the 18th century, violet was a color worn by royalty, aristocrats and the wealthy, and by both men and women. Good-quality violet fabric was expensive, and beyond the reach of ordinary people.

The first [[cobalt violet]], the intensely red-violet cobalt arsenate, was highly toxic. Although it persisted in some paint lines into the twentieth-century, it was displaced by less toxic cobalt compounds such as cobalt phosphate. Cobalt violet appeared in the second half of the 19th century, broadening the palette of artists. Cobalt violet was used by [[Paul Signac]] (1863–1935), [[Claude Monet]] (1840–1926), and [[Georges Seurat]] (1859–1891).<ref>Isabel Roelofs (2012), ''La couleur expliquée aux artistes'', p.&nbsp;52–53.</ref> Today, cobalt ammonium phosphate, cobalt lithium phosphate, and cobalt phosphate are available for use by artists. Cobalt ammonium phosphate is the most reddish of the three. Cobalt phosphate is available in two varieties — a deep less saturated blueish type and a lighter and brighter somewhat more reddish type. Cobalt lithium phosphate is a saturated lighter-valued bluish violet. A color similar to cobalt ammonium phosphate, cobalt magnesium borate, was introduced in the later twentieth-century but was not deemed sufficiently lightfast for artistic use. Cobalt violet is the only truly lightfast violet pigment with relatively strong color saturation. All other light-stable violet pigments are dull by comparison. However, the high price of the pigment and the toxicity of cobalt has limited its use.

[[Vincent van Gogh]] (1853–1890) was an avid student of color theory. He used violet in many of his paintings of the 1880s, including his paintings of irises and the swirling and mysterious skies of his starry night paintings, and often combined it with its [[complementary color]], yellow. In his painting of [[Bedroom in Arles|his bedroom in Arles]] (1888), he used several sets of complementary colors; violet and yellow, red and green, and orange and blue. In a letter about the painting to his brother Theo, he wrote, "The color here...should be suggestive of sleep and repose in general....The walls are a pale violet. The floor is of red tiles. The wood of the bed and the chairs are fresh butter yellow, the sheet and the pillows light lemon green. The bedspread bright scarlet. The window green. The bed table orange. The bowl blue. The doors lilac....The painting should rest the head or the imagination."<ref>John Gage (2006), ''La Couleur dans l'art'', p.&nbsp;50–51. Citing Letter 554 from Van Gogh to Theo. (translation of excerpt by D.R. Siefkin)</ref>

In 1856, a young British chemist named [[William Henry Perkin]] was trying to make a synthetic [[quinine]]. His experiments produced instead an unexpected residue, which turned out to be the first synthetic [[aniline dye]], a deep violet color called [[mauveine]], or abbreviated simply to [[mauve]] (the dye being named after the lighter color of the mallow [mauve] flower). Used to dye clothes, it became extremely fashionable among the nobility and upper classes in Europe, particularly after [[Queen Victoria]] wore a silk gown dyed with mauveine to the Royal Exhibition of 1862. Prior to Perkin's discovery, mauve was a color which only the aristocracy and rich could afford to wear. Perkin developed an industrial process, built a factory, and produced the dye by the ton, so almost anyone could wear mauve. It was the first of a series of modern industrial dyes which completely transformed both the chemical industry and fashion.<ref>{{cite book|author=Garfield, S.|year=2000|title=Mauve: How One Man Invented a Colour That Changed the World|publisher=Faber and Faber, London, UK|isbn=978-0-571-20197-6}}</ref>

<gallery widths="220px" heights="220px">
File:Charles de Bourbon, futur Carlos III.jpg|Charles de Bourbon, the future King [[Carlos III of Spain]] (1725).
File:Rokotov ekaterina.jpg|Portrait of Empress [[Catherine the Great]] of Russia, by [[Fyodor Rokotov]]. (State Hermitage Museum).
File:Arthur Hughes - April Love - Google Art Project.jpg|In England, [[pre-Raphaelite]] painters like [[Arthur Hughes (artist)|Arthur Hughes]] were particularly enchanted by purple and violet. This is ''[[April Love (painting)|April Love]]'' (1856).
File:Whistler James Nocturne Trafalgar Square Chelsea Snow 1876.jpg|''Nocturne: Trafalgar Square Chelsea Snow'' (1876) by [[James McNeil Whistler]], used violet to create a wintery mood.
File:CarolineRemy-Renoir.jpg|''Portrait of Caroline Remy de Guebhard'', by [[Pierre-Auguste Renoir]] (1841–1919). Mauve became a popular fashion color after the invention of the synthetic dye in 1856.
File:Van Gogh Irises in NYC partial.JPG|''Irises'' by [[Vincent van Gogh]] (1889), [[Metropolitan Museum of Art]].
File:VanGogh-starry night.jpg|''[[The Starry Night]]'', by [[Vincent van Gogh]] (1889), [[Museum of Modern Art]].
</gallery>

===20th and 21st centuries===
[[File:Five Presidents Oval Office.jpg|thumb|Five presidents in the oval office. The two more recent presidents, [[George W. Bush]] and [[Barack Obama]], are wearing violet ties.]]
Violet or purple neckties became popular at the end of the first decade of the 21st century, particularly among political and business leaders.{{Citation needed|date=April 2020}}

==In culture==
===Cultural associations===
====In Western culture====
=====Popularity of the color=====
*In Europe and America, violet is not a popular color; in a European survey, only three percent of men and women rated it as their favorite color, ranking it behind blue, green, red, black and yellow (in that order), and tied with orange. Ten percent of respondents rated it their least favorite color; only brown, pink and gray were more unpopular.<ref name="Eva Heller p. 4"/>

=====The color of royalty and luxury=====
*Because of their status as the color of Roman emperors, and as colors worn by monarchs and princes, the colors violet and purple are often associated with luxury. Certain luxury goods, such as watches and jewelry, are often placed in boxes lined with violet velvet, since violet is the complementary color of yellow, and shows gold to best advantage.

=====Vanity, extravagance, and individualism=====
*While violet is the color of humility in the symbolism of the Catholic Church, it has exactly the opposite meaning in general society. A European poll in 2000 showed it was the color most commonly associated with vanity.<ref>Eva Heller, ''Psychologie de la couleur: effets et symboliques'', p.&nbsp;167.</ref> As a color that rarely exists in nature, and a color which by its nature attracts attention, it is seen as a color of individualism and extravagance.

=====Ambiguity and ambivalence=====
*Surveys show that violet and purple are the colors most associated with ambiguity and ambivalence.

====In Asian culture====
[[File:Jidai Matsuri 2009 161.jpg|thumb|A Japanese woman in the kimono style popular in the [[Heian period]] (794–1185) with a violet head covering.]]
*In Japan, violet was a popular color introduced into Japanese dress during the [[Heian Period]] (794–1185). The dye was made from the root of the alkanet plant ([[Anchusa officinalis]]), known as ''murasaki'' in Japanese. At about the same time, Japanese painters began to use a pigment made from the same plant.<ref>Anne Varichon, ''Couleurs: pigments et teintures dans les mains des peuples'', p.&nbsp;139</ref>

===New Age===
*The "[[New Age]] Prophetess", [[Alice Bailey]], in her system called the [[Seven Rays]] which classifies humans into seven different metaphysical [[psychological types]], the "seventh ray" of "Ceremonial Order" is represented by the color violet. People who have this metaphysical psychological type are said to be "on the Violet Ray".<ref>{{cite book|last=Bailey|first=Alice A.|author-link=Alice Bailey|title=The Seven Rays of Life|location=New York|year=1995|publisher=Lucis Publishing Company|isbn=978-0-85330-142-4}}</ref>
*In the [[Ascended Master Teachings]], the color violet is used to represent the [[Ascended Master]] [[St. Germain (Theosophy)|St. Germain]].<ref name="Channeling Pages 80-90">"St. Germain" ([[Channeling (mediumistic)|dictated]] through [[Elizabeth Clare Prophet]]) ''Studies in Alchemy: the Science of Self-Transformation'' 1974:Colorado Springs, Colorado, USA Summit Lighthouse Pages 80–90 [Occult] Biographical sketch of St. Germain</ref>
**''The Invocation of the Violet Flame'' is a system of [[meditation]] practice used in the [["I AM" Activity]] and by the [[Church Universal and Triumphant]] (both Ascended Master Teaching religions).

===Religion===
* In the Roman Catholic church, violet is worn by [[bishop]]s and [[archbishop]]s, red by [[Cardinal (Catholicism)|cardinals]], and white by the [[Pope]]. Ordinary priests wear black.
* In the Roman Catholic and many other [[Western Christianity|Western churches]], violet is the [[Liturgical colours|liturgical color]] of [[Advent]] and [[Lent]], which respectively celebrate the expectant waiting and preparation for the celebration of the [[Crucifixion of Jesus]] and the time for [[penance]] and/or [[mourning]].
* There is a stained glass window created in the early 1920s in the [[Cathedral of Our Lady of the Angels]] in Los Angeles depicting [[God the Father in Western art|God the Father]] wearing a violet robe.<ref>[[:File:LA Cathedral Mausoleum Coronation of Mary.jpg|Stained glass window in the Cathedral of the Angels in Los Angeles, California depicting God the Father wearing a violet robe:]]</ref>
* After the [[Vatican II Council]], which modified many of the rules of the Catholic church, priests began to wear violet robes when celebrating masses for the dead. Black was no longer used, since it was the color of mourning outside the church, and was felt to be inappropriate in a religious ceremony.<ref>Eva Heller, ''Psychologie de la couleur: effets et symboliques''. p.&nbsp;166,</ref>
* In [[Hinduism]], violet is used to symbolically represent the seventh, crown [[chakra]] ([[Sahasrara]]).<ref>Stevens, Samantha. The Seven Rays: a Universal Guide to the Archangels. City: Insomniac Press, 2004. {{ISBN|1-894663-49-7}} p.&nbsp;24</ref>

===Politics===
[[File:Susan B Anthony-3c.jpg|thumb|The [[Susan B. Anthony]] stamp (1936), was the reddish tone of violet known as [[red-violet]] since violet was a color that represented the [[Women's Suffrage]] movement.]]

*At the beginning of the 20th century, violet, green and white were the colors of the [[women's suffrage]] movement in the United States and Britain, seeking the right to vote for women. The colors were said to represent liberty and dignity.<ref>Eva Heller, ''Psychologie de la couleur: effets et symboliques''. illustration 75.</ref><ref>{{cite web|last=LaCroix|first=Allison|title=The National Woman's Party And the Meaning Behind Their Purple, White, and Gold Textiles|url=http://nationalwomansparty.org/the-national-womans-party-and-the-meaning-behind-their-purple-white-and-gold-textiles/|publisher=[[Belmont-Paul Women's Equality National Monument]]|date=October 2015|access-date=30 July 2018}}</ref> For this reason, the postage stamp issued in 1936 to honor [[Susan B. Anthony]], a prominent leader of the suffrage movement in the United States, was colored the reddish tone of violet known as [[red-violet]].
*The [[pan-European identity|pan-European]] movement [[Volt Europa]], and its national subsidiary parties are using violet as a uniform design.<ref>https://www.volteuropa.org/</ref>
*There is a small [[New Age]] political party in [[Germany]] with about 1,150 members called ''The Violet Party''. The party believes in [[direct democracy]], a [[guaranteed minimum income]], and that politics should be based on [[Spirituality|spiritual values]]. "The Violet Party" was founded in [[Dortmund]], Germany in 2001.<ref>{{Cite web|url=http://die-violetten.de/|title=Die Violetten - Neue Ideen in der Politik|website=Die Violetten}}</ref>

===Social movement===
Violet flowers and their color became symbolically associated with [[lesbian]] love.<ref>{{cite book|title=The Alyson Almanac: A Treasury of Information for the Gay and Lesbian Community|year=1989|publisher=[[Alyson Publications]]|location=Boston, Massachusetts|page=[https://archive.org/details/alysonalmanactr00bost/page/100 100]|chapter=Gay Symbols Through the Ages|isbn=0-932870-19-8|chapter-url=https://archive.org/details/alysonalmanactr00bost/page/100}}</ref> It was used as a [[LGBT symbols#Violets|special code]] by lesbians and bisexual women for self-identification and also to communicate support for the sexual preference.<ref>{{cite book|last=Myers|first=JoAnne|title=The A to Z of the Lesbian Liberation Movement: Still the Rage (The A to Z Guide Series, No. 73 )|url=https://archive.org/details/tozlesbianlibera00myer|url-access=limited|year=2003|publisher=[[The Scarecrow Press]]|location=Lanham, Maryland|page=[https://archive.org/details/tozlesbianlibera00myer/page/n292 242]|edition=1st|isbn=978-0-8108-6811-3}}</ref><ref>{{cite book|last1=Horak|first1=Laura|title=Girls Will Be Boys: Cross-Dressed Women, Lesbians, and American Cinema, 1908-1934|date=2016|publisher=[[Rutgers University Press]]|pages=143–144|chapter=Lesbians Take Center Stage: The Captive (1926–1928)|isbn=978-0-8135-7483-7}}</ref> This connection originates from the poet [[Sappho]] and fragments of her poems. In one poem, she describes a lost love wearing a garland of "violet tiaras, braided rosebuds, dill and crocus twined around" her neck.<ref>{{cite book|last1=Barnard|first1=Mary|title=Sappho: A New Translation|url=https://archive.org/details/sappho00sapp|url-access=registration|year=1958|publisher=[[University of California Press]]|page=[https://archive.org/details/sappho00sapp/page/n62 42]|edition=1st|lccn=58006520}}</ref> In another fragment, she recalls her lover as having "put around yourself [many wreaths] of violets and roses."<ref>{{cite book|last1=Collecott|first1=Diana|title=H.D. and Sapphic Modernism 1910–1950|year=1999|publisher=[[Cambridge University Press]]|location=Cambridge, UK|page=216|edition=1st|isbn=978-0-521-55078-9}}</ref><ref>{{cite book|last1=Fantham|first1=Elaine|last2=Foley|first2=Helene Peet|last3=Kampen|first3=Natalie Boymel|last4=Pomeroy|first4=Sarah B.|last5=Shapiro|first5=H. A.|title=Women in the Classical World: Image and Text|year=1994|publisher=[[Oxford University Press]]|location=New York|page=[https://archive.org/details/isbn_9780195067279/page/15 15]|edition=1st|isbn=978-0-19-506727-9|url=https://archive.org/details/isbn_9780195067279/page/15}}</ref>

===Flags===
<gallery heights="200" mode="packed">
File:Flag of Dominica.svg|The [[flag of Dominica]] features a [[sisserou parrot]], a national symbol.
File:Flag of Nicaragua.svg|Flag of [[Nicaragua]], although at this size the individual bands of the rainbow are nearly indistinguishable.
</gallery>

==See also==
*[[List of colors]]
*[[Purple]]
*[[Shades of violet]]

==References==
{{Reflist}}
{{refbegin}}
*{{cite book|last=Ball|first=Philip|title=Bright Earth, Art and the Invention of Colour|year=2001|publisher=Hazan (French translation)|isbn=978-2-7541-0503-3}}
*{{cite book|last=Heller|first=Eva|title=Psychologie de la couleur: Effets et symboliques|year=2009|publisher=Pyramyd (French translation)|isbn=978-2-35017-156-2}}
*{{cite book|last=Pastoureau|first=Michel|title=Le petit livre des couleurs|year=2005|publisher=Editions du Panama|isbn=978-2-7578-0310-3}}
*{{cite book|last=Gage|first=John|title=Colour and Culture - Practice and Meaning from Antiquity to Abstraction|url=https://archive.org/details/colourculturepra0000gage|url-access=registration|year=1993|publisher=Thames and Hudson (Page numbers cited from French translation)|isbn=978-2-87811-295-5}}
*{{cite book|last=Gage|first=John|title=La Couleur dans l'art|year=2006|publisher=Thames and Hudson|isbn=978-2-87811-325-9}}
*{{cite book|last=Varichon|first=Anne|title=Couleurs: pigments et teintures dans les mains des peuples|year=2000|publisher=Seuil|isbn=978-2-02084697-4}}
*{{cite book|last=Zuffi|first=Stefano|title=Color in Art|year=2012|publisher=Abrams|isbn=978-1-4197-0111-5}}
*{{cite book|last=Roelofs|first=Isabelle|title=La couleur expliquée aux artistes|year=2012|publisher=Groupe Eyrolles|isbn=978-2-212-13486-5}}
*{{cite book|last=Broecke|first=Lara|title=Cennino Cennini's ''Il Libro dell'Arte'': a New English Translation and Commentary with Italian Transcription|year=2015|publisher=Archetype|isbn=978-1-909492-28-8}}
{{refend}}

==External links==
*{{Commons category-inline|Violet}}

{{Shades of violet}}
{{EMSpectrum}}
{{Color topics}}
{{DEFAULTSORT:Violet (Color)}}
[[Category:Secondary colors]]
[[Category:Tertiary colors]]
[[Category:Optical spectrum]]
[[Category:Rainbow colors]]
[[Category:Shades of violet]]