{{For|the micronation|Gay and Lesbian Kingdom of the Coral Sea Islands}}
{{Use dmy dates|date=May 2011}}
{{Use Australian English|date=May 2011}}
{{Infobox islands
 | name                = Coral Sea Islands Territory
 | image_name          = 
 | image_caption       = 
 | map_image           = Australia in its region (Coral Sea Islands Territory special).svg
 | map_caption         = 
 | location            = [[Coral Sea]]
 | total_islands       = 
 | major_islands       = [[Willis Island (Coral Sea)|Willis Island]]
 | area_km2            = 
 | population          = 4<ref group="n">No permanent population, weather monitoring station generally with four staff.</ref>
 | population_as_of    = 2018  | country             = {{flag|Australia|name=Commonwealth of Australia}}
}}
The '''Coral Sea Islands Territory''' is an [[States and territories of Australia|external territory of Australia]] which comprises a group of small and mostly uninhabited tropical islands and reefs in the [[Coral Sea]], northeast of [[Queensland]], [[Australia]]. The only inhabited island is [[Willis Island]]. The territory covers {{convert|780000|km²|0|abbr=on}}, most of which is ocean, extending east and south from the outer edge of the [[Great Barrier Reef]] and includes Heralds Beacon Island, [[Osprey Reef]], the Willis Group and fifteen other reef/island groups. [[Cato Island]] is the highest point in the Territory.<ref name="Geosci">Geoscience Australia. [http://www.ga.gov.au/education/facts/dimensions/externalterr/coral.htm Coral Sea Islands] {{webarchive|url=https://web.archive.org/web/20060821121314/http://www.ga.gov.au/education/facts/dimensions/externalterr/coral.htm |date=21 August 2006 }}</ref>

== History and status ==
The Coral Sea Islands were first charted in 1803. In the 1870s and 1880s the islands were mined for [[guano]] but the absence of a reliable supply of fresh water prevented long-term habitation.<ref name="Geosci"/> The Coral Sea Islands became an Australian external territory in 1969 by the ''Coral Sea Islands Act'' (prior to that, the area was considered a part of [[Queensland]]) and extended in 1997 to include [[Elizabeth Reef]] and [[Middleton Reef]] nearly 800 km further south.

The two latter reefs are much closer to [[Lord Howe Island]], [[New South Wales]], (about {{convert|150|km|0|abbr=on}}) than to the southernmost island of the rest of the territory, Cato Island. The islands, cays and reefs of the [[Great Barrier Reef]] are not part of the territory, belonging to Queensland instead. The outer edge of the Great Barrier Reef is the boundary between Queensland and the Coral Sea Islands Territory.

The territory is a possession or external territory of [[Australia]], administered from [[Canberra]] by the [[Department of Infrastructure, Regional Development and Cities]]. Previously it was administered by the [[Attorney-General's Department (Australia)|Attorney-General's Department]]<ref name="AussieAG">{{cite web|author=First Assistant Secretary, Territories Division |title=Territories of Australia |url=http://www.ag.gov.au/territories |publisher=Attorney-General's Department |date=2008-01-30 |access-date=2008-02-07 |quote=The Federal Government, through the Attorney-General's Department administers Ashmore and Cartier Islands, Christmas Island, the Cocos (Keeling) Islands, the Coral Sea Islands, Jervis Bay, and Norfolk Island as Territories. |url-status=dead |archive-url=https://web.archive.org/web/20080206093322/http://www.ag.gov.au/territories |archive-date=6 February 2008 }}</ref> and the [[Department of Infrastructure, Transport, Regional Development and Local Government (Australia)|Department of Transport and Regional Services]]).<ref name="DOTARS">{{cite web | author =  Department of Infrastructure, Transport, Regional Development and Local Government | title = Territories of Australia | url = http://www.infrastructure.gov.au/territories/index.aspx | access-date = 2008-02-07 | quote = As part of the Machinery of Government Changes following the Federal Election on 29 November 2007, administrative responsibility for Territories has been transferred to the Attorney General's Department. |archive-url = https://web.archive.org/web/20071216154140/http://www.infrastructure.gov.au/territories/index.aspx |archive-date = 2007-12-16}}</ref> Defence is the responsibility of Australia, and the territory is visited regularly by the [[Royal Australian Navy]].

Australia maintains automatic weather stations on many of the isles and reefs, and claims a {{convert|200|nmi|km|adj=on}} exclusive [[fishing]] zone. There is no economic activity (except for a significant but as yet unquantified charter fishing and diving industry), and only a staff of three or four people to run the meteorological station on [[Willis Island (Coral Sea)|Willis Island]] (South Islet), established in 1921. <ref>http://www.aph.gov.au/~/media/wopapub/house/committee/pwc/willisisland/subs/sub1_pdf.ashx</ref> In November 2011, the Australian government announced that a {{convert|989842|km2|sqmi|0|adj=on}} protected area was planned in the Coral Sea.<ref>{{cite news|url=https://www.bbc.co.uk/news/world-asia-15889194 |title=Australia plans huge marine reserve in Coral Sea |work=BBC News |date=25 November 2011 |access-date=25 November 2011}}</ref>

The [[Supreme Court of Norfolk Island]] has jurisdiction over the islands,<ref>{{citation|url=https://www.legislation.gov.au/Details/C2008C00598|title=Coral Sea Islands Act 1969}} (Cth) s 8 Courts having jurisdiction in the Territory</ref> however, the laws of the [[Australian Capital Territory]] apply.<ref>{{citation |url=https://www.legislation.gov.au/Details/F2006C00306 |title=Application of Laws Ordinance 1973 (Coral Sea Islands)}} (Cth).</ref> The territory's [[FIPS 10-4]] code is CR, whereas [[ISO 3166]] includes it in Australia (AU). 

In 2004, a [[Gay and Lesbian Kingdom of the Coral Sea Islands|symbolic political protest]]  run by gay rights activists based in Australia, declared an the coral sea islands to be a sovereign micronation. On 17 Nov 2017 the same group declared the kingdom to be 'dissolved', following the results of the [[Australian Marriage Law Postal Survey]].

== Geography ==
[[File:Wyspy Morza Koralowego.png|thumb|right|260px|Map of the Coral Sea Islands Territory]]
There are about 30 separate [[reef]]s and [[atoll]]s, twelve being wholly submerged or drying only during low tide, and 18 others with a total of about 51 islets and [[cay]]s (18 alone on the atoll [[Lihou Reef National Nature Reserve|Lihou Reef]]), some of which are vegetated. <!--The coral Sea is {{convert|3.9|mi|km}} deep--> The atolls exhibit a wide range of size, from a few kilometres in diameter to perhaps the second largest atoll in the world by total area (including [[lagoon]]): '''Lihou Reef''', with a lagoon size of {{convert|100|x|30|km|mi|abbr=off}} and an area of {{convert|2,500|km2|sqmi|abbr=off}}, which compares to a combined land area of the 18 individual islets of only {{convert|0.91|km2|sqmi|2|abbr=off}}. The islands are all very low.

The [[Willis Islets]] are important nesting areas for birds and turtles but contain negligible natural resources. They comprise less than {{convert|3|km2|sqmi|spell=in|abbr=off}} of land. There is no port or harbour, only offshore anchorage.

Most of the atolls fall into two groups, while '''Mellish Reef''' to the east, and '''Middleton Reef''' and '''Elizabeth Reef''' to the south are grouped separately:

===Northwestern Group===
# [[Osprey Reef]] (submerged atoll roughly oval in shape, measuring {{convert|25|by|12|km|mi|abbr=off}}, covering around {{convert|195|km2|sqmi|abbr=off}}, with lagoon up to {{convert|30|m|ft|abbr=off}} deep)
# Shark Reef (small elongated submerged reef {{convert|15|km|mi|abbr=off}} south of Osprey Reef, with a minimum depth of {{convert|7.8|m|ft|1|abbr=off}})
# Bougainville Reef (small submerged atoll, {{convert|2.5|by|4|km|mi|1|abbr=off}}, area {{convert|8|km2|sqmi|abbr=off}} with lagoon, dries at half tide)
# East Holmes Reef (submerged atoll, about {{convert|14|by|10|km|mi|abbr=off}}, area {{convert|125|km2|sqmi|abbr=off}} with lagoon)
# West Holmes Reef (submerged atoll {{convert|6|km|mi|abbr=off}} east of East Holmes Reef, about {{convert|18|by|7|km|mi|abbr=off}}, area {{convert|125|km2|sqmi|abbr=off}} with lagoon that is open on the West side, two small cays)
# Flora Reef (small submerged atoll, 5 by 4&nbsp;km, about {{convert|12|km2|sqmi|abbr=off}})
# Diane Bank (sunken atoll, depths of less than 10 m over an area of 65 by 25&nbsp;km, or {{convert|1,300|km2|sqmi|abbr=off}}, along the northern edge 3 m deep, with Sand Cay in the Northwest, 3 m high)
# North Moore Reef (small submerged atoll, 4 by 3&nbsp;km, area {{convert|8|km2|sqmi|abbr=off}} including lagoon that is open on the Northwest side)
# South Moore Reef (small submerged reef 5&nbsp;km South of North Moore Reef)
# [[Willis Island (Coral Sea)|Willis Islets]] (sunken atoll, bank 45 by 19&nbsp;km, bank area more than {{convert|500|km2|sqmi|abbr=off}}, 3 islets on the Northwestern side: North Cay, Mid Islet almost 8 m high, South Islet or Willis Island 10 m high)
# Magdelaine Cays & Coringa Islets (one large, partially sunken atoll structure, almost 90 by 30&nbsp;km, bank area about {{convert|1,500|km2|sqmi|abbr=off}}), 2 islets of the Magdelaine Cays in the North: North West Islet (area approximately {{convert|0.2|km2|sqmi|1|abbr=off}}) and South East Cay (area {{convert|0.37|km2|sqmi|abbr=off}}); 2 islets of the Coringa Islets 50 to 60&nbsp;km further Southwest: Southwest Islet or Coringa Islet (area 0.173&nbsp;km<sup>2</sup>), and Chilcott Islet (area 0.163&nbsp;km<sup>2</sup>)
# Herald Cays, Northeast Cay (encircled by a reef of 3 by 3&nbsp;km, total area 6&nbsp;km<sup>2</sup>, land area 0.34&nbsp;km<sup>2</sup>)
# Herald Cays, Southwest Cay (4&nbsp;km Southwest of Northeast Cay, encircled by a reef of 2 by 2&nbsp;km, total area 3&nbsp;km<sup>2</sup>, land area 0.188&nbsp;km<sup>2</sup>)
# Lihou Reef and Cays (largest atoll in the coral sea, with a size of 2500&nbsp;km<sup>2</sup>, land area 0.91&nbsp;km<sup>2</sup>)
# Diamond Islets & Tregosse Reefs (large, partially sunken atoll, 100 by 52&nbsp;km, area of the bank over 3000&nbsp;km<sup>2</sup>, with 4 islets and 2 small submerged reefs in the Northeast and Southeast: West Diamond Islet, Central Diamond Islet, East Diamond Islet on the Northeastern rim of the former atoll, and South Diamond Islet, East Tregosse Reef and West Tregosse Reef on the Southern rim)
# North Flinders Reef (large atoll, 34 by 23&nbsp;km, area 600&nbsp;km<sup>2</sup>, with 2 islets, Flinders Cay being the larger one with a length of 200 m and a height of 3 m)
# South Flinders Reef (atoll, 15 by 5&nbsp;km, 60&nbsp;km<sup>2</sup>)
# Herald's Surprise (small submerged reef North of Flinders Reefs, 3 by 2&nbsp;km)
# Dart Reef (small submerged reef Northwest of Flinders Reefs, 3 by 3&nbsp;km, area 6&nbsp;km<sup>2</sup> including small lagoon that is open to the North)
# Malay Reef (small submerged reef, not clearly defined, no breakers, difficult to see)
# Abington Reef (submerged reef, nearly awash, 4 by 2.5&nbsp;km, area 7&nbsp;km<sup>2</sup>)
# Marion Reef (large circular atoll formation that is composed of three main units located on the Eastern side: Marion, Long and Wansfell; and a number of smaller reefs on the west. The formation sits atop a submarine feature known as the '''Marion Plateau''' which is separated from the larger Coral Sea Plateau to the north by the '''Townsville Trough'''. Three small sand cays are located on the eastern side of Marion Reef: Paget Cay, on Long Reef, Carola Cay, south of Long Reef, and Brodie Cay, on Wansfell Reef.

The atolls of the Northwestern Group, except Osprey Reef and Shark Reef in the north, and Marion Reef in the south, are located on the '''Coral Sea Plateau''' (Queensland Plateau), a contiguous area of depths less than 1000 m.

* '''Flinders Reefs''' (North and South), '''Herald's Surprise''' and '''Dart Reef''' form a cluster of reefs of 66 by 26&nbsp;km.
* '''Magdelaine Cays''', '''Coringa Islets''' and '''Herald Cays''' are part of the 8856&nbsp;km<sup>2</sup> [[Coringa-Herald National Nature Reserve]], created on 16 August 1982 and located around 400&nbsp;km east of [[Cairns]] and 220 to 320&nbsp;km from the outer edge of the Great Barrier Reef. The 6 islets of the nature reserve have areas from 0.16 to 0.37&nbsp;km<sup>2</sup>, for a total of 1.24&nbsp;km<sup>2</sup>.
* '''[[Lihou Reef National Nature Reserve|Lihou Reef]]''' was declared a Nature Reserve on 16 August 1982, with an area of 8440&nbsp;km<sup>2</sup>.

The Nature Reserves were created to protect wildlife in the respective areas of the territory; together they form the [[Coral Sea Reserves Ramsar Site]].

===Mellish Reef===
#[[Mellish Reef]], being about 300&nbsp;km to the east of the '''Northwestern Group''', thus the most distant from the Australian continent of all the reefs and atolls of the Coral Sea Islands Territory, is not considered to be part of any group. It has the outline of a boomerang-shaped platform around 10&nbsp;km in length and 3&nbsp;km across, area 25&nbsp;km<sup>2</sup>. The surrounding reefs, which enclose a narrow lagoon, are completely submerged at high tide. Near the centre of the lagoon is the only permanent land of the reef - Heralds-Beacon Islet. The island is a small cay measuring 600 m by 120 m, area 57,000 m2, only rising a few ms above the high-water mark.<ref>{{cite web |url=http://www.oceandots.com/pacific/coralsea/mellish.php |title=Oceandots.com |access-date=2009-03-14 |url-status=bot: unknown |archive-url=https://web.archive.org/web/20101223043232/http://www.oceandots.com/pacific/coralsea/mellish.php |archive-date=23 December 2010 |df=dmy-all }}</ref>

===Southeasterly Group===
#[[Frederick Reefs]]: The reefs form a semi-enclosed lagoon, known as Anchorage Sound, with an opening on the North side. The complex measures about 10 by 4&nbsp;km, with an area of 30&nbsp;km<sup>2</sup>. On the southern side of the reef lies Observatory Cay, the only permanently dry land, although there are a few of others cays that can be awash at high tide.
#[[Kenn Reefs]], submerged atoll of about 15 by 8&nbsp;km, area 40&nbsp;km<sup>2</sup>, islet Observatory Cay in the Southeast, 2 m high
#[[Saumarez Reefs]], southernmost reefs to be located on the Coral Sea Shelf; three main reefs and numerous smaller reefs that form a large crescent-shaped formation open to the northwest, about 27 by 14&nbsp;km, area less than 300&nbsp;km<sup>2</sup>. There are two sand cays: North East Cay and South West Cay.
#[[Wreck Reefs]]: atoll 25 by 5&nbsp;km, area 75&nbsp;km<sup>2</sup>, open on the North. Islets found on the reefs include Bird Islet, West Islet and Porpoise Cay.
#[[Cato Reef]]: Cato bank 21 by 13&nbsp;km, area 200&nbsp;km<sup>2</sup> of depths less than 17 m; Cato Reef encircles an area of 3.3 by 1.8&nbsp;km, area 5&nbsp;km<sup>2</sup> including lagoon; Cato Island, in the West of the lagoon, 650 by 300 m, area 0.15&nbsp;km<sup>2</sup>, 6 m high. Close to the Southeast corner of Cato bank is Hutchison Rock, with 1 m depth over. '''Cato Island''' is the highest point in the Territory and a camp site on the Island called Heaven is the home of the '''Gay and Lesbian Kingdom of the Coral Sea Islands.'''

===Extreme South===
[[Elizabeth Reef|Elizabeth]] and [[Middleton Reef|Middleton]] Reefs, together with reefs around [[Lord Howe Island]] ([[New South Wales]]) 150&nbsp;km to the south, are regarded as the southernmost coral reefs in the world. Their location, where tropical and temperate ocean currents meet, contributes to an unusually diverse assemblage of marine species. These mostly submerged atolls which dry only during low tide were added to the territory only in 1989. They are located on the [[Lord Howe Rise]]. Already on 23 December 1987, they were protected as the [[Elizabeth and Middleton Reefs|Elizabeth and Middleton Reefs Marine National Nature Reserve]], which has an area of 1880&nbsp;km<sup>2</sup>.
#[[Middleton Reef]], atoll about 8.9 by 6.3&nbsp;km, area 37&nbsp;km<sup>2</sup> including lagoon, one islet: The Sound, 100 by 70 m (area 5,000 m<sup>2</sup>), highest point close to the Northern end 1.5 m. At low tides much of the reef flat is exposed.
#[[Elizabeth Reef]], atoll about 8.2 by 5.5&nbsp;km, area 51&nbsp;km<sup>2</sup> including lagoon, one islet: Elizabeth island (Elizabeth cay), no vegetation, 600 m by 400 m (area 0.2&nbsp;km<sup>2</sup>), highest point 0.8 m. At low tides much of the reef flat is exposed.

===Overview of islets and cays===
{| class="wikitable sortable"
|-
! Complex !! Type !! Islets/cays</tr>
| [[West Holmes Reef]]       || atoll ||align="right"|  2</tr>
| [[Diane Bank]]             || atoll (mostly sunken)    ||align="right"| Diane Bank Cay</tr>
| [[Willis Group (atoll)|Willis Group]]           || atoll (partially sunken) ||align="right"| South Islet ([[Willis Island]]), Mid Islet, North Cay</tr>
| [[Magdelaine Cays]] and [[Coringa Islets]] || atoll (partially sunken) ||align="right"| Northwest Islet, Southeast Cay, Southwest, Chilcott Islets</tr>
| [[Herald Cays]] (North)    || reef  ||align="right"|  Northwest Cay</tr>
| [[Herald Cays]] (South)    || reef  ||align="right"|  Southeast Cay</tr>
| [[Lihou Reef and Cay]]s    || atoll ||align="right"| 18</tr>
| [[Diamond Islands]] and [[Tregosse Reefs]] || atoll (partially sunken) ||align="right"| 4</tr>
| [[Flinders Reefs]] (North) || atoll ||align="right"|  Flinders, Main, Victoria Cays</tr>
| [[Marion Reef]]            || atoll ||align="right"|  Paget, Carola, Brodie Cays</tr>
| [[Mellish Reef]]           || atoll ||align="right"|  [[Heralds-Beacon Isle]]t</tr>
| [[Frederick Reefs]]        || atoll ||align="right"|  [[Observatory Cay]]</tr>
| [[Kenn Reef]]              || atoll ||align="right"|  [[Observatory Cay]]</tr>
| [[Saumarez Reef]]          || atoll ||align="right"|  Northeast, Southwest Cays</tr>
| [[Wreck Reef]]             || atoll ||align="right"|  Bird, West Islets, Porpoise Cay</tr>
| [[Cato Reef]]              || atoll ||align="right"|  [[Cato Islan]]d</tr>
| [[Middleton Reef]]         || atoll ||align="right"|  1</tr>
| [[Elizabeth Reef]]         || atoll ||align="right"|  1</tr> 
|colspan="2"| ''Total number of islands/cays'' ||align="right"| 51</tr>
|}

==Man-made structures==
Automatic, unmanned weather stations are located on the following reefs or atolls:
*Bougainville Reef
*Cato Island
*Flinders Reef (Flinders Coral Cay)
*Frederick Reef
*Holmes Reef
*Lihou Reef (Turtle Islet)
*Marion Reef
*Moore Reef

Lighthouses are located on following reefs or islands:
*Bougainville Reef
*East Diamond Islet
*Frederick Reefs
*Lihou Reef
*Saumarez Reef

Willis Island, the only inhabited island, has a number of structures.
{{further information|Willis Island (Coral Sea)#Current infrastructure}}

==See also==
{{portal|Geography|Islands|Australia}}
*[[List of islands of Australia]]

==References==
{{reflist}}

==Notes==
{{Reflist|group=n}}

* [http://www.ag.gov.au/ Coral Sea Islands History and the list of other Australia territories (Australian Government, Attorney-General's Department)]

{{coord|19|05|27|S|150|54|06|E|region:AU_type:isle|display=title}}
{{States and territories of Australia}}
{{Australia topic|title=[[History of Australia]]|prefix=History of|VI=Victoria}}
{{corals}}
{{Authority control}}
[[Category:Coral Sea Islands| ]]
[[Category:Landforms of the Coral Sea]]
[[Category:Islands of Australia]]
[[Category:States and territories established in 1969]]
[[Category:1969 establishments in Oceania]]