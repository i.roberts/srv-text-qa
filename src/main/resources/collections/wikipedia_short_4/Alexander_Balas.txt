{{Infobox royalty
| name         = Alexander I Balas
| title        =
| titletext    = 
| more         = 
| type         = 
| image        = Coin of Alexander I Balas, Antioch mint.jpg
| image_size   =
| alt          = 
| caption      = Coin of Alexander I Balas, [[Antioch]] mint
| succession   = [[Basileus]] of the [[Seleucid Empire]]
| moretext     = (King of [[Syria]])
| reign        = 150 BC – August 145 BC
| reign-type   = 
| coronation   = 
| cor-type     = 
| predecessor  = [[Demetrius I Soter]]
| pre-type     = Predecessor
| successor    = [[Demetrius II Nicator]] or [[Antiochus VI Dionysus]]
| suc-type     = Successors
| regent       = 
| reg-type     = 
 
| birth_name   = 
| birth_date   = <!-- {{birth date and age|YYYY|MM|DD|df=y}} -->
| birth_place  = [[Smyrna]]
| death_date   = August 145 BC<!-- {{death date and age|YYYY|MM|DD|YYYY|MM|DD|df=y}} -->
| death_place  = 
| burial_date  = 
| burial_place = 
| spouse       = [[Cleopatra Thea]]
| consort      = <!-- yes or no -->
| issue        = [[Antiochus VI Dionysus]] (first son with Cleopatra Thea)<!--list children in order of birth-->
| issue-link   = 
| issue-pipe   = 
| issue-type   = 
| full name    = 
| era name     = 
| era dates    = 
| regnal name  = 
| posthumous name = 
| temple name  = 
| house        = [[Seleucid dynasty|Seleucid]]
| house-type   = Dynasty
| father       = [[Antiochus IV Epiphanes]] (unconfirmed)
| mother       = [[Laodice IV]] (unconfirmed)

| occupation   = 
| signature_type = 
| signature    = 
}}

'''Alexander I Theopator Euergetes''', surnamed '''Balas''' ({{lang-grc|Ἀλέξανδρος Βάλας|Alexandros Balas}}), was the ruler of the [[Greece|Greek]] [[Seleucid Empire|Seleucid kingdom]] in 150/Summer 152 – August 145 BC.<ref>{{cite web|url=https://www.livius.org/articles/person/alexander-i-balas/?|title=Alexander I Balas|work=Livius.org}}</ref> Alexander defeated [[Demetrius I Soter]] for the crown in 150 BC. Ruling briefly, he lost the crown to [[Demetrius II Nicator]] during his defeat at the [[Battle of Antioch (145 BC)]] in Syria, dying shortly after. His reign marks the beginning of the disintegration of the Seleucid realm, with important eastern satrapies such as [[Media (region)|Media]] being lost to the nascent [[Parthian Empire]].

==Life==
===Origins and mission to Rome===
Alexander Balas claimed to be the son of [[Antiochus IV Epiphanes]] and [[Laodice IV]] and heir to the Seleucid throne. The ancient sources, [[Polybius]] and [[Diodorus]] say that this claim was false and that he and his sister [[Laodice VI]] were really natives of [[İzmir|Smyrna]] of humble origin.<ref>Polybius 33.18.5-18; Diodorus ''Bibliotheca'' 31.32a.</ref> Modern scholars disagree about whether this is true or was propaganda put about by Alexander's opponents.<ref>{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=162 n. 139}}</ref>

According to Diodorus, Alexander was originally put forward as a candidate for the Seleucid throne by [[Attalus II]] of [[Pergamum]]. Attalus had been disturbed by the Seleucid king Demetrius I's interference in [[Cappadocia]], where he had dethroned king [[Ariarathes V]].<ref>Diodorus ''Bibliotheca'' 31.32a</ref> Boris Chrubasik is sceptical, noting that there is little subsequent evidence for Attalid involvement with Alexander.<ref>{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=130 and 163}}</ref> However, Selene Psoma has proposed that a large set of coins minted in a number of cities under Attalid control in this period was produced by Attalus II in order to fund Alexander's bid for the kingship.<ref>{{cite book |last1=Psoma |first1=Selene E. |editor1-last=Thonemann |editor1-first=Peter |title=Attalid Asia Minor: Money, International Relations, and the State |date=2013 |publisher=Oxford University Press |location=Oxford |pages=265–300 |chapter=War or Trade? Attic-Weight Tetradrachms from Second-Century BC Attalid Asia Minor in Seleukid Syria after the Peace of Apameia and Their Historical Context}}</ref>

Alexander and his sister were maintained in [[Cilicia]] by Heracleides, a former minister of Antiochus IV and brother of [[Timarchus]], an usurper in [[Medes|Media]] who had been executed by the reigning king [[Demetrius I of Syria|Demetrius I Soter]].<ref name="DGRBM">{{cite encyclopedia|last=Smith |first=Philip Peter |title=Alexander Balas |editor=William Smith |editor-link=William Smith (lexicographer) |encyclopedia=[[Dictionary of Greek and Roman Biography and Mythology]] |volume=1 |pages=114–115 |publisher=[[Little, Brown and Company]] |location=Boston |year=1867 |url=http://www.ancientlibrary.com/smith-bio/0123.html |url-status=dead |archive-url=https://web.archive.org/web/20110606005906/http://www.ancientlibrary.com/smith-bio/0123.html |archive-date=2011-06-06 }}</ref> In 153 BC, Heracleides brought Alexander and his sister to [[Rome]], where he presented Alexander to the [[Roman Senate]], which recognised him as the legitimate Seleucid king and agreed to support him in his bid to take the throne. Polybius mentions that Attalus II and Demetrius I also met with the Senate at this time but does not state how this was connected to the recognition of Alexander - if at all.<ref>Polybius 33.18; {{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=[[Oxford University Press]] |location=Oxford |isbn=9780198786924|pages=130}}</ref>

===War with Demetrius I (152-150 BC)===
[[Image:Coin of Alexander I Balas, Antioch mint (2).jpg|thumb|right|Silver coin of Alexander I "Balas". The Greek inscription reads ΒΑΣΙΛΕΩΣ ΑΛΕΧΑΝΔΡΟΥ (king Alexander). The date ΓΞΡ is year 164 of the [[Seleucid dynasty|Seleucid era]], corresponding to 149&ndash;148 BC.]]
After recruiting mercenaries, Alexander and Heracleides departed to [[Ephesus]]. From there, they invaded Phoenicia by sea, seizing [[Ptolemais Akko]].<ref>Polybius 33.18.14; Josephus ''AJ'' 13.35</ref> Numismatic evidence shows that Alexander had also gained control of [[Seleucia Pieria]], [[Byblos]], [[Beirut]], [[Tyre, Lebanon|Tyre]] by 151 BC.<ref>{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=131}}</ref> On this coinage, Alexander heavily advertised his (claimed) connection to Antiochus IV, depicting Zeus Nicephorus on his coinage as Antiochus had done. He also assumed the title of ''Theopator'' ('Divinely Fathered'), which recalled Antiochus' epithet ''Theos Epiphanes'' ('God Manifest'). The coinage also presented Alexander Balas in the guise of [[Alexander the Great]], with pronounced facial features and long flowing hair. This was intended to emphasise his military prowess to his soldiers.<ref>{{cite book |last1=Bohm |first1=Claudia |title=Imitatio Alexandri im Hellenismus; Untersuchungen zum politischen Nachwirken Alexanders des Grossen in hoch- und späthellenistischen Monarchien |date=1989 |location=Munich |pages=105–116}}</ref><ref>{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=163–5}}</ref>

Alexander and Demetrius I competed with another to win over [[Jonathan Apphus]], the leader of the ascendant faction in Judaea. Jonathan was won over to Alexander's side by the grant of a high position in the Seleucid court and the [[Kohen gadol|high priesthood]] in [[Jerusalem]].<ref>Josephus ''AJ'' 13.45; I Maccabees. 10.3-6, 10.20</ref><ref>{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=165–166}}</ref> Reinforced by Jonathan's hardened soldiers, Alexander fought a decisive battle with Demetrius in July 150 BC, in which Demetrius was killed. By autumn, Alexander's kingship was recognised throughout the Seleucid realm.<ref>Josephus ''AJ'' 13.59–61; I Maccabees 10.48–50; Justin ''Epitome of Pompeius Trogus'' 35.1.9–11. ''Astronomical Diaries'' III 149 A rev. 1–13 and B obv. 1</ref><ref>{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=130–1}}</ref>

===Reign (150-147 BC)===
[[Image:Rare marriage commemorative of Alexander I Balas and Cleopatra Thea.jpg|thumb|Marriage commemorative of Alexander I Balas and [[Cleopatra Thea]].]]
Alexander gained control of Antioch at this time and his chancellor, Ammonius, murdered all the courtiers of Demetrius I, as well as his wife [[Laodice V|Laodice]] and his eldest son Antigonus.<ref>Livy, ''Periochae'' 50; {{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=132 n. 33}}</ref> [[Ptolemy VI Philometor]] of Egypt entered into an alliance with Alexander, which was sealed by Alexander's marriage to his daughter [[Cleopatra Thea]]. The wedding took place at Ptolemais, with Ptolemy VI and Jonathan Apphus in attendance. Alexander took the opportunity to shower honours on Jonathan, whom he treated as his main agent in Judaea.<ref>I Maccabees 10.61-65; Josephus ''AJ'' 13.80-85</ref><ref>{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=131–2}}</ref> The marriage was advertised by a special coinage issue, depicting the royal pair side by side - only the second depiction of a queen on Seleucid coinage. She is shown with divine attributes (a [[cornucopia]] and a [[Calathus (basket)|calathus]]) and is depicted in front of the king. Some scholars have seen Alexander as little more than a Ptolemaic puppet, arguing that this coinage emphasises Cleopatra's dominance over him and that the chancellor Ammonius was a Ptolemaic agent.<ref>{{cite journal |last1=Volkmann |first1=Hans |title=Demetrios I. und Alexander I. von Syrien |journal=Klio |date=1925 |volume=19 |issue=19 |page=406|doi=10.1524/klio.1925.19.19.373 |s2cid=194387728 }}; {{cite book|title=Untersuchungen zur Geschichte der späten Seleukiden (164–63 v. Chr.): Vom Tode des Antiochs IV. bis zur Einrichtung der Provinz Syria unter Pompeius|last1=Ehling|first1=Kay|date=2008 |location=Stuttgart |pages=155–156}}</ref> Other scholars argue that the alliance was advertised as an important one, but that the arguments for Alexander's subservience have been overstated.<ref>{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=167–169}}</ref>

Being now master of the empire, he is said to have abandoned himself to a life of debauchery, handing the administration of Antioch over to two commanders, Hierax and [[Diodotus Tryphon|Diodotus]].<ref name="EB1911">{{EB1911|inline=y|wstitle=Alexander Balas|volume=1|pages=565-566}}</ref> This representation is partially a product of his opponents' propaganda, but Alexander is not recorded to have achieved anything in this years. Meanwhile, the [[Parthian Empire|Parthians]] took advantage of the unsettled situation to invade [[Media (Iran)|Media]]. The region had been lost to Seleucid control by the middle of 148 BC.<ref>''Inscriptiones d;Iran et d'Asie centrale'' n. 70; Justin, ''Epitome of Pompeius Trogus'' 41.6.6; {{cite book |last1=Le Rider |first1=Georges |title=Suse sous les Séleucides et les Parthes: Les trouvailles monétaires et l'histoire de la ville |date=1965 |location=Paris |pages=339–340}}</ref><ref>{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=132}}</ref>

===War with Demetrius II and death (147-145 BC)===
[[File:Coin of Demetrius II Nicator, Ptolemais in Phoenicia mint.jpg|thumb|Coin of [[Demetrius II Nicator]]]]
In early 147 BC Demetrius' son [[Demetrius II of Syria|Demetrius II]] returned to Syria with a force of [[Cretan]] mercenaries led by a man called Lasthenes. Much of [[Coele Syria]] was lost to him immediately, possibly as a result of the succession of the regional commander. Jonathan attacked Demetrius's position from the south, seizing [[Jaffa]] and [[Ashdod]], while Alexander Balas was occupied with a revolt in [[Cilicia]].<ref>I Maccabees 10.69–89; Josephus ''AJ'' 13.88–102</ref> In 145 BC Ptolemy VI of Egypt invaded Syria, ostensibly in support of Alexander Balas. In practice, Ptolemy's intervention came at a heavy cost; with Alexander's permission, he took control of all the Seleucid cities along the coast, including [[Seleucia Pieria]].<ref>[[I Maccabees]] 11.3-8</ref> He may also have started minting his own coinage in the Syrian cities.<ref>{{cite journal |last1=Lorber |first1=Catharine C. |title=The Ptolemaic Era Coinage Revisited |journal=Numismatic Chronicle |date=2007 |volume=167 |pages=105–17}}</ref><ref name=C1334/>

While he was at Ptolemais Akko, however, Ptolemy switched sides. According to [[Josephus]], Ptolemy discovered that Alexander's chancellor, Ammonius, had been plotting to assassinate him, but when he demanded that Ammonius be punished, Alexander refused.<ref>Josephus ''Antiquities of the Jews'' 13.106-107; [[I Maccabees]] does not mention the episode and presents Ptolemy as planning to supported Demetrius II from the start. Josephus presents Ptolemy as genuinely supporting Alexander until this moment.</ref> Ptolemy remarried his Cleopatra Thea to Demetrius II and continued his march northward. Alexander's commanders of [[Antioch]], Diodotus and Hierax, surrendered the city to Ptolemy.<ref>[[I Maccabees]] 11; Josephus ''Antiquities of the Jews'' 13.106-107, 115</ref><ref name=C1334>{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=133–134}}</ref>

Alexander returned from Cilicia with his army, but Ptolemy VI and Demetrius II defeated his forces in a [[Battle of Antioch (145 BC)|battle]] at the [[Afrin River|Oenoparas river]].<ref>Strabo 16.2.8.</ref> Earlier, Alexander had sent his infant son [[Antiochus VI Dionysus|Antiochus]] to an Arabian dynast called Zabdiel Diocles. Alexander now fled to Arabia in order to join up with Zabdiel, but he was killed. Sources disagree about whether the killer was a pair of his own generals who had decided to switch sides or Zabdiel himself. Alexander's severed head was brought to Ptolemy, who also died shortly after from wounds sustained in the battle.<ref>Diodorus 32.9d & 10.1; Zabdiel: I Maccabees 11.17; Josephus ''AJ'' 13.118.</ref><ref>{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=134–5}}</ref>

Zabdiel continued to look after Alexander's infant son Antiochus, until 145 BC when the general Diodotus declared him king, in order to serve as the figurehead of a rebellion against Demetrius II. In 130 BC, another claimant to the throne, [[Alexander Zabinas]], would also claim to be Alexander Balas' son; almost certainly spuriously.<ref>Porphyry ''[[FGrH]]'' 260 F 32.21; {{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924|pages=142}}</ref> Alexander is the title character of the [[oratorio]] ''[[Alexander Balus]]'', written in 1747 by [[George Frideric Handel]].

==See also==
{{Portal|Asia}}
* [[List of Syrian monarchs]]
* [[Timeline of Syrian history]]

==References==
{{Reflist|30em}}

==Bibliography==
;Primary
*[[1 Maccabees]] 10 ff.
*[[Junianus Justinus|Justin]] xxxv. 1 and 2
*{{cite Josephus|pace=1|text=aj|bookno=13|chap=2|sec=1|show-translator=no|show-source=no}}
*[[Appian]], ''Syrian Wars'' (=''Roman History'' book 11), 67
*[[Polybius]], ''[[The Histories (Polybius)|The Histories]]'' xxxiii. 14.
;Secondary
*{{cite CE1913|wstitle=Alexander|volume=1|first=Anthony John|last= Maas}}
*{{cite journal|last=Mørkholm|first=Otto|year= 1981|title=Sculpture and Coins: the Portrait of Alexander Balas of Syria|journal=Numismatica e Antichità Classiche|publisher=Industria Grafica Gaggini-Bizzozero|volume=10|issn=1420-1739|oclc=715323965}}
*{{cite book |last1=Chrubasik |first1=Boris |title=Kings and Usurpers in the Seleukid Empire: The Men who would be King |date=2016 |publisher=Oxford University Press |location=Oxford |isbn=9780198786924}}

==External links==
{{Commons category}}
*[http://virtualreligion.net/iho/a_balas.html Alexander Balas], article in historical sourcebook by Mahlon H. Smith
*[http://medaillesetantiques.bnf.fr/ws/catalogue/app/collection/record/344?vc=ePkH4LF7w6yegDEO9wZyIjQyN4XWg0SmGH3MQEOKGGj-CD68Mie1NDm_GFxfw8IHADVcNKk$ Intaglio representing Alexander I]

{{s-start}}
{{s-hou|[[Seleucid Empire|Seleucid dynasty]]||Unknown||146 BC}}
{{s-bef|before=[[Demetrius I Soter]]}}
{{s-ttl|title=[[Seleucid Empire|Seleucid King]]<br /><small>([[King of Syria]])</small>|years=150&ndash;146 BC}}
{{s-aft|after=[[Demetrius II Nicator]] or [[Antiochus VI Dionysus]]}}
{{s-end}} 
{{Hellenistic rulers}}
{{Authority control}}

{{DEFAULTSORT:Alexander Balas}}
[[Category:146 BC deaths]]
[[Category:2nd-century BC Babylonian kings]]
[[Category:2nd-century BC Seleucid rulers]]
[[Category:Seleucid rulers]]
[[Category:Ptolemaic dynasty]]
[[Category:2nd-century BC rulers]]
[[Category:Year of birth unknown]]
[[Category:Kings of Syria|Alexander]]