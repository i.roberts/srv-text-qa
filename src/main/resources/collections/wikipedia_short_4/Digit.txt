{{wiktionary|digit|digits}}
'''Digit''' may refer to:

{{tocright}}

==Mathematics and science==
* [[Numerical digit]], as used in mathematics or computer science
** [[Arabic numerals]], the most common modern representation of numerical digits
* [[Digit (anatomy)]], one of several most distal parts of a limb—fingers, thumbs, and toes on hands and feet
* Dit or digit, synonym of [[Ban (unit)]], a unit of information entropy
* [[Digit (unit)]], an ancient measurement unit
* Digit, [[Dian Fossey]]'s favourite gorilla, who was killed by poachers in  Rwanda. Fossey subsequently created the [[Digit Fund]] to raise money for anti-poaching patrols

==Arts and media==
<!--* [[Digit (E1/Royal Bloodline Records Recording Artist)]], Seven "Digit" Hughes, Recording artist from Atlanta, GA-->
<!--* [[Digit (IT magazine)|''DiGIT'' (magazine)]], a free online IT magazine based in Sri Lanka-->
* [[Digit (magazine)|''Digit'' (magazine)]], an Indian information technology magazine
* [[liquid and digits|digits dancing]], a type of gestural, interpretive, rave and urban street dance
* [[Digit (Cyberchase)|Digit (''Cyberchase'')]], a character in the PBS Kids animated series ''Cyberchase''
* Digit, an obscure robotic [[Muppet]] character

==Other uses==
* [[Phone number]], "digits" in slang

==See also==
*[[Digital (disambiguation)]]

{{disambiguation}}