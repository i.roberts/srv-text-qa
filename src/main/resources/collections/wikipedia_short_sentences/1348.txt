
{{Use mdy dates|date=February 2011}}
{{Year dab|1348}}
{{refimprove|date=July 2017}}
{{Year nav|1348}}
{{C14 year in topic}}Year 1348 (MCCCXLVIII) was a leap year starting on Tuesday (link will display the full calendar) of the Julian calendar, the 1348th year of the Common Era (CE) and Anno Domini (AD) designations, the 348th year of the 2nd millennium, the 48th year of the 14th century, and the 9th and pre-final year of the 1340s decade.

== Events ==
<onlyinclude>
=== January&ndash;December ===
* January &ndash; Gonville Hall, the forerunner of Gonville and Caius College, Cambridge, England, is founded.
* January 25 &ndash; The 6.9-magnitude 1348 Friuli earthquake centered in Northern Italy was felt across Europe. Contemporary minds linked the quake with the Black Death, fueling fears that the Biblical Apocalypse had arrived.
* February 2 &ndash; Battle of Strėva: the Teutonic Order secure a victory over the Grand Duchy of Lithuania.
* April 7 &ndash; Charles University in Prague, founded the previous year by papal bull, is granted privileges by Charles I, King of Bohemia, in a golden bull. 
* April 23 &ndash; Edward III of England creates the first English order of chivalry, the Most Noble Order of the Garter.
* By June 24 &ndash; The Black Death pandemic has reached England, having probably been brought across the English Channel by a sailor from Gascony to the south coast port of Melcombe (modern-day Weymouth, Dorset); by November it will have reached London.
* July 6 &ndash; A papal bull is issued by Pope Clement VI, protecting Jews against popular aggression during the Black Death pandemic.
* November 1 &ndash; The anti-royalist Union of Valencia attacks the Jews of Murviedro because they are serfs of the King of Valencia and thus "royalists".
* November 18 &ndash; Emperor Kōmyō of Japan abdicates the throne in favour of his son Emperor Sukō, making them the second and third of the Northern Court (Ashikaga Pretenders).

=== Date unknown ===
* The Black Death pandemic spreads to central and western Europe and to Cairo.
* Stefan the Mighty, Emperor of Serbia, conquers Thessaly and Epirus.
* The Pskov Republic gains independence from the Novgorod Republic with the treaty of Bolotovo.
* Hundred Years' War (1337–1360): The effects of the Black Death cause a de facto truce to be observed between England and France until 1355.
* Estimation: Hangzhou in Mongolian China becomes the largest city of the world, taking the lead from Cairo, capital of Mamluk Egypt.</onlyinclude>

== Births ==
* April 11 &ndash; Andronikos IV Palaiologos, Byzantine Emperor (d. 1385)
* date unknown
** John Fitzalan, 1st Lord Arundel (d. 1379)
** Alice Perrers, politically active English royal mistress and courtier (d. 1400)

== Deaths ==
* February 2 &ndash; Narimantas, Christian Lithuanian prince of Pinsk (Battle of Strėva)
* June 9 &ndash; Ambrogio Lorenzetti, Sienese painter (Black Death) (b. 1290)
* June 13 &ndash; Don Juan Manuel, prince of Villena, Spanish writer (b. 1282)
* July 1 &ndash; Joan of England, princess (Black Death) (b. 1333/34)
* August 20 &ndash; Laurence Hastings, 1st Earl of Pembroke, English noble (b. 1319)
* August 23 &ndash; John de Stratford, Archbishop of Canterbury (b. c.1275)
* October 2 &ndash; Alice de Lacy, 4th Countess of Lincoln, English noblewoman (b. 1281)

* date unknown
** Laura de Noves, French countess, presumed beloved of Petrarch (b. 1310)
** Pietro Lorenzetti, Sienese painter (Black Death) (b. 1280)
** Umur of Aydın, Emir (killed in action) (b. c.1309)
** Giovanni Villani, chronicler of Florence (Black Death) (b. c. 1276)

== References ==
{{Reflist}}

{{DEFAULTSORT:1348}}
