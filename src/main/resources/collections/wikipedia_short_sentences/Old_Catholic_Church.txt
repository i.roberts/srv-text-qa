
{{distinguish|History of the Catholic Church}}
{{Other uses}}
{{short description|Churches that split from Roman Catholic Church due to rejection of papal infallibility & universal jurisdiction of the pope}}
{{more citations needed|date=June 2015}}
{{Use dmy dates|date=February 2020}}
{{Infobox Christian denomination
| icon = 
| icon_width =
| icon_alt =
| name = Old Catholic Church
| native_name = 
| native_name_lang = 
| image = Sint-Gertrudiskathedraal.JPG
| imagewidth = 200px
| alt = 
| caption = St. Gertrude's Cathedral, Utrecht, Netherlands
| abbreviation = 
| type = 
| main_classification = Independent Catholic
| orientation = 
| scripture = 
| theology = Ultrajectine
| polity = Episcopal
| governance =
| structure =
| leader_title = Archbishop
| leader_name = Bernd Wallet,<br>Metropolitan of Utrecht
| leader_title1 = Primus inter pares
| leader_name1 = Anthony Mikovsky, Prime Bishop of Polish-Catholic Church of Republic of Poland
| leader_title2 = 
| leader_name2 = 
| leader_title3 =
| leader_name3 =
| fellowships_type =
| fellowships = 
| fellowships_type1 =
| fellowships1 =
| division_type = Union of Utrecht 
| division = * Old Catholic Church of the Netherlands
** Old Catholic Church in Sweden and Denmark
* Polish-Catholic Church of Republic of Poland{{efn|The organization Polish Catholic Church in Poland, a member church of the {{abbr|UU|Union of Utrecht of the Old Catholic Churches}}, is not to be confused with the Catholic Church in Poland or confused with the Polish National Catholic Church, a former member church of the {{abbr|UU|Union of Utrecht of the Old Catholic Churches}}.}}
* Catholic Diocese of the Old Catholics in Germany
* Christian Catholic Church of Switzerland
** Old Catholic Mission in France
* Old Catholic Church of Austria
** Old Catholic Church of Croatia
* Old Catholic Church of the Czech Republic
| division_type1 = Union of Scranton
| division1 = * Polish National Catholic Church <br>
* Nordic Catholic Church
| division_type2 =
| division2 =
| division_type3 =
| division3 =
| associations = Anglican Communion (Union of Utrecht only), Church of Sweden (Union of Utrecht only)
| area = 
| language =
| headquarters = St. Gertrude's Cathedral, Utrecht, Netherlands
| origin_link =
| founder = Ignaz von Döllinger
| founded_date = 1870
| founded_place = Nuremberg, Kingdom of Bavaria
| separated_from = Catholic Church (1879)
| parent = 
| merger = 
| absorbed = 
| separations = 
| merged_into = 
| defunct =
| congregations_type =
| congregations = 
| members = 
| ministers_type =
| ministers = 
| missionaries =
| churches =
| hospitals = 
| nursing_homes = 
| aid = 
| primary_schools = 
| secondary_schools = 
| tax_status =
| tertiary = 
| other_names = 
| publications = 
| website = 
| slogan =
| logo =
| footnotes = 
}}

The term Old Catholic Church was used from the 1850s by groups which had separated from the Roman Catholic Church over certain doctrines, primarily concerned with papal authority; some of these groups, especially in the Netherlands, had already existed long before the term. These churches are not in full communion with the Holy See. Member churches of the Union of Utrecht of the Old Catholic Churches (UU) are in full communion with the Anglican Communion, and some are members of the World Council of Churches.

The formation of the Old Catholic communion of Germans, Austrians and Swiss began in 1870 at a public meeting held in Nuremberg under the leadership of Ignaz von Döllinger, following the First Vatican Council. Four years later, episcopal succession was established with the consecration of an Old Catholic German bishop by a prelate of the Church of Utrecht. In line with the "Declaration of Utrecht" of 1889, adherents accept the first seven ecumenical councils and doctrine formulated before the East–West Schism of 1054, but reject communion with the pope and a number of other Roman Catholic doctrines and practices. The Oxford Dictionary of the Christian Church notes that since 1925 they have recognized Anglican ordinations, have had full communion with the Church of England since 1932, and have taken part in the ordination of Anglican bishops. According to the principle of ex opere operato, certain ordinations by bishops not in communion with Rome are still recognised as being valid by Rome and the ordinations of and by Old Catholic bishops in the Union of Utrecht churches has never been formally questioned by Rome, only the more recent ordinations of women as priests.

The term "Old Catholic" was first used in 1853 to describe the members of the See of Utrecht who did not recognize any infallible papal authority. Later Catholics who disagreed with the Roman Catholic dogma of papal infallibility as defined by the First Vatican Council (1870) were hereafter without a bishop and joined with Utrecht to form the Union of Utrecht of the Old Catholic Churches (UU). Today these Old Catholic churches are found chiefly in Germany, Switzerland, the Netherlands, Austria, Poland and the Czech Republic.

{{TOC limit|3}}

==Beliefs==
Old Catholic theology views the Eucharist as the core of the Christian Church. From that point the church is a community of believers. All are in communion with one another around the sacrifice of Jesus Christ, as the highest expression of the love of God. Therefore, the celebration of the Eucharist is understood as the experience of Christ's triumph over sin. The defeat of sin consists in bringing together that which is divided.

Old Catholics believe in unity in diversity and often quote the Church Father Vincent of Lérins{{'s}} Commonitory: "in the Catholic Church itself, all possible care must be taken, that we hold that faith which has been believed everywhere, always, by all."{{rp|page=132}}

==History==

===Pre-Reformation diocese and archdiocese of Utrecht===
{{Main article|Archdiocese of Utrecht (695–1580)}}
Four disputes set the stage for an independent bishopric of Utrecht: the Concordat of Worms, the First Lateran Council,{{Relevance inline|the term 'First Lateran Council'|reason=The First Lateran Council was not a dispute but an event which ratified the Concordat of Worms.|date=February 2016}} the Fourth Lateran Council, and confirmation of church procedural law by Pope Leo X. Also relevant was the 12th-century Investiture Controversy over whether the Holy Roman Emperor or the Pope could appoint bishops.
In 1122, the Concordat of Worms was signed, making peace.
The emperor renounced the right to invest ecclesiastics with ring and crosier, the symbols of their spiritual power, and guaranteed election by the canons of cathedral or abbey and free consecration. Emperor Henry V and Pope Calixtus II ended the feud by granting one another peace.{{efn|The Concordat of Worms was confirmed by the First Lateran Council in 1123.{{Relevance inline|the term 'First Lateran Council'|reason=The First Lateran Council was not a dispute but an event which ratified the Concordat of Worms.|date=February 2016}}}}
In 1215, the Fourth Lateran Council canon 23 states that the duty to elect a bishop for a cathedral within three months devolves to the next immediate superior when that duty is neglected by electors.{{Primary source inline|reason=Needs secondary source with commentary to explain context.|date=February 2016}}
In 1517 Pope Leo X, in {{lang|la|Debitum pastoralis officii nobis}}, forbade the Archbishop-Elector of Cologne, Hermann of Wied, to rely on his status as {{lang|la|legatus natus}}{{efn|"As papal power increased after the middle of the eleventh century these legates came to have less and less real authority and eventually the {{lang|la|legatus natus}} was hardly more than a title."}} in summoning Philip of Burgundy, his treasurer, and his ecclesiastical and secular subjects to a court of first instance in Cologne.{{efn|Joosting and Muller noted that Leo X also promulgated another bull, in which he commissioned that the Bishop of Utrecht, his treasurer and his subjects informed that they were empowered to disregard privileges formerly granted to others and to prosecute offenders while setting aside formerly specified legal process.}} John Mason Neale explained that Leo X only confirmed a right of the church but Leo X's confirmation "was providential" in respect to the future schism.{{rp|page=72}} This greatly promoted the independence of the diocese, so that no clergy or laity from Utrecht would ever be tried by a Roman tribunal.{{citation needed|date=February 2016}}

===Overview: three stages of separation from the Roman Catholic Church===
Old Catholicism's formal separation from the Roman Catholic Church occurred over the issue of papal authority. This separation occurred in The Netherlands in 1724, creating the first Old Catholic church. The churches of Germany, Austria, Bohemia, and Switzerland created the {{abbr|UU|Union of Utrecht of the Old Catholic Churches}} after Vatican I (1871), over the Roman Catholic dogma of papal infallibility. By the early 1900s, the movement included groups in England, Canada, Croatia, France, Denmark, Italy, the United States, the Philippines, China, and Hungary.

====First stage: Post-reformation Netherlands====
{{Main article|Old Catholic Church of the Netherlands|Old Catholic Archdiocese of Utrecht}}
During the Protestant Reformation, the Roman Catholic Church was persecuted and the Holy See appointed an apostolic vicar to govern the bishop-less dioceses north of the Rhine and Waal. Protestants occupied most church buildings, and those remaining were confiscated by the government of the Dutch Republic, which favoured the Dutch Reformed Church.{{Failed verification|talk=Persecution of Catholics in Parker|reason=Not about p. 60 describes frontier towns but not about diocese or churches p 62 describes negotiating points of reputation and religion|date=February 2016}}

The northern provinces, that revolted against the Spanish Netherlands and signed the 1579 Union of Utrecht, persecuted the Roman Catholic Church, confiscated church property, expelled monks and nuns from convents and monasteries, and made it illegal to receive the Catholic sacraments. However, the Catholic Church did not die, rather priests and communities went underground. Groups would meet for the sacraments in the attics of private homes at the risk of arrest.{{sfn|Neale|1858}} Priests identified themselves by wearing all black clothing with very simple collars. All the episcopal sees of the area, including that of Utrecht, had fallen vacant by 1580, because the Spanish crown, which since 1559 had patronal rights over all bishoprics in the Netherlands, refused to make appointments for what it saw as heretical territories, and the nomination of an apostolic vicar was seen as a way of avoiding direct violation of the privilege granted to the crown. The appointment of an apostolic vicar, the first after many centuries, for what came to be called the Holland Mission was followed by similar appointments for other Protestant-ruled countries, such as England, which likewise became mission territories. The disarray of the Roman Catholic Church in the Netherlands between 1572 and about 1610 was followed by a period of expansion of Roman Catholicism under the apostolic vicars, leading to Protestant protests.

The initial shortage of Roman Catholic priests in the Netherlands resulted in increased pastoral activity of religious clergy, among whom Jesuits formed a considerable minority, coming to represent between 10 and 15 percent of all the Dutch clergy in the 1600–1650 period. Conflicts arose between these, and the apostolic vicars and secular clergy. In 1629, the priests were 321, 250 secular and 71 religious, with Jesuits at 34 forming almost half of the religious. By the middle of the 17th century the secular priests were 442, the religious 142, of whom 62 were Jesuits.

The fifth apostolic vicar of the Dutch Mission, Petrus Codde, was appointed in 1688. In 1691, the Jesuits accused him of favouring the Jansenist heresy. Pope Innocent XII appointed a commission of cardinals to investigate the accusations against Codde. The commission concluded that the accusations were groundless.

In 1700, Pope Clement XI summoned Codde to Rome to participate in the Jubilee Year, whereupon a second commission was appointed to try Codde. The result of this second proceeding was again acquittal. However, in 1701 Clement XI decided to suspend Codde and appoint a successor. The church in Utrecht refused to accept the replacement and Codde continued in office until 1703, when he resigned.

After Codde's resignation, the Diocese of Utrecht elected Cornelius Steenoven as bishop. Following consultation with both canon lawyers and theologians in France and Germany, Dominique Marie Varlet, a Roman Catholic Bishop of the French Oratorian Society of Foreign Missions, consecrated Steenoven as a bishop without a papal mandate. What had been de jure autonomous became de facto an independent Catholic church. Steenoven appointed and ordained bishops to the sees of Deventer, Haarlem and Groningen. Although the pope was notified of all proceedings, the Holy See still regarded these dioceses as vacant due to papal permission not being sought. The pope, therefore, continued to appoint apostolic vicars for the Netherlands.{{sfn|Neale|1858}} Steenoven and the other bishops were excommunicated, and thus began the Old Catholic Church in the Netherlands.{{sfn|Neale|1858}}

While the religious clergy remained loyal to Rome, three-quarters of the secular clergy at first followed Codde, but by 1706 over two-thirds of these returned to Roman allegiance. Of the laity, the overwhelming majority sided with Rome. Thus most Dutch Catholics remained in full communion with the pope and with the apostolic vicars appointed by him. However, due to prevailing anti-papal feeling among the powerful Dutch Calvinists, the Church of Utrecht was tolerated and even praised by the government of the Dutch Republic.

In 1853 Pope Pius IX received guarantees of religious freedom from King William II of the Netherlands and re-established the Roman Catholic hierarchy in the Netherlands. This existed alongside that of the Old Catholic See of Utrecht. Thereafter in the Netherlands the Utrecht hierarchy was referred to as the "Old Catholic Church" to distinguish it from those in union with the pope. According to Roman Catholic Church interpretation, the Old Catholic Church of Utrecht maintained apostolic succession and its clergy celebrated valid sacraments.{{Failed verification|date=September 2013}} The Old Catholic Diocese of Utrecht was considered schismatic but not in heresy, but the Holy See sees the Roman Catholic Archdiocese of Utrecht as the continuation of the episcopal see founded in the 7th century and raised to metropolitan status on 12 May 1559.

====Second stage: Impact of the First Vatican Council====


After the First Vatican Council (1869–1870), several groups of Roman Catholics in Austria-Hungary, Imperial Germany, and Switzerland rejected the Roman Catholic dogma of papal infallibility in matters of faith and morals and left to form their own churches. These were supported by the Old Catholic Archbishop of Utrecht, who ordained priests and bishops for them. Later the Dutch were united more formally with many of these groups under the name "Utrecht Union of Churches".

In the spring of 1871 a convention in Munich attracted several hundred participants, including Church of England and Protestant observers. Döllinger, an excommunicated Roman Catholic priest and church historian, was a notable leader of the movement but was never a member of an Old Catholic Church.

The convention decided to form the "Old Catholic Church" in order to distinguish its members from what they saw as the novel teaching in the Roman Catholic dogma of papal infallibility. Although it had continued to use the Roman Rite, from the middle of the 18th century the Dutch Old Catholic See of Utrecht had increasingly used the vernacular instead of Latin. The churches which broke from the Holy See in 1870 and subsequently entered into union with the Old Catholic See of Utrecht gradually introduced the vernacular into the liturgy until it completely replaced Latin in 1877. In 1874 Old Catholics removed the requirement of clerical celibacy.
{{Papal primacy and infallibility|expanded=objections}}

The Old Catholic Church within the German Empire received support from the government of Otto von Bismarck, whose 1870s Kulturkampf policies persecuted the Roman Catholic Church.{{Page needed|date=February 2016}} In Austria-Hungary, pan-Germanic nationalist groups, like those of Georg Ritter von Schönerer, promoted the conversion of all German speaking Catholics to Old Catholicism and Lutheranism.{{Page needed|date=February 2016}}

====Third stage: Spread of Old/Independent Catholicism throughout the World====
In 1908 the Archbishop of Utrecht Gerardus Gul consecrated Father Arnold Harris Mathew, a former Roman Catholic priest, as Regionary Bishop for England. His mission was to establish a community for Anglicans and Catholics. During his time with the Old Catholics, Mathew attended the Old Catholic Congress in Vienna in 1909 as well as acted as co-consecrator of Archbishop Michael Kowalski of the Mariavite Church in Poland. In 1910, Mathew left the {{abbr|UU|Union of Utrecht of the Old Catholic Churches}} over his allegation of their becoming more Protestant and called his church the "Old Roman Catholic Church". This event signaled the beginning of clergy and communities identifying themselves as Old Catholic despite a lack of affiliation with the Union of Utrecht of Old Catholic Churches.

In 1913, Mathew consecrated Rudolph de Landas Berghes, who emigrated to the United States in 1914 and planted the seed of Old Catholicism in the Americas. He consecrated an excommunicated Capuchin Franciscan priest as bishop: Carmel Henry Carfora. From this the Old Catholic Church in the United States evolved into local and regional self-governing dioceses and provinces along the design of St. Ignatius of Antioch – a network of communities.

Another significant figure, Joseph René Vilatte, who was ordained a deacon and priest by Bishop Eduard Herzog, of the Christian Catholic Church of Switzerland., worked with Catholics of Belgian ancestry living on the Door Peninsula of Wisconsin, with the knowledge and blessing of the Union of Utrecht and under the full jurisdiction of the local Episcopal Bishop of Fond du Lac, Wisconsin. In time, Vilatte asked the Old Catholic Archbishop of Utrecht to be ordained a bishop so that he might confirm, but his petition was not granted because {{abbr|UU|Union of Utrecht of the Old Catholic Churches}} recognized the Episcopal Church (United States) as the local catholic church.

Over the years, hundreds of people in the United States have come to claim apostolic succession from Vilatte and/or other sources; none is in communion with, nor recognized by, the Union of Utrecht of Old Catholic Churches.

The Polish National Catholic Church (PNCC) in the U.S. was previously in communion with the Union of Utrecht of Old Catholic Church. In 2003 the church voted itself out of the {{abbr|UU|Union of Utrecht of the Old Catholic Churches}} because the {{abbr|UU|Union of Utrecht of the Old Catholic Churches}} accepted the ordination of women and has an open attitude towards homosexuality, both of which the Polish National Catholic Church rejects.

At present, the only recognized group in America that is in communion with the Union of Utrecht is the Episcopal Church. However, independent Old Catholic groups with recognized apostolic succession have attempted to seek recognition from the {{abbr|UU|Union of Utrecht of the Old Catholic Churches}}, including a 2006 attempt by the Old Catholic Communion of North America (OCCNA) and a 2006 attempt by four Independent Catholic bishops who self-identified as Old Catholic, which led to the short-lived Conference of North American Old Catholic Bishops. The Old Catholic Church, Province of the United States views itself as Old Catholic, but, like all other U.S churches outside of the Episcopal Church, is not recognized by the Union of Utrecht of Old Catholic Churches.

====Old Catholic Church of Slovakia====
The Old Catholic Church of Slovakia was accepted in 2000 as a member of the Union of Utrecht. As early as 2001 some issues arose concerning future consecration of Augustin Bacinsky as old-catholic bishop of Slovakia, and the matter was postponed. Old Catholic Church of Slovakia was expelled from the Union of Utrecht in 2004, because the episcopal administrator Augustin Bacinsky had been consecrated by an episcopus vagans.

===Numbers===
{{As of|2016}}, there are 115,000 members of Old Catholic churches.


|-
! Church !! Membership
|-
|Catholic Diocese of the Old-Catholics in Germany||15,500
|-
|Old-Catholic Church in Austria||14,621
|-
|Old-Catholic Church in the Netherlands||10,000
|-
|Old-Catholic Church of Switzerland||13,500
|-
|Old-Catholic Mariavite Church in Poland||29,000
|-
|Polish Catholic Church in Poland{{efn|Polish Catholic Church in Poland, a member church of the {{abbr|UU|Union of Utrecht of the Old Catholic Churches}}, is not to be confused with the Catholic Church in Poland or confused with the {{abbr|PNCC|Polish National Catholic Church}}, a former member church of the {{abbr|UU|Union of Utrecht of the Old Catholic Churches}}.}}||20,000
|}

==Ecumenism==
Immediately after forming the {{abbr|UU|Union of Utrecht of the Old Catholic Churches}}, Old Catholic theologians dedicated themselves to a reunion of the Christian churches. The Conferences of Reunion in Bonn in 1874 and 1875 convoked by Döllinger, a leading personality of Old Catholicism, are famous. Representatives of the Orthodox, Anglican and Lutheran churches were invited. They discussed denominational differences as the ground for restoring church communion. They assumed the following principles for participation: acceptance of the Christological dogma of First Council of Nicaea and Council of Chalcedon; Christ's foundation of the Church; the Holy Bible, the doctrine of the undivided Church, and the Church fathers of the first ten centuries as the genuine sources of belief; and Vincent of Lérins's Commonitory as a preferred method for historical research.

Reunion of the churches had to be based on a re-actualization of the decisions of faith made by the undivided Church. In that way the original unity of the Church could be made visible again. Following these principles, later bishops and theologians of the Old Catholic churches stayed in contact with Russian Orthodox and Anglican representatives.

Old Catholic involvement in the multilateral ecumenical movement formally began with the participation of two bishops, from the Netherlands and Switzerland, at the Lausanne Faith and Order (F&O) conference (1927). This side of ecumenism has always remained a major interest for Old Catholics who have never missed an F&O conference. Old Catholics also participate in other activities of the WCC and of national councils of churches. By active participation in the ecumenical movement since its very beginning, the OCC demonstrates its belief in this work.

==Apostolic succession==
Old Catholicism values apostolic succession by which they mean both the uninterrupted laying on of hands by bishops through time and the continuation of the whole life of the church community by word and sacrament over the years and ages. Old Catholics consider apostolic succession to be the handing on of belief in which the whole Church is involved. In this process the ministry has a special responsibility and task, caring for the continuation in time of the mission of Jesus Christ and his Apostles.

==Liturgy==
The Old Catholic Church shares some of the liturgy with the Catholic Church and similar to the Orthodox, Anglicans and high church Protestants.

Christ-Catholic Swiss bishop Urs Küry dismissed the Catholic dogma of transubstantiation because this Scholastic interpretation presumes to explain the Eucharist using the metaphysical concept of "substance". Like the Orthodox approach to the Eucharist, Old Catholics, he says, ought to accept an unexplainable divine mystery as such and should not cleave to or insist upon a particular theory of the sacrament. Because of this approach, Old Catholics hold an open view to most issues, including the role of women in the Church, the role of married people within ordained ministry, the morality of same sex relationships, the use of conscience when deciding whether to use artificial contraception, and liturgical reforms such as open communion. Its liturgy has not significantly departed from the Tridentine Mass, as is shown in the translation of the German altar book (missal).

In 1994 the German bishops decided to ordain women as priests, and put this into practice on 27 May 1996. Similar decisions and practices followed in Austria, Switzerland and the Netherlands. In 2020, the Swiss church also voted in favour of same-sex marriage. Marriages between two men and two women will be conducted in the same manner as heterosexual marriages. The {{abbr|UU|Union of Utrecht of the Old Catholic Churches}} allows those who are divorced to have a new marriage in the church, and has no particular teaching on abortion, leaving such decisions to the married couple.

An active contributor to the Declaration of the Catholic Congress, Munich, 1871, and all later assemblies for organization was Johann Friedrich von Schulte, the professor of dogma at Prague. Von Schulte summed up the results of the congress as follows:

* adherence to the ancient Catholic faith
* maintenance of the rights of Catholics
* rejection of new Catholic dogmas
* adherence to the constitutions of the ancient Church with repudiation of every dogma of faith not in harmony with the actual consciousness of the Church
* reform of the Church with constitutional participation of the laity
* preparation of the way for reunion of the Christian confessions
* reform of the training and position of the clergy
* adherence to the State against the attacks of Ultramontanism
* rejection of the Society of Jesus
* claim to the real property of the Church

==See also==
{{portal|Catholicism|Christianity|Religion}}

* Ultrajectine

===Churches===
* List of Old Catholic Churches
* Old Catholic Church in Europe
* Ecumenical Catholic Communion

===Movements===
* Liberal Catholic Movement
* Independent Catholic Churches
* King's Family of Churches
* Liberal Catholic Church
* Old Catholics for Christ
* Willibrord Society
* German Catholics (sect)

===People===
* Franz Heinrich Reusch
* Warren Prall Watters
* Gerard Shelley

==Notes==
{{notelist}}

==References==
{{Reflist|30em}}

==Bibliography==
* Episcopi Vagantes and the Anglican Church. Henry R.T. Brandreth. London: Society for Promoting Christian Knowledge, 1947.
* Episcopi vagantes in church history. A.J. Macdonald. London: Society for Promoting Christian Knowledge, 1945.
*{{Source-attribution|{{cite book|location=Oxford; London|publisher=John Henry and James Parker|last=Neale|first=John M|author-link=John Mason Neale|title=History of the so-called Jansenist church of Holland; with a sketch of its earlier annals, and some account of the Brothers of the common life|year=1858|oclc=600855086|hdl=2027/mdp.39015067974389}}}}
* The Old Catholic Church: A History and Chronology (The Autocephalous Orthodox Churches, No. 3). Karl Pruter. Highlandville, Missouri: St. Willibrord's Press, 1996.
* The Old Catholic Sourcebook (Garland Reference Library of Social Science). Karl Pruter and J. Gordon Melton. New York: Garland Publishers, 1983.
* The Old Catholic Churches and Anglican Orders. C.B. Moss. The Christian East, January, 1926.
* The Old Catholic Movement. C.B. Moss. London: Society for Promoting Christian Knowledge, 1964.

==Further reading==
* "La Sainte Trinité dans la théologie de Dominique Varlet, aux origines du vieux-catholicisme". Serge A. Thériault. Internationale Kirchliche Zeitschrift, Jahr 73, Heft 4 (Okt.-Dez. 1983), p.&nbsp;234-245.

==External links==
{{External links|section|date=February 2016}}
{{EB1911 poster|Old Catholics}}
{{NIE poster|Old Catholics}}

===Union of Utrecht===
* [http://www.utrechter-union.org/pagina/137/english Union of Utrecht of The Old Catholic Churches]
* [http://www.okkn.nl Old-Catholic Church of the Netherlands]
* [https://web.archive.org/web/20190419015946/https://www.alt-katholisch.de/ Catholic Diocese of the Old Catholics in Germany]
* [https://web.archive.org/web/20090309172401/http://www.christkath.ch/ Old-Catholic Church of Switzerland]
* [http://www.altkatholiken.at Old-Catholic Church of Austria]
* [http://www.starokatolici.cz Old-Catholic Church of the Czech Republic]
* [http://www.slovenski-katolici.sk Old-Catholic Church of Slovakia]

===Union of Utrecht dependent churches===
* [http://www.vieux-catholique-alsace.com/ Old-Catholic Mission in France] and [https://web.archive.org/web/20110720215927/http://www.fraternitestvincentdelerins.fr/ Fraternité St Vincent de Lérins]
* [https://web.archive.org/web/20091225105537/http://www.veterocattolici.it/ Old-Catholic Mission in Italy]
* [https://web.archive.org/web/20100811112923/http://www.gammalkatolik.se/ Old-Catholic Mission in Sweden and Denmark]

{{Union of Utrecht}}
{{Religion topics}}

{{Authority control}}


Category:1870 in Christianity
Category:Pope Pius IX
Category:1701 in Christianity
Category:Christian denominations established in the 18th century
Category:Independent Catholic denominations
