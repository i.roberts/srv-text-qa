
{{Redirect|Kolberg}}
{{Use dmy dates|date=June 2020}}
{{Infobox settlement
| name = Kołobrzeg
| image_skyline = Kolobrzeg pomnik i latarnia.jpg
| imagesize = 250px
| image_caption = Coastline of the Baltic Sea in Kołobrzeg and historical lighthouse.
| image_shield = Herb Kolobrzegu.svg
| image_flag = POL Kołobrzeg flag.svg
| pushpin_map = West Pomeranian Voivodeship#Poland
| pushpin_label_position = bottom
| subdivision_type = Country
| subdivision_name = Poland
| subdivision_type1 = Voivodeship
| subdivision_name1 = West Pomeranian
| subdivision_type2 = County
| subdivision_name2 = Kołobrzeg County
| subdivision_type3 = Gmina
| subdivision_name3 = Kołobrzeg <small>(urban gmina)</small>
| leader_title = President
| leader_name = Anna Mieczkowska
| established_title = Established
| established_date = 10th century
| established_title3 = Town rights
| established_date3 = 1255
| area_total_km2 = 25.67
| population_as_of = 2014
| population_total = 46830
| population_density_km2 = auto
| timezone = CET
| utc_offset = +1
| timezone_DST = CEST
| utc_offset_DST = +2
| coordinates = {{coord|54|10|N|15|34|E|region:PL|display=inline}}
| postal_code_type = Postal code
| postal_code = 78–100 to 78–106
| area_code = +48 94
| blank_name = Car plates
| blank_info = ZKL
| website = https://www.kolobrzeg.pl }}

Kołobrzeg (<small>pronounced</small> {{respell|Ko|wo|bzhek}} {{IPAc-pl|AUD|Pl-Kołobrzeg.ogg|k|o|'|ł|o|b|ż|e|k}}; {{lang-de|link=no|Kolberg}}, {{IPA-de|ˈkɔlbɛʁk||Kolberg German pronounciation.ogg}}; {{lang-csb|Kòlbrzég}}), historically known in English as Kolberg, is a city in the West Pomeranian Voivodeship in north-western Poland with about 47,000 inhabitants ({{As of|2014|lc=y}}). Kołobrzeg is located on the Parsęta River on the south coast of the Baltic Sea (in the middle of the section divided by the Oder and Vistula Rivers). It has been the capital of Kołobrzeg County in West Pomeranian Voivodship since 1999, and was in Koszalin Voivodship from 1950 to 1998.

During the Early Middle Ages, the Pomeranian tribes established a settlement at the site of modern-day Budzistowo. Thietmar of Merseburg first mentioned the site as Salsa Cholbergiensis. Around the year 1000, when the city was part of Poland, it became seat of the Diocese of Kołobrzeg, one of five oldest Polish dioceses. During the High Middle Ages, the town was expanded with an additional settlement inhabited by German settlers a few kilometers north of the stronghold and chartered with Lübeck law, which settlement eventually superseded the original Pomeranian settlement. The city later joined the Hanseatic League. Within the Duchy of Pomerania, the town was the urban center of the secular reign of the prince-bishops of Cammin and their residence throughout the High and Late Middle Ages. When it was part of Brandenburgian Pomerania during the Early Modern Age, it withstood Polish and Napoleon's troops in the Siege of Kolberg. From 1815, it was part of the Prussian province of Pomerania. 

In the late 19th century Kolberg became a popular spa town at the Baltic Sea. In 1945, Polish and Soviet troops captured the town, while the remaining German population which had not fled the advancing Red Army was expelled in accordance to the Potsdam Agreement. Kołobrzeg, now part of post-war Poland and devastated in the preceding Battle of Kolberg, was rebuilt, but lost its status as the regional center to the nearby city of Koszalin.

==Etymology==
"Kołobrzeg" means "by the shore" in Polish; "koło" translates as "by" and "brzeg" means "coast" or "shore". {{lang-csb|Kòłobrzeg}} has a similar etymology. The original name of Cholberg was taken by Polish and Kashubian linguists in the 19th and 20th centuries to reconstruct the name. After German settlement, the original name of Cholberg evolved into {{lang-de|link=no|Kolberg}} ({{IPA-de|ˈkɔlbɛʁk||Kolberg German pronounciation.ogg}}).

== History ==

=== Pomeranian stronghold at modern Budzistowo ===

According to Piskorski (1999) and Kempke (2001), Slavic and Lechitic immigration reached Farther Pomerania in the 7th century. First Slavic settlements in the vicinity of Kołobrzeg were centered around nearby deposits of salt and date to 6th and 7th century.

In the late 9th century, the Pomeranian tribes erected a fortified settlement at the site of modern part of Kołobrzeg county called Budzistowo near modern Kołobrzeg, replacing nearby Bardy-Świelubie, a multi-ethnic emporium, as the center of the region. The Parseta valley, where both the emporium and the stronghold were located, was one of the Pomeranians' core settlement areas. The stronghold consisted of a fortified burgh with a suburbium.

The Pomeranians mined salt in salt pans located in two downstream hills. They also engaged in fishing, and used the salt to conserve foodstuffs, primarily herring, for trade. Other important occupations were metallurgy and smithery, based on local iron ore reserves, other crafts like the production of combs from horn, and in the surrounding areas, agriculture. Important sites in the settlement were a place for periodical markets and a tavern, mentioned as forum et taberna in 1140.

In the 9th and 10th centuries, the Budzistowo stronghold was the largest of several smaller ones in the Persante area, and as such is thought to have functioned as the center of the local Pomeranian subtribe. By the turn from the 10th to the 11th century, the smaller burghs in the Parseta area were given up. With the area coming under control of the Polish Duke Mieszko I, only two strongholds remained and underwent an enlargement, the one at Budzistowo and a predecessor of later Białogard. These developments were most likely associated with the establishment of Polish power over this part of the Baltic coast. In the 10th century the trade of salt and fish led to the development of the settlement into a town.

=== Piast Poland and conversion ===


During Polish rule of the area in the late 10th century, the chronicle of Thietmar of Merseburg (975–1018) mentions salsa Cholbergiensis as the see of the Bishopric of Kołobrzeg, set up during the Congress of Gniezno in 1000 and placed under the Archdiocese of Gniezno. The congress was organized by Polish duke Bolesław Chrobry and Holy Roman Emperor Otto III, and also led to the establishment of bishoprics in Kraków and Wrocław, connecting the territories of the Polish state. It was an important event not only in religious, but also political dimension in the history of the early Polish state, as it unified and organized medieval Polish territories.

The missionary efforts of bishop Reinbern were not successful, the Pomeranians revolted in 1005 and regained political and spiritual independence. In 1013 Bolesław Chrobry removed his troops from Pomerania in face of war with Holy Roman Emperor Henry III. The Polish–German war ended with Polish victory, which was confirmed by the 1018 Peace of Bautzen.

During his campaigns in the early 12th century, Bolesław III Wrymouth reacquired Pomerania for Poland, and made the local "Griffin" dynasty his vassals. The stronghold was captured by the Polish army in the winter of 1107/08, when the inhabitants (cives et oppidani) including a duke (dux Pomeranorum) surrendered without resistance. A previous Polish siege of the burgh had been unsuccessful; although the duke had fled the burgh, the Polish army was unable to break through the fortifications and the two gates. The army had however looted and burned the suburbium, which was not or only lightly fortified. The descriptions given by the contemporary chroniclers make it possible that a second, purely militarily used castle existed near the settlement, yet neither is this certain nor have archaeological efforts been able to locate traces thereof. In the 12th-century Polish chronicle Gesta principum Polonorum Kołobrzeg was named a significant and famous city.

During the subsequent Christianization of the area by Otto of Bamberg at the behest of Boleslaw, a St. Mary's church was built. This marked the first beginnings of German influence in the area. After Boleslaw's death, as a result of the fragmentation of Poland, the Duchy of Pomerania became independent, before the dukes became vassals of Denmark and the Holy Roman Empire in the late 12th century.

Besides St. Mary's, a St. John's church and a St. Petri's chapel were built.
A painting of the town of Kołobrzeg from the 13th century is located in the Museum of Polish Arms in the city.

=== From the late Middle Ages to the Thirty Years' War ===
{{multiple image
 | align = right
 | direction = vertical
 | width = 220
 | image1 = 2019 - Kołobrzeg Katedra.jpg
 | alt1 = 
 | caption1 = 
 | image2 = 2019 - Kołobrzeg Katedra 3.jpg
 | alt2 = 
 | caption2 = Brick Gothic St. Mary's Basilica
}}

During the Ostsiedlung, a settlement was founded by German settlers some kilometres off the site of the Slavic/Lechitic one. It was located within the boundaries of today's downtown of Kołobrzeg and some of the inhabitants of the Polish town moved to the new settlement. On 23 May 1255 it was chartered under Lübeck law by Duke Wartislaw III of Pomerania, and more settlers arrived, attracted by the duke. Hermann von Gleichen, German bishop of Kammin also supported the German colonisation of the region. The settlers received several privileges such as exemption from certain taxes and several benefits, making it difficult for the indigenous Pomeranian population to compete with Germans.

Henceforth, the nearby former stronghold was turned into a village and renamed "Old Town" ({{lang-la|antiqua civitatae Colbergensis}}, {{lang-de|link=no|Altstadt}}, {{lang-pl|Stare Miasto}}), first documented in 1277 and used until 1945 when it was renamed "Budzistowo". A new St. Mary's church was built within the new town before the 1260s, while St. Mary's in the former Pomeranian stronghold was turned into a nuns' abbey. In 1277 St. Benedict's monastery for nuns was founded, which in the framework of the Pomeranian Reformation in 1545 was then changed into an educational institution for noble Protestant ladies.



Already in 1248, the Kammin bishops and the Pomeranian dukes had interchanged the terrae Stargard and Kolberg, leaving the bishops in charge of the latter. When in 1276 they became the souvereign of the town also, they moved their residence there, while the administration of the diocese was done from nearby Köslin (Koszalin). In 1345, the bishops became Imperial immediate dukes in their secular reign.

In 1361, the city joined the Hanseatic League. In 1446 it fought a battle against the nearby rival city of Koszalin.

When the property of the Bishopric of Kammin was secularized during the Protestant Reformation in 1534, their secular reign including the Kolberg area became intermediately ruled by a Lutheran titular bishop, before it was turned into a Sekundogenitur of the House of Pomerania.

In the 15th century the city traded with Scotland, Amsterdam and Scandinavia. Beer, salt, honey, wool and flour were exported, while merchants imported textiles from England, southern fruits, and cod liver oil. In the 16th century, the city reached 5,000 inhabitants. The indigenous Slavs in the city were discriminated, and their rights in trade and crafts were limited, with bans on performing certain types of professions and taking certain positions in the city, for instance in 1564 it was forbidden to admit native Slavs to the blacksmiths' guild.

During the Thirty Years' War, Kolberg was occupied by imperial forces from 1627 to 1630, and thereafter by Swedish forces.

===Modern era: In Prussia===

Kolberg, with most of Farther Pomerania, was granted to Brandenburg-Prussia in 1648 by the Treaty of Westphalia and, after the signing of the Treaty of Stettin (1653), was part of the Province of Pomerania. It became part of the Kingdom of Prussia in 1701. In the 18th century, trade with Poland declined, while the production of textiles developed. In 1761, during the Seven Years' War, the town was captured after three subsequent sieges by the Russian commander Peter Rumyantsev. At the end of the war, however, Kolberg was returned to Prussia.
{{Css Image Crop|Image = GER-COL-S-1453-Prussia-Siege of Kolberg-8 groschen-1807.jpg|bSize = 220|cWidth = 220|cHeight = 139|oTop = 0|oLeft = 0|Location = left|Description = Emergency issue currency for the Siege of Kolberg (1807), 8 groschen}}


During Napoleon's invasion of Prussia during the War of the Fourth Coalition, the town was besieged from mid-March to 2 July 1807 by the Grande Armée and by Polish forces drawn from insurgents against Prussian rule (a street named after General Antoni Paweł Sułkowski, who led Polish soldiers, is located within the present-day city). As a result of forced conscription, Poles were also among Prussian soldiers during the battle. The city's defense, led by then Lieutenant-Colonel August von Gneisenau, held out until the war was ended by the Treaty of Tilsit. Kolberg became part of the Prussian province of Pomerania in 1815, after the final defeat of Napoleon; until 1872, it was administered within the Fürstenthum District ("Principality District", recalling the area's former special status), then it was within Landkreis Kolberg-Körlin.
Marcin Dunin, archbishop of Poznań and Gniezno and Roman Catholic primate of Poland, was imprisoned by Prussian authorities for ten months in 1839–1840 in the cityPapiestwo wobec sprawy polskiej w latach 1772–1865:
wybór źródel Otton Beiersdorf Zaklad Narodowy im. Ossolinskich,1960 page 309</ref> and after his release, he tried to organise a chaplaincy for the many Polish soldiers stationed in Kolberg.name="II Rzeczypospolitej 1984, pages 139-146">Na stolicy prymasowskiej w Gnieźnie i w Poznaniu:
szkice o prymasach Polski w okresie niewoli narodowej i w II Rzeczypospolitej : praca zbiorowa Feliks Lenort Księgarnia Św. Wojciecha, 1984, pages 139–146</ref>

In the 19th century the city had a small but active Polish population that increased during the century to account for 1.5% of the population by 1905. The Polish community funded a Catholic school and the Church of Saint Marcin where masses in Polish were held (initially throughout the season, after about 1890 all the year), were established. Dating back to 1261 Kolberg's Jewish population amounted to 528 people in 1887, rising to 580 two years later, and although many moved to Berlin after that date they numbered around 500 by the end of the Nineteenth century

Between 1924 and 1935, the American-German painter Lyonel Feininger, a tutor at the Staatliches Bauhaus, visited Kolberg repeatedly and painted the cathedral and environs of the town.

In the May elections of 1933, the Nazi Party received by far the most votes, 9,842 out of 19,607 cast votes.

When the Nazis took power in Germany in 1933, the Jewish community in Kolberg comprised 200 people, and the antisemitic repression by Germany's ruling party led several of them to flee the country. A Nazi newspaper, the Kolberger Beobachter, listed Jewish shops and business that were to be boycotted. Nazis also engaged in hate propaganda against Jewish lawyers, doctors, and craftsmen. At the end of 1935, Jews were banned from working in the city's health spas. During Kristallnacht, the Jewish synagogue and homes were destroyed, and in 1938 the local Jewish cemetery was vandalised, while a cemetery shrine was turned to stable by German soldiers. In 1938, all Jews in Kolberg, as all over Germany, were renamed in official German documents as "Israel" (for males) or "Sarah" (for females). In the beginning of 1939, Jews were banned from attending German schools and the entire adult population had its driving licenses revoked. After years of discrimination and harassment, local Jews were deported by the German authorities to concentration camps in 1940.

===Second World War===

During the World War II the German state brought in numerous forced laborers to the city, among them many Poles. The city's economy was changed to military production-especially after the German invasion of the Soviet Union. The forced laborers were threatened with everyday harassment and repression; they were forbidden from using phones, holding cultural events and sports events, they could not visit restaurants or swimming pools, or have contact with the local German population. Poles were only allowed to attend a church mass once a month – and only in the German language. They also had smaller food rations than Germans, and had to wear a sign with the letter P on their clothes indicating their ethnic background. Additionally, medical help for Polish workers was limited by the authorities. Arrests and imprisonment for various offences, such as "slow pace of work" or leaving the workspace, were everyday occurrences.

In 1944, the city was designated a fortress — Festung Kolberg. The 1807 siege was used for the last Nazi propaganda film, Kolberg shortly before the end of the war by Joseph Goebbels . It was meant to inspire the Germans with its depiction of the heroic Prussian defence during the Napoleonic Wars. Tremendous resources were devoted to filming this epic, even diverting tens of thousands of troops from the front lines to have them serve as extras in battle scenes. Ironically, the film was released in the final few weeks of Nazi Germany's existence, when most of the country's cinemas were already destroyed.

On 10 February 1945, the German torpedo-boat T-196 brought about 300 survivors of the {{SS|General von Steuben||2}}, which had been sunk by Soviet submarine S-13 to Kolberg. As the Red Army advanced on Kolberg, most of the inhabitants and tens of thousands of refugees from surrounding areas (about 70,000 were trapped in the Kolberg Pocket), as well as 40,000 German soldiers, were evacuated from the besieged city by German naval forces in Operation Hannibal. Only about two thousand soldiers were left on 17 March to cover the last sea transports.

Between 4 and 18 March 1945, there were major battles between the Soviet and Polish forces and the German army. Because of a lack of anti-tank weapons, German destroyers used their guns to support the defenders of Kolberg until nearly all of the soldiers and civilians had been evacuated. During the fights, Polish soldiers' losses were 1,013 dead, 142 MIA and 2,652 wounded. On 18 March, the Polish Army re-enacted Poland's Wedding to the Sea ceremony, which had been celebrated for the first time in 1920 by General Józef Haller.

After the battle the city for several weeks was under Soviet administration, the Germans that had not yet fled were expelled and the city was plundered by the Soviet troops. Freed Polish forced laborers remained and were joined by Polish railwaymen from Warsaw destroyed by the Germans.

<gallery class="center" caption="Before, during, and after the war" widths="170px" heights="140px" perrow="3">
File:Kolobrzeg c1890-1905 LOC 00729u.jpg|Kolberg between 1890 and 1905
File:Kolobrzeg1945.JPG|80% of the city destroyed in 1945
File:Kołobrzeg.jpg|Ratuszowy Square, Kolobrzeg in 2019
</gallery>

===Post-war Poland===

After World War II the region became part of Poland, under territorial changes demanded by the Soviet Union and the Polish Communist regime at the Potsdam Conference. Most Germans that had not yet fled were expelled from their homes. The town was re-settled by Polish citizens, many of whom were themselves Polish refugees from regions east of the Curzon line, the Kresy, from where they had been displaced by Soviet authorities.

In 2000 the city business council of Kołobrzeg commissioned a monument called the Millennium Memorial as a commemoration of "1000 years of Christianity in Pomerania", and as a tribute to Polish-German Reconciliation, celebrating the meeting of King Boleslaw I of Poland and King Otto III of Germany, at the Congress of Gniezno, in the year 1000. It was designed and built by the artist Wiktor Szostalo in welded stainless steel. The two figures sit at the base of a 5-meter cross, cleft in two and being held together by a dove holding an olive branch. It is installed outside the Basilica Cathedral in the city center.

==Demographics==
Before the end of World War II the town was predominantly German Protestant with Polish and Jewish minorities. Almost all of the pre-war German population fled or was expelled so that since 1945, Polish Catholics make up the majority of the population. Around the turn from the 18th to the 19th century an increase of the number of Catholics was observed, because military personnel had been moved from West Prussia to the town.{{citation needed|date=March 2011}} The mother tongue of a number of soldiers serving in the garrison of Kolberg was Polish. 




|+ Number of inhabitants in years
|-
! Year
! Inhabitants
|-
| 1740 || 5,027
|-
| 1782 || 4,006
|-
| 1794 || 4,319
|-
| 1812 || 5,597
|-
| 1816 || 5,210
|-
| 1831 || 6,221
|-
| 1843 || 7,528
|-
| 1852 || 8,658
|-
| 1861 || 10,082
|-
| 1900 || 20,200
|-
| 1925 || 30,115 
|-
| 1940 || 36,800
|-
| 1945 || approx. 3,000
|-
| 1950 || 6,800
|-
| 1960 || 16,700
|-
| 1975 || 31,800
|-
| 1990 || 45,400
|-
| 2002 || 47,500
|-
| 2004 || 45,500
|-
| 2014 || 46,830
|}

== Tourist destination ==
Kołobrzeg today is a popular tourist destination for Poles, Germans and due to the ferry connection to Bornholm also Danish people. It provides a unique combination of a seaside resort, health resort, an old town full of historic monuments and tourist entertainment options (e.g. numerous "beer gardens").


=== Bike path to Podczele ===
The town is part of the European Route of Brick Gothic network. A bike path "to Podczele", located along the seaside was commissioned on 14 July 2004. The path extends from Kołobrzeg to Podczele. The path has been financed by the European Union, and is intended to be part of a unique biking path that will ultimately circle the entire Baltic Sea. The path was breached on 24 March 2010 due to the encroachment of the sea associated with the draining of the adjacent unique Eco-Park marsh area. The government of Poland has allocated PLN 90,000 to repair the breach, and the path re-opened within a year. It was also extended in 2011 to connected with Ustronie Morskie {{convert|8|km|0|abbr=on}} to the east.

=== Oldest oak ===
South of Bagicz, some {{convert|4|km|0|abbr=off}} from Kołobrzeg, there is an 806-year-old oak (2008). Dated in the year 2000 as the oldest oak in Poland, it was named Bolesław to commemorate the king Boleslaus the Brave.

=== Cultural center ===
Kołobrzeg is also a regional cultural center. In the summer take place – a number of concerts of popular singers, musicians, and cabarets. Municipal Cultural Center, is located in the Park teatralny. Keep under attachment artistic arts, theater and dance. Patron of youth teams and the vocal choir. Interfolk organizes the annual festival, the International Meeting of the folklore and other cultural events. Cinema is a place for meetings Piast Discussion Film Club.

In Kołobrzeg there are many permanent and temporary exhibitions of artistic and historical interest. In the town hall of Kołobrzeg is located Gallery of Modern Art, where exhibitions are exposed artists from Kołobrzeg, as well as outside the local artistic circles. Gallery also conducts educational activities, including organized by the gallery of art lessons for children and young people from schools.


=== Pier ===
The Kołobrzeg Pier is currently the second longest pier in the West Pomeranian Voivodeship, after the pier in Międzyzdroje. A jetty positioned on the end of the pier enables small ships to sail for sightseeing excursions.

=== Museums ===

In town, there is a museum of Polish weapons (Muzeum Oręża Polskiego), which are presented in the collections of militaria from the early Middle Ages to the present. The palace of Braunschweig include part of museum dedicated to the history of the city. In their collections branch presents a collection of rare and common measurement tools, as well as specific measures of the workshop. The local museum is also moored at the port of ORP Fala patrol ship, built in 1964, after leaving the service transformed into a museum.

==Train connections==

Kołobrzeg has connections among others to Szczecin, "Solidarity" Szczecin–Goleniów Airport, Gdańsk, Poznań, Warsaw, Kraków and Lublin.

==Sport==
* SKK Kotwica Kołobrzeg – basketball club, which in the 2000s and 2010s competed in the Polish Basketball League, country's top flight
* Kotwica Kołobrzeg – football club

==Notable residents==

* Petrus Pachius (1579-1641/42) a German Protestant minister, teacher and poet
* Karl Wilhelm Ramler (1725–1798), poet, translator, director at Berlin theater 
=== 19th C ===
* Hermann Pluddemann (1809–1868) a German historical painter
* Ernst Maass (1856–1929) a German classical philologist. 
* Magnus Hirschfeld (1868–1935), physician, sociologist and early 20th century Gay rights campaigner
* Paul Oestreich (1878–1959), educator, reformer
* Arnold Zadikow (1884–1943), German-Jewish sculptor
* Hans-Jürgen Stumpff (1889–1968), German general of Luftwaffe, co-signer of unconditional surrender 8 May 1945 in Berlin
* Günther Angern (1893–1943), Wehrmacht general 
=== 20th C ===

* Werner Krüger (1910–2003), German engineer, invented Krueger flap in 1943
* Erika von Brockdorff (1911–1943), German resistance fighter
* Karl-Heinz Marbach (1917–1995), German U-boat commander 
* Egon Krenz (born 1937), last communist leader of East Germany
* Christine Lucyga (born 1944), politician
* Joanna Nowicka (born 1966) a Polish archer, competed in four consecutive Summer Olympics from 1988. 
* Sebastian Karpiniuk (1972–2010) a Polish politician, an assistant to President of Kolobrzeg, died in plane crash
* Dariusz Trafas (born 1972), athlete, javelin throw national record holder
* Daria Korczyńska (born 1981) a retired track and field sprint athlete 
* Robert Szpak (born 1989), athlete, javelin throw, 2008 World Junior Champion
* Maja Hyży (born 1989) a Polish singer, participated in the Eurovision Song Contest 2018

=== Famous persons connected with the city ===
* Marcin Dunin (1774–1842) archbishop of Poznań and Gniezno, primate of Poland. Imprisoned in the fortress in the city
* Friedrich Ludwig Jahn, (1778–1852), father of gymnastics, was imprisoned in Kolberg fortress in the 1820s 
* Adolf von Lützow, (1782–1834) a Prussian officer, served with distinction in the siege of Kolberg in 1807 
* Wiktor Szostalo, (born 1952) sculptor and former Solidarity activist.
* Jan Pogány, (born 1960) classical composer, conductor and cellist.
* Ryszard Kukliński, (1930–2004) colonel and spy for NATO in the Cold War period, attended high school in the city.

==International relations==
{{See also|List of twin towns and sister cities in Poland}}

===Twin towns – sister cities===
Kołobrzeg is twinned with:
{{Div col|colwidth=18em}}
* {{flagicon|GER}} Bad Oldesloe, Germany
* {{flagicon|GER}} Barth, Germany
* {{flagicon|GER}} Berlin Pankow, Germany
* {{flagicon|UKR}} Feodosiia, Ukraine
* {{flagicon|ITA}} Follonica, Italy
* {{flagicon|BEL}} Koekelberg, Belgium
* {{flagicon|SWE}} Landskrona, Sweden
* {{flagicon|DEN}} Nexø, Denmark
* {{flagicon|DEN}} Nyborg, Denmark
* {{flagicon|FIN}} Pori, Finland
* {{flagicon|SWE}} Simrishamn, Sweden

{{div col end}}
{{Coord|54|11|N|15|35|E|region:PL_type:city|display=title}}

==See also==
*Herbertiada

==References==
===Literature===
* {{in lang|de}} Gustav Kratz: Die Städte der Provinz Pommern – Abriss ihrer Geschichte, zumeist nach Urkunden (The Towns of the Province of Pomerania – Sketch of their History, mostly according to historical Records). Berlin 1865 (reprinted in 1996 by Sändig Reprint Verlag, Vaduz, {{ISBN|3-253-02734-1}}; reprinted in 2011 by Kessinger Publishing, U.S.A., {{ISBN|1-161-12969-3}}), pp.&nbsp;81–99 ([https://archive.org/details/bub_gb_g2sRAAAAYAAJ/page/n175 online])
===Notes===
{{Reflist}}

== External links ==
{{Commons|Kołobrzeg}}
* {{cite EB1911|wstitle=Kolberg |volume= 15 |short= x}}
* [http://www.kolobrzeg.eu Municipal website] {{in lang|pl|de|en}}
* [https://web.archive.org/web/20081007030444/http://www.turystyka.kolobrzeg.eu/uk/index.htm History of the town on the tourist promotion site] {{in lang|en}}
* [https://books.google.com/books?id=mOgRAAAAYAAJ&pg=PA123&dq=cassubia&hl=en&sa=X&ei=DG10VOGJJOHgyQPTsIG4Bg&ved=0CFEQ6AEwBTiWAQ#v=onepage&q=cassubia&f=false dutchy of Cassubia] {{in lang|en}}
{{Kołobrzeg County}}
{{Gmina Kołobrzeg}}
{{Polish coast|state=autocollapse}}
{{Pomerania}}

{{Authority control}}

{{DEFAULTSORT:Kolobrzeg}}

Category:Pomerania
Category:Cities and towns in West Pomeranian Voivodeship
Category:Kołobrzeg County
Category:Port cities and towns of the Baltic Sea
Category:Spa towns in Poland
Category:Resorts in Poland
Category:Members of the Hanseatic League
Category:Holocaust locations in Poland