{{Infobox CPU architecture
| name       = PA-RISC (HP/PA)
| designer   = [[Hewlett-Packard]]
| bits       = [[64-bit]] (32→64)
| introduced = 1986 (1996 PA-RISC 2.0)
| version    = 2.0 (1996)
| design     = RISC
| type       = 
| encoding   = Fixed
| branching  = Compare and branch
| endianness = Big
| extensions = [[Multimedia Acceleration eXtensions]] (MAX), MAX-2
| open       = No
| gpr        = 32
| fpr        = 32 64-bit (16 64-bit in PA-RISC 1.0)
}}
[[Image:HP PA-RISC 7300LC.jpg|thumb|[[Hewlett-Packard|HP]] PA-RISC 7300LC microprocessor]]
[[File:HP-HP9000-C110-Workstation 21.jpg|thumb|[[HP 9000]] C110 PA-RISC [[workstation]] booting [[Debian]] [[GNU]]/[[Linux]] ]]

'''PA-RISC''' is an [[instruction set architecture]] (ISA) developed by [[Hewlett-Packard]]. As the name implies, it is a [[reduced instruction set computer]] (RISC) architecture, where the PA stands for '''Precision Architecture'''. The design is also referred to as '''HP/PA''' for '''Hewlett Packard Precision Architecture'''.

The architecture was introduced on 26 February 1986, when the [[HP 3000|HP 3000 Series 930]] and [[HP 9000|HP 9000 Model 840]] [[Computer|computers]] were launched featuring the first implementation, the TS1.<ref>"One Year Ago". (26 February 1987). ''Computer Business Review''.</ref><ref>{{cite journal |first=Peter |last=Rosenbladt |title=In this Issue |date=September 1987 |journal=Hewlett-Packard Journal |volume=38 |issue=9 |page=3 |quote=... In the March 1987 issue we described the HP 3000 Series 930 and HP 9000 Model 840 Computers, which were HP's first realizations of HP Precision Architecture in off-the-shelf TTL technology. ... |url=http://www.hpl.hp.com/hpjournal/pdfs/IssuePDFs/1987-09.pdf }}</ref>

PA-RISC has been succeeded by the [[Itanium]] (originally IA-64) ISA, jointly developed by HP and [[Intel]].<ref>[http://www.informationweek.com/story/showArticle.jhtml?articleID=164302278 HP Completes Its PA-RISC Road Map With Final Processor Upgrade]</ref> HP stopped selling PA-RISC-based HP 9000 systems at the end of 2008 but supported servers running PA-RISC chips until 2013.<ref>[http://www.hp.com/products1/evolution/9000/faqs.html#2 How long will HP continue to support HP 9000 systems?]</ref>

==History==
In the late 1980s, HP was building four series of computers, all based on [[Complex instruction set computer|CISC]] CPUs. One line was the [[IBM PC compatible]] Intel [[i286]]-based Vectra Series, started in 1986. All others were non-[[Intel]] systems. One of them was the HP Series 300 of [[Motorola 68000]]-based [[workstation]]s, another Series 200 line of technical workstations based on a custom [[silicon on sapphire]] (SOS) chip design, the SOS based 16-bit [[HP 3000]] classic series, and finally the HP 9000 Series 500 [[minicomputer]]s, based on their own (16 and 32-bit) [[HP FOCUS|FOCUS]] microprocessor.

The Precision Architecture is the result of what was known inside Hewlett-Packard as the '''Spectrum''' program.<ref>{{cite journal |url=http://www.hpl.hp.com/hpjournal/pdfs/IssuePDFs/1986-08.pdf |first=William S. |last=Worley |title= Hewlett-Packard Precision Architecture: The Processor |journal=Hewlett-Packard Journal |date=August 1986 |volume=37 |issue=8 |pages=4–22 |quote=The HP Precision Architecture development program, known within HP as the Spectrum program, ...}}</ref> HP planned to use Spectrum to move all of their non-PC compatible machines to a single RISC CPU family.

Work began on the Precision Architecture at HP Laboratories in early 1982 defining the instruction set and virtual memory system, and the first [[transistor–transistor logic|TTL]] implementation began in April 1983, with simulation of the processor occurring in 1983, and with a complete processor delivered to software developers in July 1984. Systems prototyping followed, with "lab prototypes" being produced in 1985 and product prototypes in 1986.<ref name="fotland198703">{{ cite journal | url=https://archive.org/details/Hewlett-Packard_Journal_Vol._38_No._3_1987-03_Hewlett-Packard/page/n3/mode/2up | title=Hardware Design of the First HP Precision Architecture Computers | last1=Fotland | first1=David A. | last2=Shelton | first2=John F. | last3=Bryg | first3=William R. | last4=La Fetra | first4=Ross V. | last5=Boschma | first5=Simin I. | last6=Yeh | first6=Allan S. | last7=Jacobs | first7=Edward M. | journal=Hewlett-Packard Journal | date=March 1987 | access-date=6 October 2020 | volume=38 | issue=3 | pages=4-17 }}</ref>

The first processors were introduced in products during 1986. It had thirty-two 32-bit integer registers and sixteen 64-bit floating-point registers. The number of floating-point registers was doubled in the 1.1 version to 32 once it became apparent that 16 were inadequate and restricted performance. The architects included Allen Baum, Hans Jeans, Michael J. Mahon, [[Ruby B. Lee|Ruby Bei-Loh Lee]], Russel Kao, [[Steve Muchnick]], Terrence C. Miller, David Fotland, and William S. Worley.<ref>Smotherman, Mark (2 July 2009). [http://www.cs.clemson.edu/~mark/architects.html ''Recent Processor Architects''].</ref>

The first implementation was the TS1, a central processing unit built from discrete [[transistor–transistor logic]] ([[74F TTL]]) devices. Later implementations were multi-chip VLSI designs fabricated in NMOS processes (NS1 and NS2) and CMOS (CS1 and PCX).<ref>
Paul Weissmann.
[http://www.openpa.net/systems/hp_early-systems.html "Early PA-RISC Systems"].
</ref>
They were first used in a new series of [[HP 3000]] machines in the late 1980s&nbsp;– the 930 and 950, commonly known at the time as Spectrum systems, the name given to them in the development labs. These machines ran [[HP Multi-Programming Executive|MPE-XL]]. The [[HP 9000]] machines were soon upgraded with the PA-RISC processor as well, running the [[HP-UX]] version of [[Unix|UNIX]].

Other operating systems ported to the PA-RISC architecture include [[Linux]], [[OpenBSD]], [[NetBSD]] and [[NeXTSTEP]].

An interesting aspect of the PA-RISC line is that most of its generations have no Level 2 [[CPU cache|cache]]. Instead large Level 1 caches are used, formerly as separate chips connected by a bus, and now integrated on-chip. Only the PA-7100LC and PA-7300LC had L2 caches. Another innovation of the PA-RISC was the addition of vectorized instructions ([[SIMD]]) in the form of [[Multimedia Acceleration eXtensions|MAX]], which were first introduced on the PA-7100LC.

'''Precision RISC Organization''', an industry group led by HP, was founded in 1992, to promote the PA-RISC architecture.  Members included [[Convex Computer|Convex]], [[Hitachi]], [[Hughes Aircraft Company|Hughes Aircraft]], [[Mitsubishi Electric|Mitsubishi]], [[NEC]], [[Oki Electric Industry|OKI]], [[Prime Computer|Prime]], [[Stratus Technologies|Stratus]], [[Yokogawa Electric|Yokogawa]], [[Red Brick Software]], and [[Allegro Consultants, Inc.]]. 

The ISA was extended in 1996 to 64&nbsp;bits, with this revision named PA-RISC 2.0. PA-RISC 2.0 also added [[fused multiply–add]] instructions, which help certain floating-point intensive algorithms, and the [[Multimedia Acceleration eXtensions|MAX-2]] SIMD extension, which provides instructions for accelerating multimedia applications. The first PA-RISC 2.0 implementation was the [[PA-8000]], which was introduced in January 1996.

==CPU specifications==
{| class="wikitable"
!Model&nbsp;&nbsp;&nbsp;!!Marketing name!!Year!!Frequency [MHz]!!Memory Bus [MB/s]!!Process [μm]!!Transistors [millions]!!Die size [mm²]!!Power [W]!!Dcache [kB]!!Icache [kB]!!L2 cache [MB]!!ISA!!Notes
|-
| TS-1 ||?||1986||8||?||?||—||—||?||?||?||—||1.0||
|-
| CS-1 ||?||1987||8||?||1.6||0.164||72.93||1||—||0.25||—||1.0||<ref>{{cite book |first=A. |last=Marston  |display-authors=etal|chapter=A 32b CMOS single-chip RISC type processor |title=1987 IEEE International Solid-State Circuits Conference. Digest of Technical Papers |year=1987 |pages=28–29 |doi=10.1109/ISSCC.1987.1157145 |ref={{harvid|ISSCC|1987}}}}</ref>
|-
| NS-1 ||?||1987||25/30||?||1.5||0.144||70.56||?||?||?||—||1.0||<ref>{{cite book |first=J. |last=Yetter  |display-authors=etal|chapter=A 15 MIPS 32b Microprocessor |title={{harvnb|ISSCC|1987}} |year=1987 |doi=10.1109/ISSCC.1987.1157220 |pages=26–27}}</ref>
|-
| NS-2 ||?||1989||27.5/30||?||1.5||0.183||196||27||512||512||—||1.0||<ref>{{cite book |first=Brian D. |last=Boschma  |display-authors=etal|chapter=A 30 MIPS VLSI CPU |doi=10.1109/ISSCC.1989.48191 |title=IEEE International Solid-State Circuits Conference, 1989 ISSCC. Digest of Technical Papers |year=1989 |pages=82–83, 299 }}</ref>
|-
| PCX  ||?||1990||?||?||?||?||?||?||?||?||?||1.0||
|-
| PCX-S || PA-7000 ||1991||66||?||1.0||0.58||201.6||?||256||256||—||1.1a||
|-
| PCX-T || [[PA-7100]] ||1992||33–100||?||0.8||0.85||196||?||2048||1024||—||1.1b||
|-
| PCX-T || [[PA-7150]] ||1994||125||?||0.8||0.85||196||?||2048||1024||—||1.1b||
|-
| PCX-T' || [[PA-7200]] ||1994||120||960||0.55||1.26||210||30||1024||2048||—||1.1c||
|-
| PCX-L || [[PA-7100LC]] ||1994||60–100||?||0.75||0.9||201.6||7–11||—||1||2||1.1d||
|-
| PCX-L2 || [[PA-7300LC]] ||1996||132–180||?||0.5||9.2||260.1||?||64||64||0–8||1.1e||
|-
| PCX-U || [[PA-8000]] ||1996||160–180||960||0.5||3.8||337.68||?||1024||1024||—||2.0||
|-
| PCX-U+ || [[PA-8000#PA-8200|PA-8200]] ||1997||200–240||960||0.5||3.8||337.68||?||2048||2048||—||2.0||
|-
| PCX-W  || [[PA-8000#PA-8500|PA-8500]] ||1998||300–440||1920||0.25||140||467||?||1024||512||—||2.0||<ref name="l1000">[http://www.openpa.net/systems/hp_l1000_l2000-rp5400_rp5450.html "HP L1000 & L2000 (rp5400/rp5450) Servers"], ''openpa.net''</ref>
|-
| PCX-W+ || [[PA-8000#PA-8600|PA-8600]] ||2000||360–550||1920||0.25||140||467||?||1024||512||—||2.0||<ref name="l1000" />
|-
| PCX-W2 || [[PA-8000#PA-8700|PA-8700]](+) ||2001||625–875||1920||0.18||186||304||<7.1@1.5&nbsp;V||1536||768||—||2.0||
|-
| Mako || [[PA-8800#PA-8800|PA-8800]] ||2003||800–1000||6400||0.13||300||361||?||768/core||768/core||0 or 32||2.0||
|-
| Shortfin || [[PA-8000#PA-8900|PA-8900]] ||2005||800–1100||6400||0.13||?||?||?||768/core||768/core||0 or 64||2.0||
|}

==See also==
* [[Hombre chipset]] – A PA-7150-based chipset with a complete multimedia system for [[Commodore International|Commodore]]-[[Amiga]]

==References==
{{reflist}}

==External links==
* [https://web.archive.org/web/20150720041117/http://www.lostcircuits.com/mambo/index.php?option=com_content&task=view&id=42&Itemid=42 LostCircuits] Hewlett Packard PA8800 RISC Processor overview
* [https://web.archive.org/web/20150421181801/http://h21007.www2.hp.com/portal/site/dspp/menuitem.1b39e60a9475acc915b49c108973a801?chid=6dc55e210a66a010VgnVCM100000275d6e10RCRD HP's documentation] – page down for PA-RISC, architecture PDFs available
* [http://www.openpa.net/ OpenPA.net] Comprehensive PA-RISC chip and computer information
* [http://www.chipdb.org/cat-pa-risc-592.htm chipdb.org] Images of different PA-RISC processors

{{RISC-based processor architectures}}
{{CPU technologies}}


[[Category:HP microprocessors]]
[[Category:Instruction set architectures]]
[[Category:Computer-related introductions in 1986]]