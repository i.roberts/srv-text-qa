{{About|the British mathematician|the American mathematician|John B. Conway|other people|John Conway (disambiguation)}}
{{short description|English mathematician (1937–2020)}}
{{Expand language|topic=|langcode=ru|otherarticle=|date=April 2020}}
{{EngvarB|date=August 2014}}
{{Use dmy dates|date=April 2020}}
{{Infobox scientist
| name = John Horton Conway
| honorific_suffix  = {{post-nominals|country=GBR|FRS|size=100%}}
| image = John H Conway 2005 (cropped).jpg
| image_size = 250px
| caption = Conway in June 2005
| birth_date = {{birth date|df=yes|1937|12|26}}
| birth_place = [[Liverpool]], England
| death_date = {{Death date and age|df=yes|2020|4|11|1937|12|26}}
| death_place = [[New Brunswick, New Jersey|New Brunswick]], [[New Jersey]], U.S.
| education = [[Gonville and Caius College, Cambridge]] (BA, MA, PhD)
| thesis_title = Homogeneous ordered sets
| thesis_url = http://ethos.bl.uk/OrderDetails.do?uin=uk.bl.ethos.597910
| thesis_year = 1964
| doctoral_advisor = [[Harold Davenport]]<ref name="mathgene">{{MathGenealogy|id=18849}}</ref>
| doctoral_students = {{Plainlist|
* [[Richard Borcherds]]<ref name="mathgene"/>
* [[Adrian Mathias]]<ref name="mathgene"/>
* [[Simon P. Norton|Simon Norton]]<ref name="mathgene"/>
* [[Robert Arnott Wilson|Robert Wilson]]<ref name="mathgene"/>}}
| known_for = {{Plainlist|
* ''[[ATLAS of Finite Groups]]''
* [[Conway chained arrow notation]]
* [[Conway criterion]]
* [[Conway group]]s
* [[Conway notation (knot theory)]]
* [[Conway polyhedron notation]]
* [[Conway's Game of Life]]
* [[Doomsday algorithm]]
* [[Free will theorem]]
* [[Icosians]]
* [[Look-and-say sequence]]
* [[Mathieu groupoid]]
* [[Monstrous moonshine]]
* [[Pinwheel tiling]]
* [[Surreal number]]s}}
| website = {{URL|https://web.archive.org/web/20200315134415/https://www.math.princeton.edu/people/john-conway|Archived version @ web.archive.org}}
| footnotes = 
| field = [[Mathematics]]
| work_institutions = [[Princeton University]]
| prizes = {{Plainlist|
* [[Berwick Prize]] (1971)
* {{nowrap| [[Fellow of the Royal Society]] (1981)}}
* [[Pólya Prize (LMS)|Pólya Prize]] (1987)
* {{nowrap|[[Nemmers Prize in Mathematics]] (1998)}}
* [[Leroy P. Steele Prize]] (2000)}}
}}

'''John Horton Conway''' {{post-nominals|country=GBR|size=100%|FRS}} (26 December 1937 – 11 April 2020) was an English mathematician active in the theory of [[finite group]]s, [[knot theory]], [[number theory]], [[combinatorial game theory]] and [[coding theory]]. He also made contributions to many branches of [[recreational mathematics]], most notably the invention of the [[cellular automaton]] called the [[Conway's Game of Life|Game of Life]].

Born and raised in [[Liverpool]], Conway spent the first half of his career at the [[University of Cambridge]] before moving to the [[United States]], where he held the [[John von Neumann]] Professorship at [[Princeton University]] for the rest of his career.<ref>{{Cite journal | doi = 10.1080/10586458.1996.10504585| title = Packing Lines, Planes, etc.: Packings in Grassmannian Spaces| journal = Experimental Mathematics| volume = 5| issue = 2| pages = 139| year = 1996| last1 = Conway | first1 = J. H. | last2 = Hardin | first2 = R. H. | last3 = Sloane | first3 = N. J. A. | arxiv = math/0208004| s2cid = 10895494}}</ref><ref>{{Cite journal | doi = 10.1109/18.59931| title = A new upper bound on the minimal distance of self-dual codes| journal = IEEE Transactions on Information Theory| volume = 36| issue = 6| pages = 1319| year = 1990| last1 = Conway | first1 = J. H. | last2 = Sloane | first2 = N. J. A. }}</ref><ref>{{Cite journal | doi = 10.1016/0097-3165(93)90070-O| title = Self-dual codes over the integers modulo 4| journal = Journal of Combinatorial Theory, Series A| volume = 62| pages = 30–45| year = 1993| last1 = Conway | first1 = J. H. | last2 = Sloane | first2 = N. J. A. | doi-access = free}}</ref><ref>{{Cite journal | doi = 10.1109/TIT.1982.1056484| title = Fast quantizing and decoding and algorithms for lattice quantizers and codes| journal = IEEE Transactions on Information Theory| volume = 28| issue = 2| pages = 227| year = 1982| last1 = Conway | first1 = J.| last2 = Sloane | first2 = N.| url = http://neilsloane.com/doc/Me83.pdf| citeseerx = 10.1.1.392.249}}</ref><ref>{{Cite journal | doi = 10.1016/0097-3165(90)90057-4| title = Tiling with polyominoes and combinatorial group theory| journal = Journal of Combinatorial Theory, Series A| volume = 53| issue = 2| pages = 183| year = 1990| last1 = Conway | first1 = J. H. | last2 = Lagarias | first2 = J. C. }}</ref><ref name=mactutor>MacTutor History of Mathematics archive: [https://www-history.mcs.st-andrews.ac.uk/Biographies/Conway.html John Horton Conway]</ref> On 11 April 2020, at age 82, he died of complications from [[Coronavirus disease 2019|COVID-19]].<ref name="bellos-2020">
{{Cite web|date=2020-04-12|title=COVID-19 Kills Renowned Princeton Mathematician, 'Game Of Life' Inventor John Conway In 3 Days|url=https://dailyvoice.com/new-jersey/mercer/obituaries/covid-19-kills-renowned-princeton-mathematician-game-of-life-inventor-john-conway-in-3-days/786461/|access-date=2020-11-25|website=Mercer Daily Voice|language=en}}
</ref>

==Early life==
Conway was born on 26 December 1937 in [[Liverpool]], the son of Cyril Horton Conway and Agnes Boyce.<ref name="whoswho">{{cite web |url=http://www.ukwhoswho.com/view/article/oupww/whoswho/U11688 |title=CONWAY, Prof. John Horton |work=Who's Who 2014, A & C Black, an imprint of Bloomsbury Publishing plc, 2014; online edn, Oxford University Press }}{{subscription required}}</ref><ref name="mactutor" /> He became interested in mathematics at a very early age. By the time he was 11, his ambition was to become a mathematician.<ref>{{cite web|title=John Horton Conway|website=Dean of the Faculty, Princeton University|url=https://dof.princeton.edu/about/clerk-faculty/emeritus/john-horton-conway}}</ref><ref name="frontiers">{{cite book|title=Mathematical Frontiers|url=https://books.google.com/books?id=gmCSpNhXMooC&pg=PA38 | page=38 | publisher=Infobase Publishing | year = 2006 | isbn=978-0-7910-9719-9}}</ref> After leaving [[sixth form]], he studied mathematics at [[Gonville and Caius College, Cambridge]].<ref name="whoswho"/> A "terribly introverted adolescent" in school, he took his admission to Cambridge as an opportunity to transform himself into an extrovert, a change which would later earn him the nickname of "the world's most charismatic mathematician".<ref name=Guardian>{{cite news|last1=Roberts|first1=Siobhan|author-link=Siobhan Roberts|title=John Horton Conway: the world's most charismatic mathematician|url=https://www.theguardian.com/science/2015/jul/23/john-horton-conway-the-most-charismatic-mathematician-in-the-world|newspaper=[[The Guardian]]|date=23 July 2015}}</ref><ref name="Ronan2006">{{cite book|author=Mark Ronan|title=Symmetry and the Monster: One of the greatest quests of mathematics|url=https://archive.org/details/symmetrymonstero0000rona|url-access=registration|date=18 May 2006|publisher=Oxford University Press, UK|isbn=978-0-19-157938-7|pages=[https://archive.org/details/symmetrymonstero0000rona/page/163 163]|author-link=Mark Ronan}}</ref>

Conway was awarded a [[Bachelor of Arts|BA]] in 1959 and, supervised by [[Harold Davenport]], began to undertake research in number theory. Having solved the open problem posed by Davenport on [[Waring's problem|writing numbers as the sums of fifth powers]], Conway began to become interested in infinite ordinals.<ref name="frontiers" /> It appears that his interest in games began during his years studying the [[Cambridge Mathematical Tripos]], where he became an avid [[backgammon]] player, spending hours playing the game in the common room. He was awarded his doctorate in 1964 and was appointed as College Fellow and Lecturer in Mathematics at [[Sidney Sussex College, Cambridge]].<ref name="genealogy">{{cite book|author=Sooyoung Chang|year=2011|title = Academic Genealogy of Mathematicians | publisher = World Scientific | page=205 | isbn=978-981-4282-29-1}}</ref> After leaving Cambridge in 1986, he took up the appointment to the [[John von Neumann]] Chair of Mathematics at Princeton University.<ref name="genealogy" />

==Conway's Game of Life==
{{Main|Conway's Game of Life}}
[[File:Gospers glider gun.gif|thumb|right|A single [[Bill Gosper|Gosper]]'s [[Gun (cellular automaton)|Glider Gun]] creating "[[Glider (Conway's Life)|gliders]]" in [[Conway's Game of Life]]]]
Conway was especially known for the invention of the [[Conway's Game of Life|Game of Life]], one of the early examples of a [[cellular automaton]]. His initial experiments in that field were done with pen and paper, long before personal computers existed.

Since the game was introduced by Martin Gardner in ''[[Scientific American]]'' in 1970,<ref>{{Cite magazine|title=Mathematical Games: The fantastic combinations of John Conway's new solitaire game "Life"|first=Martin|last=Gardner|magazine=Scientific American|volume=223|date=October 1970|pages=120–123}}</ref> it has spawned hundreds of computer programs, web sites, and articles.<ref>{{Cite web |url=https://www.dmoz.org/Computers/Artificial_Life/Cellular_Automata/Conway%27s_Game_of_Life |title=DMOZ: Conway's Game of Life: Sites |access-date=11 January 2017 |archive-url=https://web.archive.org/web/20170317103511/http://www.dmoz.org/Computers/Artificial_Life/Cellular_Automata/Conway%27s_Game_of_Life/ |archive-date=17 March 2017 |url-status=dead }}</ref> It is a staple of recreational mathematics. There is an extensive [[wiki]] devoted to curating and cataloging the various aspects of the game.<ref>{{Cite web|url=https://www.conwaylife.com/wiki/Main_Page|title=LifeWiki|website=www.conwaylife.com}}</ref> From the earliest days, it has been a favorite in computer labs, both for its theoretical interest and as a practical exercise in programming and data display. Conway used to hate the Game of Life—largely because it had come to overshadow some of the other deeper and more important things he has done.<ref>[https://www.youtube.com/watch?v=E8kUJL04ELA Does John Conway hate his Game of Life?] (video)</ref> Nevertheless, the game did help launch a new branch of mathematics, the field of [[cellular automata]].<ref>MacTutor History: The game made Conway instantly famous, but it also opened up a whole new field of mathematical research, the field of cellular automata.</ref>

The Game of Life is known to be [[Turing completeness|Turing complete]].<ref>Rendell (2015)</ref><ref name=case>Case (2014)</ref>

==Conway and Martin Gardner==
Conway's career was intertwined with that of mathematics popularizer and ''[[Scientific American]]'' columnist [[Martin Gardner]]. When Gardner featured Conway's Game of Life in his [[Mathematical Games column]] in October 1970, it became the most widely read of all his columns and made Conway an instant celebrity.<ref>''[https://www.bbc.com/news/magazine-29688355 Martin Gardner, puzzle master extraordinaire]'' by [[Colm Mulcahy]], [[BBC News Magazine]], 21 October 2014: "The Game of Life appeared in Scientific American in 1970, and was by far the most successful of Gardner's columns, in terms of reader response."</ref><ref name=topten>Mulcahy (2014).</ref> Gardner and Conway had first corresponded in the late 1950s, and over the years Gardner had frequently written about recreational aspects of Conway's work.<ref>[http://mathfactor.uark.edu/2010/06/ha-conway-on-gardner/ The Math Factor Podcast Website] John H. Conway reminisces on his long friendship and collaboration with Martin Gardner.</ref> For instance, he discussed Conway's game of [[Sprouts (game)|Sprouts]] (Jul 1967), [[Hackenbush]] (Jan 1972), and his [[Angel problem|angel and devil problem]] (Feb 1974). In the September 1976 column, he reviewed Conway's book ''[[On Numbers and Games]]'' and even managed to explain Conway's [[surreal numbers]].<ref>Martin Gardner, ''Penrose Tiles to Trapdoor Ciphers'', W. H. Freeman & Co., 1989, {{ISBN|0-7167-1987-8}}, Chapter 4. A non-technical overview; reprint of the 1976 Scientific American article.</ref>

Conway was a prominent member of [[Martin Gardner#Martin Gardner's Mathematical Grapevine|Martin Gardner's Mathematical Grapevine]]. He regularly visited Gardner and often wrote him long letters summarizing his recreational research. In a 1976 visit, Gardner kept him for a week, pumping him for information on the [[Penrose tiling]]s which had just been announced. Conway had discovered many (if not most) of the major properties of the tilings.<ref>''[http://www.ams.org/notices/200506/fea-gardner.pdf Interview with Martin Gardner]'' [[Notices of the AMS]], Vol. 52, No. 6, June/July 2005, pp. 602–611</ref> Gardner used these results when he introduced the world to Penrose tiles in his January 1977 column.<ref>[https://www.quantamagazine.org/john-conways-life-in-games-20150828/ A Life In Games: The Playful Genius of John Conway] by [[Siobhan Roberts]], [[Quanta Magazine]], 28 August 2015</ref> The cover of that issue of ''Scientific American'' features the Penrose tiles and is based on a sketch by Conway.<ref name=topten/>

Conferences called [[Gathering 4 Gardner]] are held every two years to celebrate the legacy of Martin Gardner, and Conway himself was often a featured speaker at these events, discussing various aspects of recreational mathematics.<ref>[http://gathering4gardner.org/VIDEOS.html Presentation Videos] {{webarchive|url=https://web.archive.org/web/20160809042801/http://gathering4gardner.org/VIDEOS.html |date=9 August 2016 }} from 2014 Gathering 4 Gardner</ref><ref name= bellos>Bellos, Alex (2008). ''[https://www.theguardian.com/science/2008/may/31/maths.science The science of fun]'' The Guardian, 30 May 2008</ref>

==Major areas of research==

===Combinatorial game theory===
Conway was widely known for his contributions to [[combinatorial game theory]] (CGT), a theory of [[partisan game]]s. This he developed with [[Elwyn Berlekamp]] and [[Richard K. Guy|Richard Guy]], and with them also co-authored the book ''[[Winning Ways for your Mathematical Plays]]''. He also wrote the book ''[[On Numbers and Games]]'' (''ONAG'') which lays out the mathematical foundations of CGT.

He was also one of the inventors of [[Sprouts (game)|sprouts]], as well as [[phutball|philosopher's football]]. He developed detailed analyses of many other games and puzzles, such as the [[Soma cube]], [[peg solitaire]], and [[Conway's soldiers]]. He came up with the [[angel problem]], which was solved in 2006.

He invented a new system of numbers, the [[surreal numbers]], which are closely related to certain games and have been the subject of a mathematical novelette by [[Donald Knuth]].<ref>[http://discovermagazine.com/1995/dec/infinityplusonea599 Infinity Plus One, and Other Surreal Numbers] by Polly Shulman, [[Discover Magazine]], 1 December 1995</ref> He also invented a nomenclature for exceedingly [[large number]]s, the [[Conway chained arrow notation]]. Much of this is discussed in the 0th part of ''ONAG''.

===Geometry===
In the mid-1960s with [[Michael Guy (computer scientist)|Michael Guy]], Conway established that there are sixty-four [[uniform polychoron|convex uniform polychora]] excluding two infinite sets of prismatic forms. They discovered the [[grand antiprism]] in the process, the only [[non-Wythoffian]] uniform [[polychoron]].<ref>J. H. Conway, "Four-dimensional Archimedean polytopes", Proc. Colloquium on Convexity, Copenhagen 1965, Kobenhavns Univ. Mat. Institut (1967) 38–39.</ref> Conway has also suggested a system of notation dedicated to describing [[polyhedra]] called [[Conway polyhedron notation]].

In the theory of tessellations, he devised the [[Conway criterion]] which is a fast way to identify many prototiles that tile the plane.<ref name=rhoads>{{cite journal| doi=10.1016/j.cam.2004.05.002 | volume=174 | issue=2 | title=Planar tilings by polyominoes, polyhexes, and polyiamonds | year=2005 | journal=Journal of Computational and Applied Mathematics | pages=329–353 | last1 = Rhoads | first1 = Glenn C.| bibcode=2005JCoAM.174..329R | doi-access=free }}</ref>

He investigated lattices in higher dimensions and was the first to determine the symmetry group of the [[Leech lattice]].

===Geometric topology===
In knot theory, Conway formulated a new variation of the [[Alexander polynomial]] and produced a new invariant now called the Conway polynomial.<ref>[http://mathworld.wolfram.com/ConwayPolynomial.html Conway Polynomial] [[Wolfram MathWorld]]</ref> After lying dormant for more than a decade, this concept became central to work in the 1980s on the novel [[knot polynomial]]s.<ref>Livingston, Charles, Knot Theory (MAA Textbooks), 1993, {{ISBN|0883850273}}</ref> Conway further developed [[tangle theory]] and invented a system of notation for tabulating knots, nowadays known as [[Conway notation (knot theory)|Conway notation]], while correcting a number of errors in the 19th-century knot tables and extending them to include all but four of the non-alternating primes with 11 crossings.<ref>Topology Proceedings 7 (1982) 118.</ref>(Some might say "all but 3½ of the non-alternating primes with 11 crossings." The typographical duplication in the published version of his 1970 table seems to be an effort to include one of the two missing knots that was included in the draft of the table that he sent to Fox [Compare D. Lombardero's 1968 Princeton Senior Thesis, which distinguished this one, but not the other, from all others, based on its Alexander polynomial].)  In knot theory the [[Conway knot]] is named after him.

===Group theory===
He was the primary author of the ''[[ATLAS of Finite Groups]]'' giving properties of many [[finite simple group]]s. Working with his colleagues Robert Curtis and [[Simon P. Norton]] he constructed the first concrete representations of some of the [[sporadic group]]s. More specifically, he discovered three sporadic groups based on the symmetry of the [[Leech lattice]], which have been designated the [[Conway groups]].<ref name=harris>Harris (2015)</ref> This work made him a key player in the successful [[classification of the finite simple groups]].

Based on a 1978 observation by mathematician [[John McKay (mathematician)|John McKay]], Conway and Norton formulated the complex of conjectures known as [[monstrous moonshine]]. This subject, named by Conway, relates the [[monster group]] with [[elliptic modular function]]s, thus bridging two previously distinct areas of mathematics—[[finite group]]s and [[complex function theory]]. Monstrous moonshine theory has now been revealed to also have deep connections to [[string theory]].<ref>[http://www.daviddarling.info/encyclopedia/M/Monstrous_Moonshine_conjecture.html Monstrous Moonshine conjecture] David Darling: Encyclopedia of Science</ref>

Conway introduced the [[Mathieu groupoid]], an extension of the [[Mathieu group M12|Mathieu group M<sub>12</sub>]] to 13 points.

===Number theory===
As a graduate student, he proved one case of a [[Waring's problem|conjecture]] by [[Edward Waring]], that every integer could be written as the sum of 37 numbers each raised to the fifth power, though [[Chen Jingrun]] solved the problem independently before Conway's work could be published.<ref>[http://www.ems-ph.org/journals/newsletter/pdf/2005-09-57.pdf#page=34 Breakfast with John Horton Conway]</ref>

===Algebra===
Conway wrote a textbook on [[Stephen Kleene]]'s theory of state machines and published original work on [[algebraic structure]]s, focusing particularly on [[quaternion]]s and [[octonion]]s.<ref>Conway and Smith (2003): "Conway and Smith's book is a wonderful introduction to the normed division algebras: the real numbers, the complex numbers, the quaternions, and the octonions."</ref>  Together with [[Neil Sloane]], he invented the [[icosian]]s.<ref>{{cite web| url=http://math.ucr.edu/home/baez/week20.html| title=This Week's Finds in Mathematical Physics (Week 20)| author=John Baez| date=2 October 1993}}</ref>

===Analysis===
He invented a [[Conway base 13 function|base 13 function]] as a counterexample to the [[converse (logic)|converse]] of the [[intermediate value theorem]]: the function takes on every real value in each interval on the real line, so it has a [[Darboux property]] but is ''not'' [[continuous function|continuous]].

===Algorithmics===
For calculating the day of the week, he invented the [[Doomsday algorithm]]. The algorithm is simple enough for anyone with basic arithmetic ability to do the calculations mentally. Conway could usually give the correct answer in under two seconds. To improve his speed, he practised his calendrical calculations on his computer, which was programmed to quiz him with random dates every time he logged on. One of his early books was on [[finite-state machine]]s.

===Theoretical physics===
In 2004, Conway and [[Simon B. Kochen]], another Princeton mathematician, proved the [[free will theorem]], a startling version of the "[[Hidden-variable theory|no hidden variables]]" principle of [[quantum mechanics]]. It states that given certain conditions, if an experimenter can freely decide what quantities to measure in a particular experiment, then elementary particles must be free to choose their spins to make the measurements consistent with physical law. In Conway's provocative wording: "if experimenters have [[free will]], then so do elementary particles."<ref>''[http://www.cs.auckland.ac.nz/~jas/one/freewill-theorem.html Conway's Proof Of The Free Will Theorem] {{Webarchive|url=https://web.archive.org/web/20171125154530/https://www.cs.auckland.ac.nz/~jas/one/freewill-theorem.html |date=25 November 2017 }}'' by Jasvir Nagra</ref>

==Awards and honours==
Conway received the [[Berwick Prizes|Berwick Prize]] (1971),<ref name="LMS Prizewinners">{{Cite web|url=https://www.lms.ac.uk/prizes/list-lms-prize-winners|title=List of LMS prize winners &#124; London Mathematical Society|website=www.lms.ac.uk}}</ref> was elected a [[Fellow of the Royal Society]] (1981),<ref name="royal">{{Cite web|url=https://royalsociety.org/people/john-conway-11257/|title=John Conway|website=The Royal Society|access-date=11 April 2020}}</ref> became a fellow of the American Academy of Arts and Sciences in 1992, was the first recipient of the [[Pólya Prize (LMS)]] (1987),<ref name="LMS Prizewinners"/> won the [[Nemmers Prize in Mathematics]] (1998) and received the [[Leroy P. Steele Prize]] for Mathematical Exposition (2000) of the [[American Mathematical Society]]. In 2001 he was awarded an honorary degree from the [[University of Liverpool]],<ref>{{Cite web|url=https://www.cnn.com/2020/04/14/us/john-conway-death-obit-trnd/index.html|title=John H. Conway, a renowned mathematician who created one of the first computer games, dies of coronavirus complications|first=Anna |last=Sturla|website=CNN|access-date=2020-04-16}}</ref> and in 2014 one from [[Alexandru Ioan Cuza University]].<ref>{{Cite web|url=https://www.uaic.ro/en/doctor-honoris-causa-john-horton-conway-mathematics-chair-princeton-university/ |title=Doctor Honoris Causa for John Horton Conway |website=Alexandru Ioan Cuza University |access-date=2020-07-07 }}</ref>
His FRS nomination, in 1981, reads: {{quote|A versatile mathematician who combines a deep combinatorial insight with algebraic virtuosity, particularly in the construction and manipulation of "off-beat" algebraic structures which illuminate a wide variety of problems in completely unexpected ways. He has made distinguished contributions to the theory of finite groups, to the theory of knots, to mathematical logic (both set theory and automata theory) and to the theory of games (as also to its practice).<ref name=royal/>}}

In 2017 Conway was given honorary membership of the British [[Mathematical Association]].<ref>{{Cite web|url=https://www.m-a.org.uk/honorary-members|title=Honorary Members|website=The Mathematical Association|access-date=11 April 2020}}</ref>

==Death==
On 8 April 2020, Conway developed symptoms of [[COVID-19]].<ref name="dailyvoice">{{cite web |last1=Levine |first1=Cecilia |title=COVID-19 Kills Renowned Princeton Mathematician, 'Game Of Life' Inventor John Conway In 3 Days |url=https://dailyvoice.com/new-jersey/mercer/obituaries/covid-19-kills-renowned-princeton-mathematician-game-of-life-inventor-john-conway-in-3-days/786461/ |website=Mercer Daily Voice |language=en |date=12 April 2020}}</ref> On 11 April, he died in [[New Brunswick, New Jersey|New Brunswick]], [[New Jersey]] at the age of 82.<ref name=dailyvoice/><ref>{{Cite news |last=Zandonella |first=Catherine |url=https://www.princeton.edu/news/2020/04/14/mathematician-john-horton-conway-magical-genius-known-inventing-game-life-dies-age |title=Mathematician John Horton Conway, a 'magical genius' known for inventing the 'Game of Life,' dies at age 82 |date=14 April 2020 |access-date=2020-04-15 |publisher=[[Princeton University]] |language=en}}</ref><ref name="NRC">{{Cite news |last=Van den Brandhof |first=Alex |url=https://www.nrc.nl/nieuws/2020/04/12/wiskundige-conway-was-een-speels-genie-en-kenner-van-symmetrie-a3996590 |title=Mathematician Conway was a playful genius and expert on symmetry |date=12 April 2020 |work=[[NRC Handelsblad]] |access-date=12 April 2020 |language=nl}}</ref><ref>{{cite web|url=https://www.nytimes.com/2020/04/15/technology/john-horton-conway-dead-coronavirus.html|title=John Horton Conway, a 'Magical Genius' in Math, Dies at 82|work=New York Times|date=15 April 2020|last=Roberts|first=Siobhan|access-date=17 April 2020|url-access=limited}}</ref><ref>{{Cite news|last=Mulcahy|first=Colm|date=2020-04-23|title=John Horton Conway obituary|language=en-GB|work=The Guardian|url=https://www.theguardian.com/science/2020/apr/23/john-horton-conway-obituary|access-date=2020-05-30|issn=0261-3077}}</ref>

==Publications==
* 1971 – ''Regular algebra and finite machines''. [[Chapman and Hall]], London, 1971, Series: Chapman and Hall mathematics series, {{isbn|0412106205}}.
* 1976 – ''[[On numbers and games]]''. [[Academic Press]], New York, 1976, Series: L.M.S. monographs, 6, {{isbn|0121863506}}.
* 1979 – ''On the Distribution of Values of Angles Determined by Coplanar Points'' (with [[Paul Erdős]], [[Michael Guy]], and H. T. Croft). [[London Mathematical Society|Journal of the London Mathematical Society]], vol. II, series 19, pp.&nbsp;137–143.
* 1979 – ''Monstrous Moonshine'' (with [[Simon P. Norton]]).<ref>{{Cite journal|url=https://academic.oup.com/blms/article/11/3/308/339059|title=Monstrous Moonshine|first1=J. H.|last1=Conway|first2=S. P.|last2=Norton|date=1 October 1979|journal=Bulletin of the London Mathematical Society|volume=11|issue=3|pages=308–339|via=academic.oup.com|doi=10.1112/blms/11.3.308}}</ref> [[Bulletin of the London Mathematical Society]], vol. 11, issue 2, pp.&nbsp;308–339.
* 1982 – ''[[Winning Ways for your Mathematical Plays]]'' (with [[Richard K. Guy]] and [[Elwyn Berlekamp]]). [[Academic Press]], {{isbn|0120911507}}.
* 1985 – ''[[Atlas of finite groups]]'' (with Robert Turner Curtis, [[Simon Phillips Norton]], [[Richard A. Parker]], and [[Robert Arnott Wilson]]). [[Clarendon Press]], New York, [[Oxford University Press]], 1985, {{isbn|0198531990}}.
* 1988 – ''Sphere Packings, Lattices, and Groups''<ref>{{cite journal |last=Guy |first=Richard K.|author-link=Richard K. Guy|title=Review: ''Sphere packings, lattices and groups'', by J. H. Conway and N. J. A. Sloane |journal=Bulletin of the American Mathematical Society (N.S.) |year=1989 |volume=21|issue=1|pages=142–147|url=http://www.ams.org/journals/bull/1989-21-01/S0273-0979-1989-15795-9/S0273-0979-1989-15795-9.pdf |doi= 10.1090/s0273-0979-1989-15795-9 }}</ref> (with [[Neil Sloane]]). [[Springer-Verlag]], New York, Series: Grundlehren der mathematischen Wissenschaften, 290, {{isbn|9780387966175}}.
* 1995 – ''Minimal-Energy Clusters of Hard Spheres'' (with [[Neil Sloane]], R. H. Hardin, and [[Tom Duff]]). [[Discrete & Computational Geometry]], vol. 14, no. 3, pp.&nbsp;237–259.
* 1996 – ''The Book of Numbers'' (with [[Richard K. Guy]]). [[Copernicus Publications|Copernicus]], New York, 1996, {{isbn|0614971667}}.
* 1997 – ''The Sensual (quadratic) Form'' (with Francis Yein Chei Fung). [[Mathematical Association of America]], Washington, DC, 1997, Series: Carus mathematical monographs, no. 26, {{isbn|1614440255}}.
* 2002 – ''On Quaternions and Octonions'' (with Derek A. Smith). [[A. K. Peters]], Natick, MA, 2002, {{ISBN|1568811349}}.
* 2008 – ''The Symmetries of Things'' (with Heidi Burgiel and [[Chaim Goodman-Strauss]]). [[A. K. Peters]], Wellesley, MA, 2008, {{ISBN|1568812205}}.

==See also==
* [[List of things named after John Horton Conway]]

==References==
{{reflist}}

==Sources==
* Alpert, Mark (1999). ''[https://web.archive.org/web/20030427214911/http://www.cpdee.ufmg.br/~seixas/PaginaATR/Download/DownloadFiles/NotJustFunAndGames.PDF Not Just Fun and Games]'' ''Scientific American'', April 1999
* Conway, John and Smith, Derek A. (2003). ''[http://math.ucr.edu/home/baez/octonions/conway_smith/ On quaternions and Octonions : their Geometry, Arithmetic, and Symmetry]'' Bull. Amer. Math. Soc. 2005, vol=42, issue=2, pp.&nbsp;229–243, {{ISBN|1568811349}}
* [[Margaret Boden|Boden, Margaret]] (2006). ''Mind As Machine'', Oxford University Press, 2006, p.&nbsp;1271
* Case, James (2014). ''[https://sinews.siam.org/Details-Page/martin-gardners-mathematical-grapevine Martin Gardner's Mathematical Grapevine]''  Book Reviews of ''Undiluted Hocus-Pocus: The Autobiography of Martin Gardner'' and ''Martin Gardner in the Twenty-First Century'', By James Case, [[Society for Industrial and Applied Mathematics|SIAM]] News, 1 April 2014
* [[Marcus du Sautoy|du Sautoy, Marcus]] (2008). ''Symmetry'', HarperCollins, p.&nbsp;308
* [[Richard K. Guy|Guy, Richard K]] (1983). ''[https://www.jstor.org/pss/2690263 Conway's Prime Producing Machine]'' [[Mathematics Magazine]], Vol. 56, No. 1 (Jan. 1983), pp.&nbsp;26–33
* [[Michael Harris (mathematician)|Harris, Michael]] (2015). [http://www.nature.com/nature/journal/v523/n7561/full/523406a.html Review of ''Genius At Play: The Curious Mind of John Horton Conway''] ''[[Nature (journal)|Nature]]'', 23 July 2015
* Mulcahy, Colm (2014). ''[https://blogs.scientificamerican.com/guest-blog/the-top-10-martin-gardner-scientific-american-articles/?redirect=1 The Top 10 Martin Gardner Scientific American Articles]'' Scientific American, 21 October 2014
* {{cite book |last=Roberts |first=Siobhan |date=2015 |title=Genius at play: The curious mind of John Horton Conway |publisher= Bloomsbury |isbn=978-1620405932 }}
* {{MacTutor Biography|id=Conway}}
* {{MathGenealogy|id=18849}}
* Princeton University (2009). [http://www.math.princeton.edu/WebCV/ConwayBIB.pdf Bibliography of John H. Conway] Mathematics Department
* Rendell, Paul (2015). ''[https://www.springer.com/gp/book/9783319198415 Turing Machine Universality of the Game of Life]'' Springer, July 2015, {{isbn|3319198416}}
* [[Charles Seife|Seife, Charles]] (1994). ''[http://www.users.cloud9.net/~cgseife/conway.html Impressions of Conway]'' [[The Sciences]]
* Schleicher, Dierk (2011), [https://www.ams.org/notices/201305/rnoti-p567.pdf Interview with John Conway], Notices of the AMS

==External links==
{{Sister project links| wikt=no | commons=Category:John Horton Conway | b=no | n=English mathematician John Horton Conway dies after contracting COVID-19 | q=John Horton Conway | s=no | v=no | voy=no | species=no | d=no}}
* {{Scopus|id=55322875100}}
* {{cite web |url= https://mediacentral.princeton.edu/media/Proof+of+the+Free+Will+Theorem/1_pvviswj5 |format= Video |title= Proof of the Free Will Theorem |series= Archived Lectures |date= 20 April 2009 |first= John |last= Conway }}
* {{youtube|p= PLt5AfwLFPxWIL8XA1npoNAHseS-j1y-7V |John Conway. Videos. Numberphile. }}
** {{youtube|ea7lJkEhytA|Look-and-Say Numbers. Feat John Conway (2014) }}
** {{youtube|R9Plq-D1gEk|Inventing the Game of Life (2014) }}
* {{youtube|dVpydmTqfNw|The Princeton Brick (2014) }} Conway leading a tour of brickwork patterns in Princeton, lecturing on the ordinals and on sums of powers and the Bernoulli numbers
*[https://www.quantamagazine.org/john-conway-solved-mathematical-problems-with-his-bare-hands-20200420/ necrology by Keith Hartnett in Quanta Magazine, April 20, 2020]

{{Portal bar|Biography|Mathematics}}

{{Conway's Game of Life}}

{{FRS 1981}}
{{Authority control}}

{{DEFAULTSORT:Conway, John Horton}}
[[Category:John Horton Conway| ]]
[[Category:1937 births]]
[[Category:2020 deaths]]
[[Category:20th-century British mathematicians]]
[[Category:21st-century mathematicians]]
[[Category:Algebraists]]
[[Category:Group theorists]]
[[Category:Combinatorial game theorists]]
[[Category:Cellular automatists]]
[[Category:Mathematics popularizers]]
[[Category:Recreational mathematicians]]
[[Category:English mathematicians]]
[[Category:Alumni of Gonville and Caius College, Cambridge]]
[[Category:Fellows of Sidney Sussex College, Cambridge]]
[[Category:Fellows of the Royal Society]]
[[Category:Princeton University faculty]]
[[Category:Scientists from Liverpool]]
[[Category:British expatriate academics in the United States]]
[[Category:Researchers of artificial life]]
[[Category:Deaths from the COVID-19 pandemic in New Jersey]]
[[Category:Wikipedia articles containing unlinked shortened footnotes]]