{{short description|Serous membrane that forms the lining of the abdominal cavity or the coelom—it covers most of the intra-abdominal (or coelomic) organs—in amniotes and some invertebrates}}
{{hatnote|Not to be confused with [[Perineum]]. "Peritoneal" redirects here: it is not to be confused with [[Perineal (disambiguation){{!}}Perineal]] or [[Peroneal (disambiguation){{!}}Peroneal]]. "Intraperitoneal" redirects here: for the intraperitoneal route of administration, see [[intraperitoneal injection]].}}
{{Infobox anatomy
| Name        = Peritoneum
| Latin       = Peritoneum
| Image       = Gray1040.png
| Caption     = The peritoneum, colored in blue
| Width       = 
| Image2      = Gray1035.png
| Caption2    = The [[epiploic foramen]], [[greater sac]] or general cavity (red) and [[lesser sac]], or omental bursa (blue)
| Precursor   = 
| System      = 
| Artery      = 
| part_of     = [[Abdomen]]
| pronunciation = {{IPAc-en|ˌ|p|ɛr|ᵻ|t|ə|ˈ|n|iː|əm}}
| Vein        = 
| Nerve       = 
| Lymph       = 
}}
The '''peritoneum''' is the [[serous membrane]] forming the lining of the [[abdominal cavity]] or [[coelom]] in [[amniote]]s and some invertebrates, such as [[annelid]]s. It covers most of the intra-abdominal (or coelomic) organs, and is composed of a layer of [[mesothelium]] supported by a thin layer of [[connective tissue]]. This '''peritoneal''' lining of the cavity supports many of the [[abdomen#Contents|abdominal organs]] and serves as a conduit for their [[blood vessel]]s, [[lymphatic vessel]]s, and [[nerve]]s.

The abdominal cavity (the space bounded by the [[vertebrae]], [[abdominal muscles]], [[Thoracic diaphragm|diaphragm]], and [[pelvic floor]]) is different from the '''intraperitoneal space''' (located within the abdominal cavity but wrapped in peritoneum). The structures within the intraperitoneal space are called "intraperitoneal" (e.g., the [[stomach]] and [[intestine]]s), the structures in the abdominal cavity that are located behind the intraperitoneal space are called "[[retroperitoneal]]" (e.g., the [[kidney]]s), and those structures below the intraperitoneal space are called "subperitoneal" or "infraperitoneal" (e.g., the [[bladder]]).

==Structure==
===Layers===
The peritoneum is one continuous sheet, forming two layers and a [[potential space]] between them: the [[peritoneal cavity]].

The outer layer, the '''parietal peritoneum''', is attached to the [[abdominal wall]] and the [[pelvic walls]].<ref>Tank, P. (2013) ''Grants Dissector'' 15th ed., ch.4 ''The abdomen'', p.99</ref> The [[tunica vaginalis]], the serous membrane covering the male [[testis]], is derived from the [[vaginal process]], an outpouching of the parietal peritoneum.

The inner layer, the '''visceral peritoneum''', is wrapped around the visceral organs, located inside the intraperitoneal space for protection. It is thinner than the parietal peritoneum. The [[mesentery]] is a double layer of visceral peritoneum that attaches to the [[human gastrointestinal tract|gastrointestinal tract]]. There are often blood vessels, nerves, and other structures between these layers. The space between these two layers is technically outside of the peritoneal sac, and thus not in the peritoneal cavity.

The potential space between these two layers is the [[peritoneal cavity]], filled with a small amount (about 50 mL) of slippery [[serous fluid]] that allows the two layers to slide freely over each other.

===Subdivisions===
[[Mesentery#Peritoneal folds|Peritoneal folds]] are omenta, mesenteries and ligaments; they connect organs to each other or to the abdominal wall.<ref name="Drake2009p295">Drake et al. (2009) Grays Anatomy for Students, 2nd Edition, ''Abdominal Viscera'', p.406</ref> There are two main regions of the peritoneal cavity, connected by the [[omental foramen]].
* The [[greater sac]], represented in red in the diagrams above.
* The [[lesser sac]], represented in blue. The lesser sac is divided into two "omenta":
** The [[lesser omentum]] (or ''gastrohepatic'') is attached to the [[lesser curvature of the stomach]] and the [[liver]].<ref name="Tortora">Tortora, Gerard J., Anagnostakos, Reginald Merryweather, Nicholas P. (1984) ''Principles of Anatomy and Physiology'', Harper & Row Publishers, New York {{ISBN|0-06-046656-1}}</ref>
** The [[greater omentum]] (or ''gastrocolic'') hangs from the [[greater curve of the stomach]] and loops down in front of the [[intestines]] before curving back upwards to attach to the transverse [[colon (anatomy)|colon]].<ref name="Tortora" /> In effect it is draped in front of the intestines like an apron and may serve as an insulating or protective layer.<ref name="Tortora" />

The [[mesentery]] is the part of the peritoneum through which most abdominal organs are attached to the abdominal wall and supplied with [[blood]] and [[lymph]] vessels and nerves.

====[[Greater omentum|Omenta]]====
{| class="wikitable" style="width: 100%;"
 | '''Sources''' || '''Structure'''  || '''From'''  || '''To'''  || '''Contains '''
 |-
 | [[Dorsal mesentery]] || Greater omentum   || Greater curvature of stomach (and [[spleen]]) || Transverse colon  ||right and left gastroepiploic vessels and fat ||
 |-
 |  || [[Gastrosplenic ligament]]  || Stomach || Spleen || [[Short gastric artery]], [[Left gastroepiploic artery]]
 |-
 |  || [[Gastrophrenic ligament]] || Stomach || [[Thoracic diaphragm|Diaphragm]] || [[inferior phrenic arteries|Left inferior phrenic artery]]
 |-
 |  || [[Gastrocolic ligament]]  ||  Stomach || [[Transverse colon]]  || [[Right gastroepiploic artery]] –
 |-
 |  || [[Splenorenal ligament]] || Spleen|| Kidney  || [[Splenic artery]], [[Tail of pancreas]]
 |-
 | [[Ventral mesentery]] || Lesser omentum || Lesser curvature of the stomach (and [[duodenum]])||Liver  ||The right free margin-hepatic artery, portal vein, and bile duct,lymph nodes and the lymph vessels,hepatic plexus of nerve,all enclosed in perivascular fibrous sheath. Along the lesser curvature of the stomach-left and right gastric artery,gastric group of lymph nodes and lyphatics, branches from gastric nerve.

 |-
 |  ||[[Hepatogastric ligament]]  || Stomach  ||  Liver  || [[Right gastric artery|Right]] and [[left gastric artery]]
 |-
 |  || [[Hepatoduodenal ligament]]  || Duodenum  || Liver  || [[Hepatic artery proper]], [[hepatic portal vein]], [[bile duct]], [[autonomic nerves]]
 |}

====[[Mesenteries]]====
{| class="wikitable" style="width: 100%;"
 | '''Sources''' || '''Structure'''  || '''From'''  || '''To'''  || '''Contains '''
 |-
 | Dorsal mesentery || Mesentery proper || [[Small intestine]] ([[jejunum]] and [[ileum]])  || Posterior abdominal wall  || [[Superior mesenteric artery]], accompanying veins, autonomic nerve plexuses, lymphatics, 100–200 lymph nodes and connective tissue with fat
 |-
 | || [[Transverse mesocolon]] || [[Transverse colon]]   || Posterior abdominal wall  || [[Middle colic]]
 |-
 |   || [[Sigmoid mesocolon]] || Sigmoid colon   || [[Pelvic wall]]  || [[Sigmoid arteries]] and [[superior rectal artery]]
 |-
 |  || [[Mesoappendix]] || Mesentery of ileum  || [[Vermiform appendix|Appendix]]  || [[Appendicular artery]]
 |}

====Other ligaments and folds====
{| class="wikitable" style="width: 100%;"
 | '''Sources''' || '''Structure'''  || '''From'''  || '''To'''  || '''Contains '''
 |-
 | Ventral mesentery || [[Falciform ligament]]  || Liver  || [[Thoracic diaphragm]], anterior abdominal wall  || [[Round ligament of liver]], [[paraumbilical ve
 |-
 | Left [[umbilical vein]] || [[Round ligament of liver]]  || Liver   || [[Navel|Umbilicus]] ||
 |-
 | Ventral mesentery || [[Coronary ligament]]  || Liver || Thoracic diaphragm  ||
 |-
 | [[Ductus venosus]] || [[Ligamentum venosum]]    || Liver  || Liver  ||
 |-
 |  || [[Phrenicocolic ligament]]  || [[Left colic flexure]]  || Thoracic diaphragm ||
 |-
 | Ventral mesentery || [[Left triangular ligament]], [[right triangular ligament]] || Liver  ||    ||
 |-
 |  || [[Umbilical folds]]  ||  [[Urinary bladder]] ||   ||
 |-
 |  || [[Ileocecal fold]] || Ileum || [[Cecum]]  ||
 |-
 |  || [[Broad ligament of the uterus]]   || [[Uterus]]  ||  Pelvic wall  || [[Mesovarium]], [[mesosalpinx]], [[mesometrium]]
 |-
 |  || [[Round ligament of uterus]] || Uterus || [[Inguinal canal]]  ||
 |-
 |  || [[Suspensory ligament of the ovary]] || [[Ovary]]  || Pelvic wall  || [[Ovarian artery]]
|}

In addition, in the [[pelvic cavity]] there are several structures that are usually named not for the peritoneum, but for the areas defined by the peritoneal folds:

{| class="wikitable" style="width: 100%;"
| '''Name''' || '''Location''' || '''Sexes possessing structure'''
 |-
 | [[Rectovesical pouch]] || Between [[rectum]] and urinary bladder || Male only
 |-
 | [[Rectouterine pouch]] || Between rectum and uterus || Female only
 |-
 | [[Vesicouterine pouch]] || Between urinary bladder and uterus || Female only
 |-
 | [[Pararectal fossa]]|| Surrounding rectum || Male and female
 |-
 | [[Paravesical fossa]] || Surrounding urinary bladder || Male and female
|}

===Classification of abdominal structures===
The structures in the abdomen are classified as intraperitoneal, [[retroperitoneal]] or infraperitoneal depending on whether they are covered with visceral peritoneum and whether they are attached by mesenteries (mensentery, mesocolon).

{| class="wikitable" style="width: 100%;"
| '''Intraperitoneal''' || '''Retroperitoneal''' || '''Infraperitoneal / Subperitoneal'''
 |-
 | Stomach,half of the First part of the duodenum [2.2&nbsp;cm], jejunum, ileum, cecum, [[Vermiform appendix|appendix]], [[transverse colon]], [[sigmoid colon]], rectum (upper 1/3)  || The rest of the duodenum, [[ascending colon]], [[descending colon]], rectum (middle 1/3) || Rectum (lower 1/3)
 |-
 | Liver, spleen, [[pancreas]] (only tail)|| Pancreas (except tail) ||
 |-
 | || Kidneys, [[adrenal gland]]s, [[ureter|proximal ureters]], [[renal artery|renal vessels]]
|| Urinary bladder, [[ureter|distal ureters]]
 |-
 | In women: ovaries|| ||Gonadal blood vessels, Uterus, Fallopian Tubes ||
 |-
 | || [[Inferior vena cava]], [[aorta]] ||
|}

Structures that are ''intraperitoneal'' are generally mobile, while those that are ''retroperitoneal'' are relatively fixed in their location.

Some structures, such as the kidneys, are "primarily retroperitoneal", while others such as the majority of the duodenum, are "secondarily retroperitoneal", meaning that structure developed intraperitoneally but lost its mesentery and thus became retroperitoneal.

===Development===
The peritoneum develops ultimately from the [[mesoderm]] of the [[trilaminar embryo]]. As the mesoderm differentiates, one region known as the [[lateral plate mesoderm]] splits to form two layers separated by an [[intraembryonic coelom]]. These two layers develop later into the visceral and parietal layers found in all [[serous cavity|serous cavities]], including the peritoneum.

As an [[embryo]] develops, the various abdominal organs grow into the abdominal cavity from structures in the abdominal wall. In this process they become enveloped in a layer of peritoneum. The growing organs "take their blood vessels with them" from the abdominal wall, and these blood vessels become covered by peritoneum, forming a mesentery.<ref>{{cite web|url=http://healthoracle.org/downloads/P/Peritoneum.pdf|title=healthoracle.org Is For Sale|website=healthoracle.org|access-date=14 April 2018}}</ref>

Peritoneal folds develop from the [[ventral mesentery|ventral]] and [[dorsal mesentery]] of the embryo.<ref name="Drake2009p295"/>

==Clinical significance==
===Peritoneal dialysis===
{{Main|Peritoneal dialysis}}
In one form of [[dialysis]], called '''[[peritoneal dialysis]]''', a glucose solution is sent through a tube into the peritoneal cavity. The fluid is left there for a prescribed amount of time to absorb waste products, and then removed through the tube. The reason for this effect is the high number of arteries and veins in the peritoneal cavity. Through the mechanism of [[diffusion]], waste products are removed from the blood.

===Peritonitis===
{{main|Peritonitis}}
Peritonitis is the [[inflammation]] of the peritoneum. It is more commonly associated to infection from a punctured organ of the abdominal cavity. It can also be provoked by the presence of fluids that produce chemical irritation, such as [[gastric acid]] or [[pancreatic juice]]. Peritonitis causes fever, tenderness, and pain in the abdominal area, which can be localized or diffuse. The treatment involves rehydration, administration of antibiotics, and surgical correction of the underlying cause. Mortality is higher in the elderly and if present for a prolonged time.<ref name="harrison's">{{cite book|last1=Longo|first1=D|last2=Fauci|first2=A|last3=Kasper|first3=D|last4=Hauser|first4=S|last5=Jameson|first5=J|last6=Loscalzo|first6=J|title=Harrison's Principles of Internal Medicine|date=2012|publisher=McGraw-Hill|location=New York|isbn=978-0071748896|edition=18th|pages=2518–2519}}</ref>

===Primary peritoneal carcinoma===
{{main|Primary peritoneal carcinoma}}
Primary peritoneal cancer is a cancer of the cells lining the peritoneum.

==Etymology==
"Peritoneum" is derived from [[Ancient Greek|Greek]] περιτόναιον ''peritonaion'' "peritoneum, abdominal membrane"<ref name=OED>{{cite web|url=http://www.etymonline.com/index.php?term=peritoneum|title=peritoneum - Origin and meaning of peritoneum by Online Etymology Dictionary|website=www.etymonline.com|access-date=14 April 2018}}</ref> via [[Latin]]. In Greek, περί ''peri'' means "around," while τείνω ''teino'' means "to stretch"; thus, "peritoneum" means "stretched over."<ref name=OED/>

==Additional images==
<<gallery class="center">
 Image:Gray403.png|Median sagittal section of pelvis, showing the arrangement of fasciæ
 Image:Gray1038.png|Horizontal disposition of the peritoneum in the lower part of the abdomen
 Image:Gray1125.png|Sagittal section through posterior abdominal wall, showing the relations of the capsule of the kidney
 Image:Gray1224.png|Topography of thoracic and abdominal viscera
 Image:Gray1039.png|Horizontal disposition of the peritoneum in the upper part of the [[abdomen]]
</gallery>

==References==
{{Reflist}}

== External links ==
{{Commons category}}
* {{SUNYAnatomyLabs|37|03|01|02}}
* [http://www.vivo.colostate.edu/hbooks/pathphys/digestion/basics/peritoneum.htmll Overview and diagrams at colostate.edu]

{{Abdominopelvic cavity}}
{{Authority control}}

[[Category:Abdomen]]