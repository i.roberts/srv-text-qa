package de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.json.JSONObject;

import de.dfki.speaker.qa.engine.searchengine.SearchDocument;
import de.dfki.speaker.qa.engine.searchengine.lucene.exceptions.UnSupportedDocumentParserFormatException;
import de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.analyzer.AnalyzerFactory;
import de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.documentparser.DocumentParserFactory;
import de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.documentparser.IDocumentParser;
import de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.documentparser.TXTDocumentParser;
import de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.queryparser.OwnQueryParser;
import de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.resultconverter.LuceneResultConverterToJSON;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;


/**
 * @author Julian Moreno Schneider julian.moreno_schneider@dfki.de
 * @modified_by 
 * @project srv-text-qa
 * @date 28.12.2020
 * @date_modified 
 * @company DFKI
 * @description 
 * Configures Lucene indexing files
 * NOTE: keep in mind the exception  LockObtainFailedException. that can appear when accesing concurrently to an index.
 *
 */
public class LuceneSentenceModule {

	static Logger logger = Logger.getLogger(LuceneSentenceModule.class);
	
	private static LuceneSentenceModule lucene;

	private boolean indexCreate = false;
	private String luceneIndexPath;

	HashMap<String, LuceneIndex> indexes = new HashMap<String, LuceneIndex>();
	
	private StanfordCoreNLP pipeline;

	protected LuceneSentenceModule (String repositoriesPath){
		Properties props = new Properties();
	    props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner");
		// set up pipeline
		pipeline = new StanfordCoreNLP(props);		
		if(repositoriesPath.endsWith(File.separator)){
			luceneIndexPath = repositoriesPath;
		}
		else{
			luceneIndexPath = repositoriesPath + File.separator;
		}
	}
	
	public static LuceneSentenceModule getInstance(String repositoriesPath){
	     if(lucene == null) {
	         lucene = new LuceneSentenceModule(repositoriesPath);
	      }
	      return lucene;
	}
	
	public String listIndexes() {
		try {
			//TODO
			List<LuceneIndex> indexesList = null;
			JSONObject indexes = new JSONObject();
			int counter = 0;
			for (LuceneIndex i : indexesList) {
				indexes.put("index"+counter, i.getIndexId());
				counter++;
			}
			JSONObject obj = new JSONObject();
			obj.put("indexes", indexes);
			return obj.toString();
		}
		catch(Exception e){
			logger.error(e.getMessage());
			throw e;
		}
	}

	public String deleteIndex(String indexId) throws Exception{
		if(!deleteIndexFiles(indexId)){
			String msg = "Error at deleting the files from the index "+indexId;
			logger.error(msg);
			throw new Exception(msg);
		}
		return String.format("Index \"%s\" correctly deleted", indexId);
	}

	public boolean deleteIndexFiles(String indexId) {
		try {
			File f = new File(luceneIndexPath+ indexId);
			boolean error = false;
			File[] files = f.listFiles();
			for (File f2 : files) {
				if(!f2.delete()){
					error=true;
				}
			}
			if(!f.delete()){
				error=true;
			}
			if(error){
				throw new Exception("Some files have not been deleted. Check manually again.");
			}
			return true;
		}
		catch(Exception e){
			return false;
		}
	}

	/** 
	 * Create a new index.
	 * 
	 * @param indexId
	 * @param language
	 * @param sFields
	 * @param sAnalyzers
	 * @return
	 * @throws IOException
	 * @throws ExternalServiceFailedException
	 */
	public String createIndex(String indexId, String language, String sFields, String sAnalyzers, boolean overwrite) throws Exception {
		if(!createIndexFiles(indexId, language, sFields, sAnalyzers, overwrite)){
			String msg = String.format("Error at creating the files for index \"%s\"",indexId);
			logger.error(msg);
			throw new Exception(msg);
		}
		return String.format("The index \"%s\" has been correctly generated", indexId);
	}

	public boolean createIndexFiles(String indexId, String language, String sFields, String sAnalyzers, boolean overwrite) throws Exception {
		//TODO Check if the index exists, then throw Exception.
//		System.out.println(luceneIndexPath + indexId);
		File f = new File(luceneIndexPath + indexId);
		Path path = f.toPath();
		Directory dir = FSDirectory.open(path);

		IndexWriterConfig iwc = null;
		Map<String, Analyzer> analyzerMap = new HashMap<String, Analyzer>();
		String[] fields = sFields.split(";");
		String[] analyzers = sAnalyzers.split(";");
		if(fields.length!=analyzers.length){
			String msg = "The number of fields and analyzers is different";
        	throw new Exception(msg);
		}
		
		for (int i = 0; i < fields.length; i++) {
//			System.out.println("GENERATING "+analyzers[i]+" for "+fields[i]);
			Analyzer particularAnalyzer = AnalyzerFactory.getAnalyzer(analyzers[i],language);
			analyzerMap.put(fields[i], particularAnalyzer);
		}
		PerFieldAnalyzerWrapper wrapper = new PerFieldAnalyzerWrapper(
				AnalyzerFactory.getAnalyzer("standard", language), 
				analyzerMap);
		iwc = new IndexWriterConfig(wrapper);

		if(overwrite){
			iwc.setOpenMode(OpenMode.CREATE);
		}
		else{
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
		}
		// Optional: for better indexing performance, if you are indexing many documents, increase the RAM
		// buffer.  But if you do this, increase the max heap size to the JVM (eg add -Xmx512m or -Xmx1g):
		// iwc.setRAMBufferSizeMB(256.0);
		IndexWriter writer = new IndexWriter(dir, iwc);
		writer.commit();
		writer.close();
		return true;
	}

	public boolean createIndexDDBB(String indexId, String language, String sFields, String sAnalyzers) throws Exception{
		try{
			LuceneIndex li = indexes.get(indexId);
			if(li==null){
				LuceneIndex index = new LuceneIndex();
				index.setIndexId(indexId);
				index.setFields(sFields);
				index.setAnalyzers(sAnalyzers);
				index.setLanguage(language);
				index.setCreationTime(new Date());
				indexes.put(indexId, index);
				return true;
			}
			else{
				logger.error("There is an existing lucene_index with the same name");
				throw new Exception("There is an existing lucene_index with the same name");
			}
		}
		catch(Exception e){
			e.printStackTrace();
			throw e;
		}
	}

	public void deleteDocument(String documentId,String indexId) throws Exception{
		LuceneIndex index = indexes.get(indexId);
		if(index==null){
			String msg = String.format("The index \"%s\" does not exist.",indexId);
        	throw new Exception(msg);
		}
		
		File f = new File(luceneIndexPath + indexId);
		Path path = f.toPath();
		Directory dir = FSDirectory.open(path);
		/**
		 *  Code for deleting the document from the index
		 */
		IndexWriterConfig iwc = null;
		Map<String, Analyzer> analyzerMap = new HashMap<String, Analyzer>();
		String[] fields = index.getFields().split(";");
		String[] analyzers = index.getAnalyzers().split(";");
		if(fields.length!=analyzers.length){
			String msg = "The number of fields and analyzers is different";
        	throw new Exception(msg);
		}
		
		for (int i = 0; i < fields.length; i++) {
//			System.out.println("GENERATING "+analyzers[i]+" for "+fields[i]);
			Analyzer particularAnalyzer = AnalyzerFactory.getAnalyzer(analyzers[i],index.getLanguage());
			analyzerMap.put(fields[i], particularAnalyzer);
		}
		PerFieldAnalyzerWrapper wrapper = new PerFieldAnalyzerWrapper(
				AnalyzerFactory.getAnalyzer("standard", index.getLanguage()), 
				analyzerMap);
		iwc = new IndexWriterConfig(wrapper);

		iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
		IndexWriter writer = new IndexWriter(dir, iwc);
		
		try  {
			Term term= new Term("documentId",documentId);
			writer.deleteDocuments(term);
		}
		catch (IOException e){
			writer.commit();
			writer.close();
        	throw new Exception("Error adding document to index::"+e.getMessage());
		}
		writer.commit();
		writer.close();
	}

	/** 
	 * Index a text file. 
	 * @param docContent
	 * @param docType
	 * @param index
	 * @param create
	 * @param sFields
	 * @param sAnalyzers
	 * @return
	 * @throws IOException
	 * @throws ExternalServiceFailedException
	 */
	public boolean addDocument(String docContent, Map<String, String> metadata, String indexId, String language, String sFields, String sAnalyzers) throws Exception{
//		LuceneIndex index = indexes.get(indexId);
//		if(index==null){
//			String msg = String.format("The index \"%s\" does not exist.",indexId);
//        	throw new Exception(msg);
//		}
		File f = new File(luceneIndexPath + indexId);
		Path path = f.toPath();
		Directory dir = FSDirectory.open(path);

		IndexWriterConfig iwc = null;
		Map<String, Analyzer> analyzerMap = new HashMap<String, Analyzer>();
		String[] fields = sFields.split(";");
		String[] analyzers = sAnalyzers.split(";");
		if(fields.length!=analyzers.length){
			String msg = "The number of fields and analyzers is different";
        	throw new Exception(msg);
		}		
		for (int i = 0; i < fields.length; i++) {
//			System.out.println("GENERATING "+analyzers[i]+" for "+fields[i]);
			Analyzer particularAnalyzer = AnalyzerFactory.getAnalyzer(analyzers[i],language);
			analyzerMap.put(fields[i], particularAnalyzer);
		}
		PerFieldAnalyzerWrapper wrapper = new PerFieldAnalyzerWrapper(
				AnalyzerFactory.getAnalyzer("standard", language), 
				analyzerMap);
		iwc = new IndexWriterConfig(wrapper);

		iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
		// Optional: for better indexing performance, if you are indexing many documents, increase the RAM
		// buffer.  But if you do this, increase the max heap size to the JVM (eg add -Xmx512m or -Xmx1g):
		// iwc.setRAMBufferSizeMB(256.0);

		IndexWriter writer = new IndexWriter(dir, iwc);
		try {
			IDocumentParser documentParser;
			try {
				documentParser = DocumentParserFactory.getDocumentParser("txt");
			} catch (UnSupportedDocumentParserFormatException e1) {
				documentParser = new TXTDocumentParser();
			}
	//		NIFWriter.addLuceneIndexingInformation(docContent, indexId, luceneIndexPath);			
			List<Document> docs = documentParser.parseSentenceDocumentsFromString(docContent, metadata, fields, pipeline);
			for (Document doc : docs) {
//				System.out.println(doc.toString());
				writer.addDocument(doc);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Error adding document to index::"+e.getMessage());
        	return false;
        }
		finally {
			writer.commit();
			writer.close();
		}
		return true;
	}

	/** 
	 * Index a text file. 
	 * @param docContent
	 * @param docType
	 * @param index
	 * @param create
	 * @param sFields
	 * @param sAnalyzers
	 * @return
	 * @throws IOException
	 * @throws ExternalServiceFailedException
	 */
	public boolean addDocuments(String docContents[], Map<String, String> metadata, String indexId, String language, String sFields, String sAnalyzers) throws Exception{
//		LuceneIndex index = indexes.get(indexId);
//		if(index==null){
//			String msg = String.format("The index \"%s\" does not exist.",indexId);
//        	throw new Exception(msg);
//		}
		File f = new File(luceneIndexPath + indexId);
		Path path = f.toPath();
		Directory dir = FSDirectory.open(path);

		IndexWriterConfig iwc = null;
		Map<String, Analyzer> analyzerMap = new HashMap<String, Analyzer>();
		String[] fields = sFields.split(";");
		String[] analyzers = sAnalyzers.split(";");
		if(fields.length!=analyzers.length){
			String msg = "The number of fields and analyzers is different";
        	throw new Exception(msg);
		}		
		for (int i = 0; i < fields.length; i++) {
//			System.out.println("GENERATING "+analyzers[i]+" for "+fields[i]);
			Analyzer particularAnalyzer = AnalyzerFactory.getAnalyzer(analyzers[i],language);
			analyzerMap.put(fields[i], particularAnalyzer);
		}
//		System.out.println("MAP"+analyzerMap);
		PerFieldAnalyzerWrapper wrapper = new PerFieldAnalyzerWrapper(
				AnalyzerFactory.getAnalyzer("standard", language), 
				analyzerMap);
//		System.out.println("WRAPPER"+wrapper.toString());
		iwc = new IndexWriterConfig(wrapper);

		iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
		// Optional: for better indexing performance, if you are indexing many documents, increase the RAM
		// buffer.  But if you do this, increase the max heap size to the JVM (eg add -Xmx512m or -Xmx1g):
		// iwc.setRAMBufferSizeMB(256.0);

		IndexWriter writer = new IndexWriter(dir, iwc);
		IDocumentParser documentParser;
		try {
			documentParser = DocumentParserFactory.getDocumentParser("txt");
		} catch (UnSupportedDocumentParserFormatException e1) {
			documentParser = new TXTDocumentParser();
		}
//		NIFWriter.addLuceneIndexingInformation(docContent, indexId, luceneIndexPath);
		for (String content : docContents) {
			Document doc = documentParser.parseDocumentFromString(content, metadata, fields);
//			System.out.println(doc==null);
//			System.out.println(doc.get("id"));
//			System.out.println(doc.get("ngram"));
//			System.out.println(doc.get("labels"));
//			System.out.print(".");
			try  {
				writer.addDocument(doc);
			}
			catch (IOException e){
				writer.close();
	        	throw new Exception("Error adding document to index::"+e.getMessage());
			}
		}
		writer.commit();
		writer.close();
		return true;
	}

	/**
	 * Searches a query against a field of an index and return hitsToReturn documents.
	 * @param index index where to search for the query text
	 * @param field document field against what to match the query
	 * @param queryString text of the input query
	 * @param hitsToReturn number of documents to be returned
	 * @return JSON format string containing the results information and content
	 * @throws ExternalServiceFailedException
	 */
	public String search(String indexId,String queryString, int hitsToReturn, String language, String sFields, String sAnalyzers) throws Exception {
		try{
//			LuceneIndex index = indexes.get(indexId);
//			if(index==null){
//				String msg = String.format("The index \"%s\" does not exist.",indexId);
//	        	throw new Exception(msg);
//			}
			File f = new File(luceneIndexPath + indexId);
			if(f==null || !f.exists()){
				throw new Exception("Specified index ["+luceneIndexPath + indexId+"] does not exists.");
			}
			logger.info("Searching in folder: "+f.getAbsolutePath());
			Path path = f.toPath();
			Directory dir = FSDirectory.open(path);
			IndexReader reader = DirectoryReader.open(dir);
			IndexSearcher searcher = new IndexSearcher(reader);
			
//			Document doc = reader.document(0);
			String[] fields = sFields.split(";");
			String[] analyzers = sAnalyzers.split(";");
			if(fields.length!=analyzers.length){
				logger.error("The number of fields and analyzers is different");
				throw new Exception("The number of fields and analyzers is different");
			}
			Query query = OwnQueryParser.parseQuery(queryString, fields, analyzers, language);
//			System.out.println("QUERY: "+query.toString());
			TopDocs results = searcher.search(query, hitsToReturn);
//			Explanation exp = searcher.explain(query, 0);
			String result = LuceneResultConverterToJSON.convertResults(query, searcher, results);
			reader.close();
			return result;
		}
		catch(IOException e){
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Searches a query against a field of an index and return hitsToReturn documents.
	 * @param index index where to search for the query text
	 * @param field document field against what to match the query
	 * @param queryString text of the input query
	 * @param hitsToReturn number of documents to be returned
	 * @return JSON format string containing the results information and content
	 * @throws ExternalServiceFailedException
	 */
	public List<SearchDocument> searchDocuments(String indexId,String queryString, int hitsToReturn, String language, String sFields, String sAnalyzers) throws Exception {
		try{
//			LuceneIndex index = indexes.get(indexId);
//			if(index==null){
//				String msg = String.format("The index \"%s\" does not exist.",indexId);
//	        	throw new Exception(msg);
//			}
			File f = new File(luceneIndexPath + indexId);
			if(f==null || !f.exists()){
				throw new Exception("Specified index ["+luceneIndexPath + indexId+"] does not exists.");
			}
			logger.info("Searching in folder: "+f.getAbsolutePath());
			Path path = f.toPath();
			Directory dir = FSDirectory.open(path);
			IndexReader reader = DirectoryReader.open(dir);
			IndexSearcher searcher = new IndexSearcher(reader);
			
//			Document doc = reader.document(0);
			String[] fields = sFields.split(";");
			String[] analyzers = sAnalyzers.split(";");
			if(fields.length!=analyzers.length){
				logger.error("The number of fields and analyzers is different");
				throw new Exception("The number of fields and analyzers is different");
			}
			Query query = OwnQueryParser.parseQuery(queryString, fields, analyzers, language);
//			System.out.println("QUERY: "+query.toString());
			TopDocs results = searcher.search(query, hitsToReturn);
			List<SearchDocument> resultDocuments = new LinkedList<SearchDocument>();
			ScoreDoc[] hits = results.scoreDocs;
			int numTotalHits = hits.length;
//			System.out.println(numTotalHits);
			for (int i = 0; i < numTotalHits; i++) {
				Document doc = searcher.doc(hits[i].doc);
				SearchDocument sd = new SearchDocument();
				sd.setDocumentId(doc.get("id"));
				sd.setText(doc.get("text"));
				sd.setSource("LuceneIndex_"+indexId);
				sd.setEntities(doc.get("entities"));
//				System.out.println("DOCID: "+doc.get("id"));
//				System.out.println("NGRAM: "+doc.get("ngram"));
//				System.out.println("LABELS: "+doc.get("labels"));
				resultDocuments.add(sd);
			}
			reader.close();
			return resultDocuments;
		}
		catch(IOException e){
			throw new Exception(e.getMessage());
		}
	}

	public String getIndexDirectory() {
		return luceneIndexPath;
	}

	public void setIndexDirectory(String indexDirectory) {
		this.luceneIndexPath = indexDirectory;
	}


	public boolean isIndexCreate() {
		return indexCreate;
	}

	public void setIndexCreate(boolean indexCreate) {
		this.indexCreate = indexCreate;
	}
	
}
