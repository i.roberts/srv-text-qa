package de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.resultconverter;

import org.apache.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;

public class LuceneResultConverterToJSON {
	
	static Logger logger = Logger.getLogger(LuceneResultConverterToJSON.class);
	
	public static String convertResults(Query query, IndexSearcher searcher,TopDocs results) throws Exception{
		try{
			ScoreDoc[] hits = results.scoreDocs;
			int numTotalHits = hits.length;//results.totalHits;
//			System.out.println(numTotalHits);
			//TODO Should we put the query into the prefix????
			String output = "";
			for (int i = 0; i < numTotalHits; i++) {
				Document doc = searcher.doc(hits[i].doc);
//				String nifContent = doc.get("nifcontent"); 
//				System.out.println("DEBUG: nifcontent: "+nifContent);
				output += " " + doc.get("id");
//				System.out.println("DOCID: "+doc.get("id"));
//				System.out.println("NGRAM: "+doc.get("ngram"));
//				System.out.println("LABELS: "+doc.get("labels"));
			}
			return output;
		}
		catch(Exception e){
			e.printStackTrace();
			throw new Exception("Error at converting LUCENE output to String:: " + e.getMessage());
		}
	}

}
