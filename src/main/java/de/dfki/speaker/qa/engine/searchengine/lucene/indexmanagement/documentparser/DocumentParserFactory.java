package de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.documentparser;

/**
 * @author Julian Moreno Schneider julian.moreno_schneider@dfki.de
 * @modified_by 
 * @project srv-entity-linking
 * @date 24.06.2020
 * @date_modified 
 * @company DFKI
 * @description 
 *
 */
public class DocumentParserFactory {

	public static IDocumentParser getDocumentParser(String type) throws Exception{
		if(type==null || type.equals("")){
			throw new Exception("DocumentParser is empty and must be provided.");
		}
//		if(type.equalsIgnoreCase("wikidata")){
//			return new WikidataEntryParser();
//		}
		if(type.equalsIgnoreCase("txt")){
			return new TXTDocumentParser();
		}
//		if(type.equalsIgnoreCase("nif")){
//			return new NIFDocumentParser();
//		}
		throw new Exception("DocumentParser type not supported.");
	}
}
