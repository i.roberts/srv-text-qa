package de.dfki.speaker.qa.engine.answerextraction;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

import de.dfki.speaker.qa.engine.searchengine.SearchDocument;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Answer {

	private String snippet;
	
	private double score;
	
	private SearchDocument supportDocument;
	
	private String source;
	
	public String toJSON() throws Exception {
		return new ObjectMapper().writeValueAsString(this);
	}

	public String toELGJSON() throws Exception {
		JSONObject json = new JSONObject();		
		json.put("role", "string");
		json.put("content", snippet);
//		json.put("texts", "");
		json.put("score", score);
		JSONObject features = new JSONObject();
		features.put("source", source);
		features.put("supportDocument", supportDocument.toJSON());
		json.put("features", features);
		return json.toString();
	}
}
