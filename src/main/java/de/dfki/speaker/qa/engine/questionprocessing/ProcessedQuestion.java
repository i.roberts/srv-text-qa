package de.dfki.speaker.qa.engine.questionprocessing;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessedQuestion {

	private String text;
	
	public ProcessedQuestion(String text) {
		this.text = text;
	}

	private String questionType;

	private List<String> entities;
	
	@Override
	public String toString() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String thisAsString = objectMapper.writeValueAsString(this);
			return thisAsString;
		}
		catch(Exception e) {
			e.printStackTrace();
			return super.toString();
		}
	}
}
