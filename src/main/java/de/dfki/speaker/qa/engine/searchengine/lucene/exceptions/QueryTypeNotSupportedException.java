package de.dfki.speaker.qa.engine.searchengine.lucene.exceptions;

@SuppressWarnings("serial")
public class QueryTypeNotSupportedException extends Exception{

	public QueryTypeNotSupportedException(){
		super("The given query type is not supported");
	}
}
