package de.dfki.speaker.qa.engine.searchengine;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import de.dfki.speaker.qa.engine.questionprocessing.ProcessedQuestion;
import de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.LuceneModule;

@Component
public class LuceneSearcher implements Searcher {

	Logger logger = Logger.getLogger(LuceneSearcher.class);
		
	LuceneModule luceneModule;
	
	String language = "de";
	String fields = "id;text;entities";
	String analyzers = "WhiteSpace;Standard;NERAnalyzer";
	String indexName = "text-qa-speaker";
	boolean overwrite = true;

	public LuceneSearcher() throws Exception {
		indexName = "text-qa-speaker";
		language = "de";
		fields = "id;text;entities";
		analyzers = "WhiteSpace;Standard;NERAnalyzer";
		overwrite = false;
		luceneModule = LuceneModule.getInstance("src/main/resources/indexes");
		String resultMessage = luceneModule.createIndex(indexName, language, fields, analyzers, overwrite);
		System.out.println(resultMessage);
	}
	
	public LuceneSearcher(String indexName, String language, String fields, String analyzers, boolean overwrite) throws Exception {
		luceneModule = LuceneModule.getInstance("src/main/resources/indexes");
		String resultMessage = luceneModule.createIndex(indexName, language, fields, analyzers, overwrite);
		System.out.println(resultMessage);
	}
	
	public List<SearchDocument> searchDocuments(ProcessedQuestion processedQuestion, List<SearchDocument> resultList) throws Exception {		
		String queryString = processedQuestion.getText();
		int hitsToReturn = 5;
		List<SearchDocument> resultDocuments = luceneModule.searchDocuments(indexName, queryString, hitsToReturn, language, fields, analyzers);
		resultList.addAll(resultDocuments);
		return resultList;
	}

	public boolean addDocument(String text, Map<String, String> metadata) throws Exception {
		if(luceneModule.addDocument(text, metadata, indexName, language, fields, analyzers)) {
			System.out.println("Document correctly added");
		}
		return true;
	}

	public boolean addDocuments(List<String> documents, List<Map<String, String>> metadatas) throws Exception {
		if(documents.size() != metadatas.size()) {
			throw new Exception("Documents and metadatas size must be equal.");
		}
		for (int i = 0; i < documents.size(); i++) {
			String doc = documents.get(i);
			Map<String, String> metadata = metadatas.get(i);
			if(luceneModule.addDocument(doc, metadata, indexName, language, fields, analyzers)) {
				System.out.println("Documents correctly added");
			}
		}
		return true;
	}

}
