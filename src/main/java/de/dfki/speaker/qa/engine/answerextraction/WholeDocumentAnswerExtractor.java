package de.dfki.speaker.qa.engine.answerextraction;

import java.util.List;

import org.springframework.stereotype.Component;

import de.dfki.speaker.qa.engine.questionprocessing.ProcessedQuestion;
import de.dfki.speaker.qa.engine.searchengine.SearchDocument;

@Component
public class WholeDocumentAnswerExtractor implements AnswerExtractor {

	public WholeDocumentAnswerExtractor(){
	}
	
	public List<Answer> extractAnswers(ProcessedQuestion pq, List<SearchDocument> documents, List<Answer> answers) {
		for (SearchDocument sdoc : documents) {
			Answer a = new Answer();
			a.setSupportDocument(sdoc);
			a.setSource("WholeDocument_AE");
			a.setSnippet(sdoc.getText());
			a.setScore(sdoc.getScore());
			answers.add(a);
		}
		return answers;
	}

}
