package de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.analyzer;

import java.io.StringReader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.ngram.NGramTokenizer;

public class NGramAnalyzerMine extends Analyzer{

	int n = 3;
	
	@Override
	protected TokenStreamComponents createComponents(String text) {
//		String parts[] = text.split("\t");
//		String text2 = "";
//		for (String part : parts) {
//			for (int i = 0; i < part.length()-n; i++) {
//				String ngram = part.substring(i, i+n);
//				text2 += ngram + ";";
//			}
//		}
//		System.out.println("NGRAM TEXT2: "+text2);
//		StringReader sr = new StringReader(text);
//		Tokenizer tokenizer = new NGramTokenizer(sr);
		Tokenizer tokenizer = new NGramTokenizer(n, n);
		//Tokenizer tokenizer = new SemicolonTokenizer(sr);
		TokenStream filter = new LowerCaseFilter(tokenizer);
		return new TokenStreamComponents(tokenizer, filter);
	}

}
