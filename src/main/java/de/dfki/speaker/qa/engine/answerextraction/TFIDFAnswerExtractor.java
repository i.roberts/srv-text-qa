package de.dfki.speaker.qa.engine.answerextraction;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.springframework.stereotype.Component;

import de.dfki.speaker.qa.engine.questionprocessing.ProcessedQuestion;
import de.dfki.speaker.qa.engine.searchengine.SearchDocument;
import de.dfki.speaker.qa.engine.searchengine.lucene.indexmanagement.LuceneModule;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreEntityMention;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

@Component
public class TFIDFAnswerExtractor implements AnswerExtractor {

	private StanfordCoreNLP pipeline;

	public TFIDFAnswerExtractor(){
		Properties props = new Properties();
	    props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner");
		// set up pipeline
		pipeline = new StanfordCoreNLP(props);		
	}
	
	public List<Answer> extractAnswers(ProcessedQuestion pq, List<SearchDocument> documents, List<Answer> answers) {
		/**
		 * Process each document against the question to extract concrete answers
		 */
		for (SearchDocument sdoc : documents) {
			/**
			 * TODO We have to split the sentences of the document,
			 */
			List<String> sentences = new LinkedList<String>();
			String docText = sdoc.getText().trim();
			CoreDocument doc = new CoreDocument(docText);
			pipeline.annotate(doc);
			for (CoreSentence sent : doc.sentences()) {
//		        System.out.println(sent.text());
		        sentences.add(sent.text());
		    }
			
			/**
			 * and then apply a TFIDF approach comparing the sentences to the query.
			 * As an improvement, later we can include also entities and so on.
			 * Probably it will be nice to implement using Lucene. Let see how fat it is.
			 */
			String language = "de";
			String fields = "id;text;entities";
			String analyzers = "WhiteSpace;Standard;NERAnalyzer";
			String indexName = "temporary-sentence-index";
			boolean overwrite = true;

			LuceneModule luceneModule = LuceneModule.getInstance("src/main/resources/indexes/sentences");
			try {
				String resultMessage = luceneModule.createIndex(indexName, language, fields, analyzers, overwrite);
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error to creating LuceneIndex for sentence TFIDF.");
			}

			/**
			 * Index all the sentences in the LuceneIndex
			 */
			int i = 0;
			for (String sentence : sentences) {
				i++;
				HashMap<String, String> metadata = new HashMap<String, String>();
				metadata.put("id", i+"");
				try {
					if(luceneModule.addDocument(sentence, null, indexName, language, fields, analyzers)) {
						System.out.println("Sentence correctly added");
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("Error ading sentence to luceneindex for TFIDF.");
				}
			}
			/**
			 * Retrieve the most important sentences for the document
			 */
			String queryString = pq.getText();
			int hitsToReturn = 1;
			List<SearchDocument> resultDocuments = null;
			try {
				resultDocuments = luceneModule.searchDocuments(indexName, queryString, hitsToReturn, language, fields, analyzers);
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error searching sentences in Lucene index in TFIDF.");
			}
			for (SearchDocument sd : resultDocuments) {
				Answer a = new Answer();
				a.setSupportDocument(sdoc);
				a.setSource("TF-IDF");
				a.setSnippet(sd.getText());
				a.setScore(sd.getScore());
				answers.add(a);
			}
		}
		return answers;
	}

}
