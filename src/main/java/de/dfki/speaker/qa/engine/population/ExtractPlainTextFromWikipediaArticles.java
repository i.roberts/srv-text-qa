package de.dfki.speaker.qa.engine.population;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ExtractPlainTextFromWikipediaArticles {

	public static void main(String[] args) throws Exception {
		
		String path = "/Users/julianmorenoschneider/Downloads/enwiki-latest-pages-articles1.xml-p1p41242";
		
		System.out.print("Loading XML file...");
	    File fXmlFile = new File(path);
	    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	    Document doc = dBuilder.parse(fXmlFile);
		System.out.println("... DONE");
	            
	    //optional, but recommended
	    //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
//		System.out.print("Normalizing XML file...");
//	    doc.getDocumentElement().normalize();
//		System.out.println("... DONE");
	    NodeList nList = doc.getElementsByTagName("page");
	            
	    System.out.println("----------------------------");

	    int cnt = 0;
	    int cntLimit = 100;
	    for (int temp = 0; temp < nList.getLength(); temp++) {
	        Node nNode = nList.item(temp);
	        System.out.println("\nCurrent Element :" + nNode.getNodeName());
	        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	            Element eElement = (Element) nNode;
	            if(eElement.getElementsByTagName("redirect").getLength()==0) {
	            	System.out.println("ARTICLE.");
	            	
	            	double d = Math.random();
	            	if(d<0.005) {
			            String title = eElement.getElementsByTagName("title").item(0).getTextContent();
			            String text = eElement.getElementsByTagName("text").item(0).getTextContent();	            	
	//		            System.out.println("Staff id : " + eElement.getAttribute("id"));
	//		            System.out.println("First Name : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
	//		            System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
	//		            System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
	//		            System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());
			            File oFile = new File("src/main/resources/collections/wikipedia_short/"+title.replace(' ', '_')+".txt");
			            FileUtils.writeStringToFile(oFile, text, "utf-8");
		    	        cnt++;
	            	}
	            }
	            else {
	            	System.out.println("REDIRECT.");
	            }
	        }
	        if(cnt==cntLimit) {
	        	System.out.println("Reached end of "+cntLimit+" articles.");
	        	System.exit(0);
	        }
	    }

	}
}
